# Seen.it #

Throwaway for somebody else to take over.

___________________

# Ideas #

* ## Backend: ##
     * **Config:** All config values
     * **Model:** Entry point for frontend
##
* ## Frontend: ##
     * **preProcessor:** Preprocess files and handle updates in code
     * **defaultPage:** Page template
     * **main:** Example page
     * **Components:** All component**of all pages
     * **Processed:** Processed components, used in includes

##
##
___________________
# Progress #
* ##Backend:##
     * Tests pending
     * Frontend first
##
* ##Frontend:##
     * Under construction
     * Technologies:
         * ~~[Kickstart](http://getkickstart.com/docs/3.x/ui/forms/)~~ **Překrývá odkazy**
         * ~~[YAML](http://www.yaml.de/docs/index.html)~~ **Zbytečně velké a složité**
         * [Bootstrap](http://getbootstrap.com/getting-started/)
         * [Sorttable](https://github.com/voidberg/html5sortable)
         * [Dialogs](https://nakupanda.github.io/bootstrap3-dialog/)