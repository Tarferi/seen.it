var SeenIt = function(action, movieId, buttonParent) {
	var instance = this;
	this.buttonParent = buttonParent;
	this.action = action;
	this.movieId = movieId;
	this.url = "http://seenit.ithief.net";
	this.request_url = this.url + "/Ajax";
	this.lastRequestId;

	this.Btn = new Buttons();

	this.loadingBtn = this.Btn.getLoadingButton();

	this.getLastURL = function() {
		return instance.url + "/movie/" + instance.lastRequestId;
	}

	this.getLastMovieID = function() {
		return instance.lastRequestId;
	}

	this.ajax = new Ajax();

	this.asyncCommit = function(movieId, callback) {
		instance.ajax.asyncQuery(instance.action, {
			"movieId" : movieId
		}, function(result) {
			callback(result);
		});
	}

	this.seenMovie = function(id, callback) {
		instance.asyncCommit(id, function(dc) {
			var result = JSON.parse(dc);
			var success = result["State"] == "Ok";
			var details = result["Details"];
			instance.lastRequestId = result["Id"];
			var ret = 5;
			if (success) {
				if (details == "Seen") {
					ret = 1;
				} else if (details == "Not Seen") {
					ret = 2;
				} else if (details == "Not Listed") {
					ret = 3;
				} else {
					ret = 5;
				}
			} else {
				if (details == "Not logged in") {
					ret = 4;
				} else {
					ret = 5;
				}
			}
			callback(ret);
		});
	}

	this.process = function(movieId) {
		instance.buttonParent.appendChild(instance.loadingBtn);
		instance.seenMovie(instance.movieId, function(response) {
			instance.buttonParent.removeChild(instance.loadingBtn);
			if (response) {
				var seenState = response;
				var btn;
				var movieUrl = instance.getLastURL();
				instance.Btn.setMovieId(instance.getLastMovieID());
				if (seenState == 1) {
					btn = instance.Btn.getSeenButton();
				} else if (seenState == 2) {
					btn = instance.Btn.getUnseenButton();
				} else if (seenState == 3) {
					btn = instance.Btn.getUnlistedButton();
				} else if (seenState == 4) {
					btn = instance.Btn.getNotLoggedButton();
				} else {
					btn = instance.Btn.getServiceUnavailableButton();
				}
				instance.buttonParent.appendChild(btn);
			} else {
				instance.buttonParent.appendChild(instance.Btn.getServiceUnavailableButton());
			}
			instance.Btn.fixTitles();
		})

	};
}
