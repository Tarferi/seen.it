var el = document.createElement("div")
el.style.display = "inline-block"
el.style.float = "right"
el.style.position = "relative"
var hr = document.location.href;
var obj;
var main = /((?:http(?:s)?:\/\/)?www.imdb.com\/title\/tt\d+(?:\/?)$)/;
var plotsummary = /((?:http(?:s)?:\/\/)?www.imdb.com\/title\/tt\d+\/plotsummary$)/;
var req = /((?:http(?:s)?:\/\/)?www.imdb.com\/title\/tt\d+\/\?ref_=rvi_tt$)/;
if (pMatch(hr, main)) {
	var obj = document.getElementsByClassName("title_wrapper")[0];
} else if (pMatch(hr, plotsummary)) {
	var obj = document.getElementsByClassName("subpage_title_block")[0];
} else if (pMatch(hr, req)) {
	var obj = document.getElementsByClassName("title_wrapper")[0];
} else {
	obj = false;
}

function pMatch(str, re) {
	var m;
	if ((m = re.exec(str)) !== null) {
		if (m.index === re.lastIndex) {
			re.lastIndex++;
		}
	}
	if (m) {
		return m.length > 0;
	} else {
		return false;
	}
}

function getMovieID(hr) {
	var str1 = "title/tt";
	var a1 = hr.search(str1);
	a1 += str1.length;
	var res = hr.substr(a1).split("/")[0];
	return res;
}

if (obj) {
	obj.appendChild(el)
	new SeenIt("seenMovieIMDB", getMovieID(window.location.href), el).process();
}