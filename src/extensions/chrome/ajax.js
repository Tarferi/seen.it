var Ajax = function() {
	var instance = this;

	this.url = "http://seenit.ithief.net";
	this.request_url = this.url + "/Ajax";

	this.asyncQuery = function(action, data, callback) {
		data["origin"] = 'chrome-extension://' + chrome.runtime.id;
		var qdata = data;
		var qcallback = callback;
		var qaction = action;
		async(function() {
			var datastr = [];
			for ( var key in qdata) {
				datastr[datastr.length] = key + "=" + qdata[key];
			}
			var data = datastr.join("&");
			var url = instance.request_url + "?action=" + qaction + "&cache=" + (Math.random() * 1000000) + "&" + data;
			//console.log(url);
			var req = new XMLHttpRequest();
			req.withCredentials = true;
			req.onreadystatechange = function() {
				if (req.readyState == 4 && req.status == 200) {
					var status = req.status;
					var result = req.responseText;
					qcallback(result);
				}
			};
			req.open("GET", url, true);
			req.send();
		});
	}
}

function async(qfunc) {
	setTimeout(qfunc, 10);
}