function getMovieID(hr) {
	var str1 = "film/";
	var p1 = hr.search(str1);
	if (p1 != -1) {
		p1 += str1.length
	}
	var idarr = [];
	var tl = hr.length;
	for (var i = p1; i < tl; i++) {
		var chr = hr[i];
		if (chr == "0" || chr == "1" || chr == "2" || chr == "3" || chr == "4" || chr == "5" || chr == "6" || chr == "7" || chr == "8" || chr == "9") {
			idarr.push(chr);
		} else {
			break;
		}
	}
	var id = idarr.join("");
	return id;
}

var head = document.getElementsByClassName("header")[0];

new SeenIt("seenMovieCSFD",getMovieID(window.location.href),head).process();
