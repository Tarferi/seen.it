var Buttons = function() {
	var instance = this;
	this.movieId = 0;
	this.wrapComponent = document.createElement("div");

	this.wrapComponent.style.position = "fixed";
	this.wrapComponent.style.left = "0px";
	this.wrapComponent.style.top = "0px";
	this.wrapComponent.style.right = "0px";
	this.wrapComponent.style.bottom = "0px";
	this.wrapComponent.classList.add("bootstrap-iso");

	this.fader = document.createElement("div");
	this.fader.classList.add("fade");
	this.fader.classList.add("modal-backdrop");

	this.wrapComponent.style.display = "none";
	this.wrapComponent.style.zIndex = 1040;
	document.body.appendChild(this.wrapComponent);
	this.wrapComponent.appendChild(this.fader);

	this.setMovieId = function(movieId) {
		instance.movieId = movieId;
	}

	this.getLoadingButton = function() {
		var d = $('<span class="bootstrap-iso"><div class="qbtn btn btn-sm btn-default disabled" title="Loading data" title="Loading Seen.it data" data-toggle="popover" data-trigger="hover" data-content="If loading data takes too long, try refreshing the page."><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>&nbsp;Loading...</div></span>').get()[0];
		return d;
	}

	this.getServiceUnavailableButton = function() {
		var d = $('<span class="bootstrap-iso"><div class="qbtn pointer btn btn-sm btn-warning" title="Service unavailable" data-toggle="popover" data-trigger="hover" data-content="The Seen.it service is not available at the moment. This usually happens during version updates, when bugs are being fixed or you have outdated plugin. Click the button to be redirected to page with further informations"><span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;Service unavailable</div></span>').get()[0];
		d.addEventListener("click", instance.clickedUnavailableButton);
		return d;
	}

	this.getNotLoggedButton = function() {
		var d = $('<span class="bootstrap-iso"><div class="qbtn pointer btn btn-sm btn-danger" title="Click to login"><span class="glyphicon glyphicon glyphicon-alert"></span>&nbsp;Not logged in</div></span>').get()[0];
		d.addEventListener("click", instance.clickedNotLoggedInButton);
		return d;
	}

	this.getSeenButton = function() {
		var d = $('<span class="bootstrap-iso"><div class="qbtn btn btn-sm btn-success" title="Already seen movie" data-toggle="popover" data-trigger="hover" data-content="You have already seen this movie."><span class="glyphicon glyphicon glyphicon-ok"></span>&nbsp;Seen movie</div></span>').get()[0];
		return d;
	}

	this.getUnseenButton = function() {
		var d = $('<span class="bootstrap-iso"><div class="qbtn btn btn-sm btn-info" title="Unseen movie" data-toggle="popover" data-trigger="hover" data-content="You have not yet seen this movie, but you already have it in your list."><span class="glyphicon glyphicon-eye-close"></span>&nbsp;Unseen movie</div></span>').get()[0];
		return d;
	}

	this.getUnlistedButton = function() {
		var d = $('<span class="bootstrap-iso"><div class="qbtn btn btn-sm btn-default" title="Unseen movie" data-toggle="popover" data-trigger="hover" data-content="You have not yet seen this movie. Click the button to add it to your list"><span class="glyphicon glyphicon-eye-close"></span>&nbsp;Unseen movie</div></span>').get()[0];
		d.addEventListener("click", instance.clickedUnlistedButton);
		return d;
	}

	this.patchBodyT = function(dialog) {
		instance.patchBody(true, dialog);
	}

	this.patchBodyF = function(dialog) {
		instance.patchBody(false, dialog);
	}

	this.bodyPatched = false;

	this.patchDialog = function(dialog) {
		var modal = dialog.getModal()[0];
		modal.remove();
		instance.wrapComponent.appendChild(modal);
	}

	this.patchBody = function(patch, dialog) {
		instance.bodyPatched = patch;
		if (patch) {
			this.wrapComponent.style.display = "inline-block";
			this.fader.classList.add("in");
		} else {
			this.fader.classList.remove("in");
			this.wrapComponent.style.display = "none";
		}
	}

	this.clickedNotLoggedInButton = function() {
		var qq = document.createElement("span");
		qq.classList.add("bootstrap-iso");
		var q = document.createElement("div");
		qq.appendChild(q);
		var inputUsername = document.createElement("input");
		inputUsername.classList.add("form-control");
		inputUsername.type = "text";
		inputUsername.placeholder = "Username";

		var ulabel = document.createElement("label");
		ulabel.innerHTML = "Username:";
		var plabel = document.createElement("label");
		plabel.innerHTML = "Password:";

		inputPassword = document.createElement("input");
		inputPassword.classList.add("form-control");
		inputPassword.type = "password";
		inputPassword.placeholder = "Password";

		q.appendChild(ulabel);
		q.appendChild(inputUsername);
		q.appendChild(document.createElement("br"));
		q.appendChild(plabel);
		q.appendChild(inputPassword);

		var tryLogin = function(dialog) {
			dialog.setClosable(false);
			dialog.enableButtons(false);
			inputUsername.setAttribute("readonly", "true");
			inputPassword.setAttribute("readonly", "true");
			var ajax = new Ajax();
			ajax.asyncQuery("tryLogin", {
				"username" : inputUsername.value,
				"password" : inputPassword.value
			}, function(result) {
				var res;
				try {
					res = JSON.parse(result);
				} catch (err) {
					res = {};
				}
				if (res) {
					if (res["Status"] == "Ok") {
						dialog.close();
						location.reload();
						return;
					}
				}
				dialog.setClosable(true);
				dialog.enableButtons(true);
				inputUsername.removeAttribute("readonly");
				inputPassword.removeAttribute("readonly");
			});
		};

		instance.patchDialog(BootstrapDialog.show({
			closeable : false,
			type : BootstrapDialog.TYPE_PRIMARY,
			title : 'Seen.it login',
			message : qq,
			onshow : instance.patchBodyT,
			onhidden : instance.patchBodyF,
			cssClass : 'login-dialog',
			buttons : [ {
				label : 'Login',
				cssClass : 'btn-primary',
				action : function(dialog) {
					tryLogin(dialog);
				}
			}, {
				label : 'Cancel',
				cssClass : 'btn-default',
				action : function(dialog) {
					dialog.close();
				}
			}, ]
		}));
	}

	this.fixTitles = function() {
		$('[data-toggle="popover"]').popover({
			trigger : 'hover',
			'placement' : 'right'
		});
	}

	this.clickedUnavailableButton = function() {

	}

	this.clickedUnlistedButton = function() {
		var qq = document.createElement("span");
		qq.classList.add("bootstrap-iso");
		var q = document.createElement("div");
		qq.appendChild(q);
		var sel = document.createElement("select");
		var mainOpt = document.createElement("option");
		mainOpt.innerHTML = "All movies";
		mainOpt.value = "-1";
		sel.appendChild(mainOpt);
		var mainCat = document.createElement("optgroup");
		sel.appendChild(mainCat);
		mainCat.setAttribute("label", "My albums");
		sel.classList.add("form-control");
		var ajax = new Ajax();
		var processing = document.createElement("span");
		processing.classList.add("glyphicon");
		processing.classList.add("glyphicon-refresh");
		processing.classList.add("glyphicon-refresh-animate");
		processing.classList.add("text-info");
		var lbl = document.createElement("label");
		lbl.innerHTML = "Loading album list&nbsp;";
		q.appendChild(lbl);
		q.appendChild(processing);
		var term = false;
		var addBtn = document.createElement("div");
		addBtn.classList.add("btn");
		addBtn.classList.add("btn-success");
		addBtn.classList.add("pointer");
		var addGlyph = document.createElement("span");
		addGlyph.classList.add("glyphicon");
		addGlyph.classList.add("glyphicon-plus");
		addBtn.appendChild(addGlyph);
		var addText = document.createElement("span");
		addText.innerHTML = "&nbsp;Import";
		addBtn.appendChild(addText);
		addBtn.style.marginTop = "10px";
		var canAdd = true;
		var movieId = instance.movieId;
		var okLabel = document.createElement("span");
		okLabel.style.display="none";
		okLabel.classList.add("text-success");
		addBtn.addEventListener("click", function() {
			if (canAdd) {
				canAdd = false;
				addBtn.classList.add("disabled");
				var qajax = new Ajax();
				var albumId = sel.options[sel.selectedIndex].value;
				var albumName = sel.options[sel.selectedIndex].innerHTML;
				var action = "copyMovies";
				if (albumId == -1) {
					action = "importMovies";
				}
				qajax.asyncQuery(action, {
					"data" : movieId,
					"albumId" : albumId
				}, function(result) {
					if (term) {
						return;
					}
					var res;
					try {
						res = JSON.parse(result);
					} catch (err) {
						res = {};
					}
					if (res["Status"] == "Ok") {
						canAdd = true;
						addBtn.classList.remove("disabled");
						okLabel.style.display="";
						okLabel.innerHTML="Added to "+(albumId==-1?"All movies.":"album '"+albumName+"'");
						return;
					}
					instance.patchDialog(BootstrapDialog.show({
						onshow : instance.patchBodyT,
						onhidden : instance.patchBodyF,
						type : BootstrapDialog.TYPE_DANGER,
						title : 'Import failed',
						message : "Movie import failed. Please try it again later.",
					}));
					canAdd = true;
					addBtn.classList.remove("disabled");
				});
			}
		});
		var dialog = BootstrapDialog.show({
			onshow : instance.patchBodyT,
			onhidden : instance.patchBodyF,
			type : BootstrapDialog.TYPE_PRIMARY,
			title : 'Seen.it import movie',
			message : qq,
			buttons : [ {
				label : 'Close',
				cssClass : 'btn-primary',
				action : function(dialog) {
					term = true;
					dialog.close();
				}
			}, ]
		});
		instance.patchDialog(dialog);
		var modal = dialog.getModal().get();
		ajax.asyncQuery("getAlbumList", [], function(result) {
			if (term) {
				return;
			}
			var res;
			try {
				res = JSON.parse(result);
			} catch (err) {
				res = {};
			}
			if (res) {
				if (res["Status"] == "Ok") {
					var data = res["Data"];
					for ( var id in data) {
						// for (var i = 0; i < data.length; i++) {
						var aname = data[id];
						var elem = document.createElement("option");
						elem.innerHTML = Safe.escape(aname);
						elem.value = id;
						mainCat.appendChild(elem);
					}
					lbl.innerHTML = "Select album:";
					processing.remove();
					q.appendChild(sel);
					q.appendChild(addBtn);
					q.appendChild(document.createElement("br"));
					q.appendChild(okLabel);
				}
			}
		});
	}
}
