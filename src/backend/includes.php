<?php
$gu = glob ( "exceptions/*.php" );
foreach ( $gu as $filename ) {
	include $filename;
}

include_once "Indenter.php";

include_once "Lock.php";
include_once "Session.php";
include_once "Config.php";
include_once "Model.php";

include_once "db/Database.php";
include_once "db/DatabaseDatasource.php";
include_once "db/MySQL.php";

include_once "objects/annotation_parser.php";
include_once "objects/annotations.php";
include_once "objects/DatabaseObject.php";
include_once "objects/DatabaseObjectView.php";
include_once "objects/ObjectAnotations.php";

include_once "../frontend/preProcessor.php";
include_once "../frontend/viewState.php";

include_once "../frontend/QForms.php";

include_once  "objects/importers/Importer.php"; 

foreach ( glob ( "objects/importers/*.php" ) as $filename ) {
	include_once $filename;
}

foreach ( glob ( "objects/inner/*.php" ) as $filename ) {
	include $filename;
}

foreach ( glob ( "objects/inner/views/*.php" ) as $filename ) {
	include $filename;
}

function my_gzdecode($data) {
	if (function_exists ( "gzdecode" )) {
		return gzdecode ( $data );
	} else {
		return gzinflate ( substr ( $data, 10, - 8 ) );
	}
}

class Safe {
	// return text.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
	public static function escape($string) {
		return str_replace ( array (
				"&",
				"<",
				">",
				"\"",
				"'" 
		), array (
				"&amp;",
				"&lt;",
				"&gt;",
				"&quot;",
				"&#039;" 
		), $string );
		// return htmlentities($string);
	}

}