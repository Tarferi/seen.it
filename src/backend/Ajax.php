<?php
ini_set ( 'xdebug.var_display_max_depth', 20 );

include_once "includes.php";

if (isset ( $_GET ["origin"] )) {
	header ( 'Access-Control-Allow-Origin: ' . $_GET ["origin"] );
	header ( 'Access-Control-Allow-Credentials: true' );
}

class Ajax {
	/**
	 * Session handler
	 *
	 * @var Session
	 */
	private $session;

	private function requiresLogin() {
		if ($this->session->isLogged ()) {
			return true;
		} else {
			die ( json_encode ( array (
					"State" => "Fail",
					"Details" => "Not logged in" 
			) ) );
		}
	}

	private function getOrFalse($array, $key) {
		if (isset ( $array [$key] )) {
			return $array [$key];
		} else {
			return false;
		}
	}

	public function __construct() {
		$this->session = Config::getConfig ()->getSession ();
		if (isset ( $_GET ["action"] )) {
			$state = array ();
			try {
				$action = $_GET ["action"];
				switch ($action) { // TODO: Downloading avatars batch
					/*
					 * case "downloadAvatars": $movies = Movie::searchMatch ( array () ); var_dump ( $movies ); foreach ( $movies as $movie ) { $movie->savePoster ( $movie->getPosterURL () ); } break;
					 */
					case "updateMovieAvatar":
						if ($movieId = $this->getOrFalse ( $_GET, "movieId" )) {
							$movie = Movie::getByID ( $movieId, true );
							if ($movie) {
								$movie->reImportMovie ();
								die ( json_encode ( array (
										"Status" => "Ok" 
								) ) );
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "seenMovieCSFD":
					case "seenMovieIMDB":
						
						if ($movieId = $this->getOrFalse ( $_GET, "movieId" )) {
							if ($action == "seenMovieCSFD") {
								Movie::importCsfdMovie ( $movieId );
								$movie = Movie::getByCsfdID ( $movieId );
							} else {
								Movie::importImdbMovie ( $movieId );
								$movie = Movie::getByImdbID ( $movieId );
							}
							if ($movie) {
								if ($this->requiresLogin ()) {
									$user = $this->session->getUser ();
									$state ["State"] = "Ok";
									$mvi = MovieList::getMovieForUser ( $user, $movie );
									$state ["Id"] = $movie->getID ();
									if ($mvi) {
										if ($mvi->hasSeen ()) {
											$state ["Details"] = "Seen";
										} else {
											$state ["Details"] = "Not Seen";
										}
									} else {
										$state ["Details"] = "Not Listed";
									}
								}
							} else {
								$state ["State"] = "Fail";
								$state ["Details"] = "No such movie";
							}
						}
					break;
					case "usernameAvailable":
						
						if (isset ( $_GET ["username"] )) {
							$username = $_GET ["username"];
							echo User::usernameExists ( $username ) ? "NO" : "YES";
							die ();
						} else {
							die ( "NO" );
						}
					break;
					case "IHaveSeen":
					case "IHaveNotSeen":
						
						$seen = $_GET ["action"] == "IHaveSeen" ? MovieList::HAS_SEEN : MovieList::HAS_NOT_SEEN;
						if (isset ( $_GET ["movieId"] )) {
							$mid = $_GET ["movieId"];
							$movie = Movie::getByID ( $mid, true );
							if ($movie) {
								if ($this->requiresLogin ()) {
									$user = Config::getConfig ()->getSession ()->getUser ();
									$mvi = $user->setSeenMovie ( $user, $movie, $seen );
									die ( json_encode ( array (
											"Status" => "Ok",
											"Date" => Config::timestampToTextDate ( $mvi->getSeenDate () ) 
									) ) );
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "DeleteFromAlbum":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["movieListId"] ) && isset ( $_GET ["albumId"] )) {
								$movieListId = $_GET ["movieListId"];
								$albumId = $_GET ["albumId"];
								$album = Album::getByID ( $albumId, true );
								$movieList = MovieList::getByID ( $movieListId, true );
								if ($album && $movieList) {
									if ($album->getOwnerID () == $this->session->getUser ()->getID ()) {
										if ($movieList->getUserID () == $this->session->getUser ()->getID ()) {
											$album->removeMovieListFromAlbum ( $movieList );
											die ( json_encode ( array (
													"Status" => "Ok" 
											) ) );
										}
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "DeleteFromList":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["movieListId"] )) {
								$movieListId = $_GET ["movieListId"];
								$movieList = MovieList::getByID ( $movieListId, true );
								if ($movieList) {
									if ($movieList->getUserID () == $this->session->getUser ()->getID ()) {
										$albumList = AlbumMovie::deleteMovieList ( $movieListId );
										$movieList->delete ();
										die ( json_encode ( array (
												"Status" => "Ok" 
										) ) );
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "tryLogin":
						
						if (isset ( $_GET ["username"] ) && isset ( $_GET ["password"] )) {
							$username = $_GET ["username"];
							$password = $_GET ["password"];
							$user = User::getByCredentials ( $username, $password );
							if ($user) {
								$this->session->setLogged ( true );
								$this->session->setUser ( $user );
								die ( json_encode ( array (
										"Status" => "Ok" 
								) ) );
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "getAlbumList":
						
						if ($this->requiresLogin ()) {
							$albs = Album::getAlbumsForUser ( $this->session->getUser () );
							if (is_array ( $albs )) {
								$names = array ();
								foreach ( $albs as $alb ) {
									$names [$alb->getID ()] = $alb->getName ();
								}
								die ( json_encode ( array (
										"Status" => "Ok",
										"Data" => $names 
								) ) );
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "getMoviesInAlbum":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["albumId"] )) {
								$count = isset ( $_GET ["count"] ) ? $_GET ["count"] * 1 : 20;
								$offset = isset ( $_GET ["page"] ) ? $_GET ["page"] * 1 : 1;
								$count = $count < 1 ? 10 : $count > 100 ? 100 : $count;
								$offset = $offset < 1 ? 1 : $offset;
								$albumId = $_GET ["albumId"];
								$album = Album::getByID ( $albumId, true );
								if ($album) {
									$user = $this->session->getUser ();
									if ($album->userCanView ( $user )) {
										$total = 0;
										$movies = QuickAlbumMovie::getAlbumMoviesForID ( $albumId, $user->getID (), $count, $offset, $total );
										$as = array ();
										foreach ( $movies as $movie ) {
											$as [] = array (
													"movieId" => $movie->getMovieID (),
													"movieListId" => $movie->getMovieListtID (),
													"albumMovieId" => $movie->getAlbumListID (),
													"name" => $movie->getMovieName (),
													"seen" => $movie->hasSeen (),
													"inlist" => $movie->isInList (),
													"date" => Config::timestampToTextDate ( $movie->getSeenDate () ) 
											);
										}
										die ( json_encode ( array (
												"Status" => "Ok",
												"Data" => $as,
												"Count" => $count,
												"Total" => $total,
												"Page" => $offset 
										) ) );
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "getAllMyMovies":
						
						if ($this->requiresLogin ()) {
							$userId = $this->session->getUser ()->getID ();
							$count = isset ( $_GET ["count"] ) ? $_GET ["count"] * 1 : 20;
							$offset = isset ( $_GET ["page"] ) ? $_GET ["page"] * 1 : 1;
							$count = $count < 1 ? 10 : $count > 100 ? 100 : $count;
							$offset = $offset < 1 ? 1 : $offset;
							$user = $this->session->getUser ();
							$total = 0;
							$movies = QuickMovie::getAllMovies ( $userId, $count, $offset, $total );
							$as = array ();
							
							foreach ( $movies as $movie ) {
								$as [] = array (
										"movieId" => $movie->getMovieID (),
										"movieListId" => $movie->getMovieListtID (),
										"name" => $movie->getMovieName (),
										"seen" => $movie->hasSeen (),
										"inlist" => $movie->isInList (),
										"date" => Config::timestampToTextDate ( $movie->getSeenDate () ) 
								);
							}
							die ( json_encode ( array (
									"Status" => "Ok",
									"Data" => $as,
									"Count" => $count,
									"Total" => $total,
									"Page" => $offset 
							) ) );
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "newAlbum":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["albumName"] )) {
								$albumName = $_GET ["albumName"];
								$album = Album::createAlbum ( $this->session->getUser (), $albumName );
								if ($album) {
									die ( json_encode ( array (
											"Status" => "Ok",
											"Data" => array (
													"AlbumID" => $album->getID (),
													"AlbumName" => $album->getName (),
													"AlbumCount" => 0 
											) 
									) ) );
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "changeAlbumName":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["albumId"] ) && isset ( $_GET ["albumName"] )) {
								$albumId = $_GET ["albumId"];
								$albumName = trim ( $_GET ["albumName"] );
								$album = Album::getByID ( $albumId, true );
								if ($album->getOwnerID () == $this->session->getUser ()->getID ()) {
									if (strlen ( $albumName ) > 1 && strlen ( $albumName ) < 30) {
										if ($album) {
											$album->setName ( $albumName );
											die ( json_encode ( array (
													"Status" => "Ok",
													"Data" => array (
															"AlbumName" => $album->getName () 
													) 
											) ) );
										}
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "deleteAlbum":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["albumId"] )) {
								$albumId = $_GET ["albumId"];
								$album = Album::getByID ( $albumId, true );
								if ($album) {
									if ($album->getOwnerID () == $this->session->getUser ()->getID ()) {
										AlbumMovie::deleteAllFromAlbum ( $album );
										$album->delete ();
										die ( json_encode ( array (
												"Status" => "Ok" 
										) ) );
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					case "copyMovies":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["data"] ) && isset ( $_GET ["albumId"] )) {
								$albumId = $_GET ["albumId"];
								$data = $_GET ["data"];
								$album = Album::getByID ( $albumId, true );
								if ($album) {
									if ($album->getOwnerID () == $this->session->getUser ()->getID ()) {
										$ids = explode ( ":", $data );
										$createdCount = 0;
										$list = MovieList::newItems ( $this->session->getUser (), $ids, $createdCount );
										$addedCount = AlbumMovie::addMovies ( $album, $list );
										die ( json_encode ( array (
												"Status" => "Ok",
												"Created" => $createdCount,
												"Added" => $addedCount 
										) ) );
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "importMovies":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["data"] )) {
								$data = $_GET ["data"];
								$ids = explode ( ":", $data );
								$createdCount = 0;
								$list = MovieList::newItems ( $this->session->getUser (), $ids, $createdCount );
								die ( json_encode ( array (
										"Status" => "Ok",
										"Created" => $createdCount,
										"Added" => 0 
								) ) );
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "moveMovies":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["data"] ) && isset ( $_GET ["albumId"] ) && isset ( $_GET ["sourceId"] )) {
								$albumId = $_GET ["albumId"];
								$fromAlbumId = $_GET ["sourceId"];
								$data = $_GET ["data"];
								$album = Album::getByID ( $albumId, true );
								$fromAlbum = Album::getByID ( $fromAlbumId, true );
								if ($album && $fromAlbum) {
									if ($album->getOwnerID () == $this->session->getUser ()->getID ()) {
										if ($fromAlbum->getOwnerID () == $this->session->getUser ()->getID ()) {
											$ids = explode ( ":", $data );
											$addedCount = 0;
											$list = MovieList::getItems ( $this->session->getUser (), $ids, $addedCount );
											AlbumMovie::moveMovies ( $fromAlbum, $album, $list );
											die ( json_encode ( array (
													"Status" => "Ok",
													"Added" => $addedCount 
											) ) );
										}
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "deleteMovies":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["data"] ) && isset ( $_GET ["albumId"] )) {
								$albumId = $_GET ["albumId"];
								$data = $_GET ["data"];
								$album = Album::getByID ( $albumId, true );
								if ($album) {
									if ($album->getOwnerID () == $this->session->getUser ()->getID ()) {
										$ids = explode ( ":", $data );
										$deletedCount = 0;
										$list = MovieList::getItems ( $this->session->getUser (), $ids, $deletedCount );
										AlbumMovie::deleteMovies ( $album, $list );
										die ( json_encode ( array (
												"Status" => "Ok",
												"Deleted" => count ( $list ) 
										) ) );
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "deleteMoviesFromList":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["data"] )) {
								$data = $_GET ["data"];
								$ids = explode ( ":", $data );
								$deletedCount = 0;
								$list = MovieList::getItems ( $this->session->getUser (), $ids, $deletedCount );
								AlbumMovie::deleteMovieListArr ( $list );
								die ( json_encode ( array (
										"Status" => "Ok",
										"Deleted" => count ( $list ) 
								) ) );
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "getCommentsForMovie":
						
						if (isset ( $_GET ["movieId"] )) {
							$movieId = $_GET ["movieId"];
							$data = Comment::getQuickDetails ( $movieId );
							$b = array ();
							foreach ( $data as $d ) {
								$b [] = array (
										"CommentID" => $d->getCommentID (),
										"CreatorID" => $d->getCreatorID (),
										"Title" => $d->getCommentTitle (),
										"Contents" => $d->getCommentContent (),
										"CreatorUsername" => $d->getCreatorUsername (),
										"AvatarURL" => User::getAvatarURLForUsername ( $d->getCreatorUsername () ) 
								);
							}
							$userId = - 1;
							if ($this->session->isLogged ()) {
								$userId = $this->session->getUser ()->getID ();
							}
							die ( json_encode ( array (
									"Status" => "Ok",
									"Data" => $b,
									"MyId" => $userId 
							) ) );
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "savePost":
						
						if (isset ( $_GET ["postId"] ) && isset ( $_GET ["title"] ) && isset ( $_GET ["contents"] )) {
							$postId = $_GET ["postId"];
							$title = $_GET ["title"];
							$contents = $_GET ["contents"];
							$post = Comment::getByID ( $postId, true );
							if ($post) {
								if ($post->getCreatorID () == $this->session->getUser ()->getID ()) {
									$post->setTitle ( $title );
									$post->setContents ( $contents );
									$post->save ();
									die ( json_encode ( array (
											"Status" => "Ok" 
									) ) );
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "newPost":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["movieId"] ) && isset ( $_GET ["title"] ) && isset ( $_GET ["contents"] )) {
								$movieId = $_GET ["movieId"];
								$title = $_GET ["title"];
								$contents = $_GET ["contents"];
								$movie = Movie::getByID ( $movieId, true );
								if ($movie) {
									$user = $this->session->getUser ();
									Comment::newComment ( $movie, $user, $title, $contents );
									die ( json_encode ( array (
											"Status" => "Ok" 
									) ) );
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "deletePost":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["postId"] )) {
								$postId = $_GET ["postId"];
								$post = Comment::getByID ( $postId, true );
								if ($post) {
									if ($post->getCreatorID () == $this->session->getUser ()->getID ()) {
										$post->setDeleted ( true );
										die ( json_encode ( array (
												"Status" => "Ok" 
										) ) );
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "canAddFriend":
						
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["name"] )) {
								$name = $_GET ["name"];
								$user = User::getForName ( $name );
								if ($user) {
									$me = $this->session->getUser ();
									if ($user->getID () != $me->getID ()) {
										$fs = FriendShip::getFriendShip ( $user, $me );
										if (! $fs) {
											die ( json_encode ( array (
													"Status" => "Ok",
													"UserID" => $user->getID () 
											) ) );
										} else {
											if ($fs->meRejected ( $me )) {
												die ( json_encode ( array (
														"Status" => "Ok",
														"UserID" => $user->getID () 
												) ) );
											}
										}
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "setAlbumPrivate":
					case "setAlbumFriends":
					case "setAlbumPublic":
						
						$vis = $action == "setAlbumPrivate" ? Album::A_PRIVATE : ($action == "setAlbumFriends" ? Album::A_FRIENDS : Album::A_PUBLIC);
						if ($this->requiresLogin ()) {
							if (isset ( $_GET ["albumId"] )) {
								$albumId = $_GET ["albumId"];
								$album = Album::getByID ( $albumId, true );
								if ($album) {
									if ($album->getOwnerID () == $this->session->getUser ()->getID ()) {
										$album->setVisibility ( $vis );
										$album->save ();
										die ( json_encode ( array (
												"Status" => "Ok" 
										) ) );
									}
								}
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
					
					case "register":
						
						$vars = array (
								"username",
								"password",
								"email",
								"firstname",
								"lastname",
								"terms",
								"bday",
								"firstname_vis",
								"lastname_vis",
								"email_vis",
								"bday_vis",
								"findus" 
						);
						$is = true;
						foreach ( $vars as $var ) {
							if (! isset ( $_GET [$var] )) {
								$is = false;
							}
						}
						if ($is) {
							$username = trim ( $_GET ["username"] );
							$password = $_GET ["password"];
							$email = $_GET ["email"];
							$fname = trim ( $_GET ["firstname"] );
							$lname = trim ( $_GET ["lastname"] );
							$terms = $_GET ["terms"];
							$bday = $_GET ["bday"];
							$findus = $_GET ["findus"];
							$vfname = $_GET ["firstname_vis"];
							$vlname = $_GET ["lastname_vis"];
							$vemail = $_GET ["email_vis"];
							$vbday = $_GET ["bday_vis"];
							$errarr = array ();
							if (User::getForName ( $username )) {
								$errarr ["User"] = "Username already exists";
							}
							if (! User::usernameValid ( $username )) {
								$errarr ["User"] = "Invalid username";
							}
							if (strlen ( $password ) < 3 || strlen ( $password ) > 40) {
								$errarr ["Password"] = "Invalid password";
							}
							if ($terms == "off") {
								$errarr ["Terms"] = "You must agree with the terms";
							}
							if (strlen ( $fname ) > 0) {
								if (! User::usernameValid ( $fname )) {
									$errarr ["Firstname"] = "Invalid firstname";
								}
							}
							if (strlen ( $lname ) > 0) {
								if (! User::usernameValid ( $lname )) {
									$errarr ["Lastname"] = "Invalid lastname";
								}
							}
							if (! QInputObject::isValidEmail ( $email )) {
								$errarr ["Email"] = "Invalid email";
							}
							$bday = 0;
							if ($bday != "") {
								if (! QInputObject::isValidDate ( $bday )) {
									$errarr ["Birthday"] = "Invalid date";
								} else {
									if (QInputObject::dateIsPast ( $bday )) {
										$errarr ["Birthday"] = "This must be past date";
									} else {
										$bday = QInputObject::dateToStamp ( $bday );
									}
								}
							}
							$visCheck = function ($vis) {
								return $vis == 0 || $vis == 1 || $vis == 2;
							};
							if (! $visCheck ( $vfname ) && $visCheck ( $vlname ) && $visCheck ( $vemail ) && $visCheck ( $vbday )) {
								$errarr ["Visibilities"] = "Invalid value";
							}
							if (count ( $errarr ) == 0) {
								User::createUser ( $username, $password, $email, $fname, $lname, $bday, $vemail, $vfname, $vlname, $vbday );
								$user = User::getByCredentials ( $username, $password );
								if ($user) {
									Config::getConfig ()->getSession ()->setLogged ( true );
									Config::getConfig ()->getSession ()->setUser ( $user );
									die ( json_encode ( array (
											"Status" => "Ok" 
									) ) );
								} else {
									die ( json_encode ( array (
											"Status" => "Error" 
									) ) );
								}
							} else {
								die ( json_encode ( array (
										"Status" => "Miss",
										"Details" => $errarr 
								) ) );
							}
						}
						die ( json_encode ( array (
								"Status" => "Error" 
						) ) );
					break;
				}
			} catch ( NotLoggedException $e ) {
				$state ["State"] = "Fail";
				$state ["Details"] = "Not logged in";
			} catch ( DatabaseException $e ) {
				$state ["State"] = "Fail";
				$state ["Details"] = "Database exception";
			}
			if (count ( $state ) == 0) {
				$state ["State"] = "Fail";
				$state ["Details"] = "Invalid request";
			}
			die ( json_encode ( $state ) );
		}
	}

}

new Ajax ();
die ( json_encode ( array (
		"State" => "Fail",
		"Details" => "Unknown method" 
) ) );

