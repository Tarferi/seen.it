<?php

class Model {
	private $dataSource;
	
	public function __construct() {
		$this->dataSource=new DatabaseDataSource();
		$this->session=new Session();
	}
	
	public function getDataSource() {
		return $this->dataSource;
	}
	
}