<?php
ini_set ( 'xdebug.var_display_max_depth', 20 );

class Config {

	/**
	 * When set to True, database is recreated on every page load
	 *
	 * @var boolean
	 */
	const DEBUG_RECREATE_DATABASE = false;

	/**
	 * When set to True, Protect annotations are not effective
	 *
	 * @var unknown
	 */
	const DEBUG_OVERRIDE_PROTECT = false;

	/**
	 * When set to True, catalog is used for caching component files.
	 * When set to False, all components are recreated on every page load regardless the catalog
	 *
	 * @var boolean
	 */
	const DEBUG_USE_CATALG = false;

	/**
	 * When set to True, page load time and database query count are displayed at the bottom of the page
	 *
	 * @var boolean
	 */
	const DEBUG_SHOW_PAGE_INFO = false;
	
	/**
	 * Current instance (for singleton purposes only)
	 *
	 * @var Config
	 */
	private static $instance;
	
	/**
	 * Current user session.
	 * Is never NULL
	 *
	 * @var Session
	 */
	private $session;

	public function redirect($target) {
		if ($target == null && $target != "") {
			$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			header ( "Location: " . $actual_link );
		} else {
			header ( "Location: " . Config::SERVER_URL . "/" . $target );
		}
		die ();
	}

	/**
	 *
	 * @return Config
	 */
	public static function getConfig() {
		if (static::$instance == null) {
			static::$instance = new Config ();
			static::$instance->init ();
		}
		return static::$instance;
	}

	const SERVER_URL = "http://127.0.0.1:8000/web";

	const BACKEND_URL = "backend";

	const FRONTEND_URL = "frontend";

	const FRONT2BACK = "../backend";

	const BACK2FRONT = "../frontend";

	const PUBLIC_WEB_ROOT = "/web";

	const DATABASE_IP = "127.0.0.1";

	const DATABASE_PORT = "3306";

	const DATABASE_NAME = "seenit";

	const DATAASE_TABLE_PREFIX = "seenit_";

	const DATABASE_USERNAME = "root";

	const DATABASE_PASSWORD = "1";
	/**
	 *
	 * @deprecated Moved to session
	 * @var String
	 */
	private $client_IP;
	/**
	 *
	 * @deprecated Moved to session
	 * @var String
	 */
	private $client_Port;
	
	/**
	 *
	 * @deprecated Moved to session
	 * @var String
	 */
	private $client_Agent;
	/**
	 * Application model entry point
	 *
	 * @var Model
	 */
	private $model;

	private function __construct() {
		$this->client_IP = $_SERVER ["REMOTE_ADDR"];
		$this->client_Port = $_SERVER ["REMOTE_PORT"];
		$this->client_Agent = $_SERVER ["HTTP_USER_AGENT"];
		date_default_timezone_set ( "Europe/Prague" );
	}

	private function init() {
		$this->model = new Model ();
		$this->handleDatabaseCreation ();
		$this->session = new Session ();
	}

	private function handleDatabaseCreation() {
		if (Config::DEBUG_RECREATE_DATABASE) {
			DatabaseObject::recreateDatabase ();
		}
	}

	/**
	 *
	 * @return Session
	 */
	public function getSession() {
		return $this->session;
	}

	/**
	 *
	 * @return Model
	 */
	public function getModel() {
		return $this->model;
	}

	/**
	 * @Return DatabaseDataSource
	 */
	public static function getDatasource() {
		return static::getConfig ()->getModel ()->getDataSource ();
	}

	public function getClientIP() {
		return $this->client_IP;
	}

	public function getClientPort() {
		return $this->client_Port;
	}

	public function getClientAgent() {
		return $this->client_Agent;
	}
	
	/**
	 * Microtime of script execution start
	 *
	 * @var float
	 */
	private static $loadStart;
	/**
	 * Microtime of script execution end
	 *
	 * @var float
	 */
	private static $loadEnd;

	public static function getLoadStart() {
		return static::loadStart;
	}

	public static function getLoadEnd() {
		return static::loadEnd;
	}

	public static function executionStart() {
		static::$loadStart = microtime ( true );
	}

	public static function executionEnd() {
		static::$loadEnd = microtime ( true );
	}

	public static function getRuntime() {
		return static::$loadEnd - static::$loadStart;
	}

	public static function timestampToTextDay($stamp) {
		return ($stamp == null || $stamp == 0) ? "" : date ( 'd.m.Y', $stamp );
	}

	public static function timestampToTextDate($stamp) {
		return ($stamp == null || $stamp == 0) ? "" : date ( 'd.m.Y H:i', $stamp );
	}

	public static function timestampToInputDate($stamp) {
		return ($stamp == null || $stamp == 0) ? "" : date ( 'Y-m-d', $stamp );
	}

}