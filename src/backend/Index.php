<?php
include_once "includes.php";

class Index {
	private $routers = array ();
	private $singleRoutes = array ();
	private $defaultRoute = null;
	private $currentData = array ();

	private function addRoute($source, $target = false) {
		$this->routers [$source] = $target ? $target : $source;
	}

	private function addSingleRoute($source, $target = false) {
		$this->singleRoutes [$source] = $target ? $target : $source;
	}

	private function setDefaultRoute($target) {
		$this->defaultRoute = $target;
	}

	private function handleRouting() {
		if (! isset ( $_GET ["page"] )) {
			$page = "main";
		} else {
			$page = $_GET ["page"];
		}
		$this->currentData = $_GET;
		if (isset ( $this->routers [$page] )) {
			$target = $this->routers [$page];
		} else {
			if (isset ( $this->singleRoutes [$page] )) {
				$target = $this->singleRoutes [$page];
				$target ();
				return;
			} else {
				$target = $this->defaultRoute;
			}
		}
		if (isset ( $_GET [$page] )) {
			$target ( $_GET [$page] );
		} else {
			$target = $this->defaultRoute;
			$target ( null );
		}
	}

	private function constr_func() {
		$instance = $this;
		$this->setDefaultRoute ( function ($data) use($instance) {
			$instance->defaultRouteTarget ( $data );
		} );
		$this->addRoute ( "movie", function ($data) use($instance) {
			$instance->isMovie ( $data );
		} );
		$this->addRoute ( "user", function ($data) use($instance) {
			$instance->isUser ( $data );
		} );
		$this->addSingleRoute ( "register", function () use($instance) {
			$instance->isRegister ();
		} );
		$this->addRoute ( "console", function () use($instance) {
			$instance->isConsole ();
		} );
		$this->handleRouting ();
		Config::executionEnd ();
		if (Config::DEBUG_SHOW_PAGE_INFO) {
			echo "Execution time: " . Config::getRuntime () . " ms<br />";
			echo "Queries passed: " . Config::getConfig ()->getDatasource ()->getSource ()->getTotalQueriesCount () . "<br />";
		}
	}

	public function __construct() {
		Config::executionStart ();
		$this->constr_func ();
	}

	public function isConsole() {
		$this->loadPage ( "console" );
	}

	public function defaultRouteTarget($target) {
		$this->loadPage ( "main" );
	}

	public function isMovie($movieId) {
		viewState::setMovie ( Movie::getByID ( $movieId, true ) );
		$this->loadPage ( "movie" );
	}

	public function isUser($userId) {
		viewState::setViewUserID ( $userId );
		$this->loadPage ( "user" );
	}

	public function isRegister() {
		$this->loadPage ( "register" );
	}

	public function loadPage($pageName) {
		Config::getConfig ();
		$pageFile = "pages/" . $pageName . ".php";
		$cwd = getcwd ();
		chdir ( Config::BACK2FRONT );
		ob_start ();
		include $pageFile;
		$page = ob_get_clean ();
		$indenter = new Indenter ();
		$indenter->setIndentation ( "  " );
		try {
			$output = $indenter->clean ( $page );
		} catch ( Exception $e ) {
			die ( "Errors exists on page!" );
		}
		echo $output;
		chdir ( $cwd );
		return;
	}

}

new Index ();