<?php

abstract class Database {
	private $link = false;
	private $selectCount = 0;
	private $executeCount = 0;

	public function getSelectQueriesCount() {
		return $this->selectCount;
	}

	public function getExecuteQueriesCount() {
		return $this->executeCount;
	}

	public function getTotalQueriesCount() {
		return $this->selectCount + $this->executeCount;
	}

	public function selectQuery($query, $replacements) {
		if ($this->link === false) {
			$this->link = $this->handleInitDatabase ();
			if ($this->link === false || $this->link === null) {
				throw new DatabaseException ();
			}
		}
		$query = $this->handleReplacements ( $this->link, $query, $replacements );
		if ($query != false) {
			$this->selectCount ++;
			return $this->commitSelectQuery ( $this->link, $query );
		}
		return false;
	}

	protected abstract function handleInitDatabase();

	protected abstract function handleReplacements($link, $query, $replacements);

	protected abstract function commitExecuteQuery($link, $query);

	protected abstract function commitSelectQuery($link, $query);

	public function executeQuery($query, $replacements) {
		if ($this->link === false) {
			$this->link = $this->handleInitDatabase ();
			if ($this->link === false || $this->link === null) {
				throw new DatabaseException ();
			}
		}
		$query = $this->handleReplacements ( $this->link, $query, $replacements );
		if ($query != false) {
			$this->executeCount ++;
			//var_dump($query);
			//echo $query."<br /><br />";
			return $this->commitExecuteQuery ( $this->link, $query );
		}
		return false;
	}

}
