<?php

class DatabaseDataSource {
	private $source;

	public function __construct() {
		$this->source = new MySQL ();
	}

	public function getSource() {
		return $this->source;
	}

	public function getUserByID($id) {
	}

	public function getUserByLogin($username, $password) {
		$res = $this->source->selectQuery ( "Select * from `users` WHERE username='?' AND password='?'", array (
				$username,
				strtolower ( md5 ( $password ) ) 
		) );
		if (count ( $res ) == 1) {
			$u = $res [0];
			return new User ( $u ["id"], $u ["Username"], $u ["password"], $u ["name"], $u ["email"], $u ["borndate"] );
		}
		return false;
	}

	public function getRowsByTableAndID($table, $id) {
		$res = $this->source->selectQuery ( "Select * from `?` WHERE id='?'", array (
				$table,
				$id 
		) );
		return $res;
	}

	public function saveObject($object) {
		$req = $object->getSaveSQL ();
		return $this->source->executeQuery ( $req [0], $req [1] );
	}

	public function createObject($object) {
		$req = $object->getCreateSQL ();
		return $this->source->executeQuery ( $req [0], $req [1] );
	}

	public function deleteObject($object) {
		$req = $object->getDeleteSQL ();
		return $this->source->executeQuery ( $req [0], $req [1] );
	}

}