<?php

class MySQL extends Database {

	protected function handleInitDatabase() {
		$b = new mysqli ( Config::DATABASE_IP, Config::DATABASE_USERNAME, Config::DATABASE_PASSWORD, Config::DATABASE_NAME );
		if ($b) {
			$b->set_charset("utf8");
			return $b;
		} else {
			return false;
		}
	}

	protected function handleReplacements($link, $query, $replacements) {
		$b = explode ( "?", $query );
		if (count ( $b ) != count ( $replacements ) + 1) {
			throw new DatabaseException ();
		} else {
			$totalstr = array ();
			for($i = 0, $o = count ( $replacements ); $i < $o; $i ++) {
				$totalstr [] = $b [$i];
				$r = $link->real_escape_string ( $replacements [$i] );
				$totalstr [] = $r;
			}
			$totalstr [] = $b [count ( $b ) - 1];
		}
		return implode ( "", $totalstr );
	}

	protected function commitExecuteQuery($link, $query) {
		try {
			$link->query ( $query );
		} catch ( Exception $e ) {
			throw new DatabaseException ();
		}
	}

	protected function commitSelectQuery($link, $query) {
		try {
			$result = $link->query ( $query );
			if (! $result) {
				throw new DatabaseException ();
			}
			$res = array ();
			while ( $obj = $result->fetch_object () ) {
				$res [] = $obj;
			}
			$result->free();
			return $res;
		} catch ( Exception $e ) {
			var_dump($query);
			throw new DatabaseException ();
		}
	}

}