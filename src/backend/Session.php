<?php

class Session {

	private static $instanceExists=false;
	
	const S_LOGGED = "S_LOGGED";

	const S_USERID = "S_USERID";
	private $cache_user = false;

	public function __construct() {
		if(!static::$instanceExists) {
			static::$instanceExists=true;
			session_start ();
		}
	}

	public function destroy() {
		session_destroy ();
	}

	public function isLogged() {
		return $this->getDataOrFalse ( Session::S_LOGGED );
	}

	public function setLogged($logged = true) {
		$this->addData ( Session::S_LOGGED, $logged );
		$this->cache_user = false;
	}

	public function setUser($user) {
		$userId = $user->getID ();
		$this->addData ( Session::S_USERID, $userId );
		$this->cache_user = $user;
	}

	/**
	 * 
	 * @throws NotLoggedException
	 * @return User
	 */
	public function getUser() {
		if ($this->isLogged ()) {
			if ($this->cache_user) {
				return $this->cache_user;
			}
			$userId = $this->getDataOrFalse ( Session::S_USERID );
			if ($userId) {
				$user = User::getByID ( $userId, true );
				if ($user) {
					$this->cache_user = $user;
					return $user;
				}
			}
		}
		$this->setLogged ( false );
		throw new NotLoggedException ();
	}

	public function addData($key, $data) {
		$_SESSION [$key] = $data;
	}

	public function hasData($key) {
		return isset ( $_SESSION [$key] );
	}

	public function getData($key) {
		return $_SESSION [$key];
	}

	public function getDataOrDefault($key, $default) {
		return $this->hasData ( $key ) ? $this->getData ( $key ) : $default;
	}

	public function getDataOrFalse($key) {
		return $this->getDataOrDefault ( $key, false );
	}

	public function getDataOrTrue($key) {
		return $this->getDataOrDefault ( $key, true );
	}

	public function getDataOrNull($key) {
		return $this->getDataOrDefault ( $key, null );
	}

}