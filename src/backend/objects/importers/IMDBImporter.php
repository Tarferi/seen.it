<?php

class IMDBImporter extends Importer {

	private function getCsfdID($mv) {
		$csfd = file_get_contents ( "http://www.csfd.cz/hledat/?q=" . urlencode ( $mv->getTitle () ) );
		$html = (ord ( $csfd [0] ) == 31) ? my_gzdecode ( $csfd ) : $csfd;
		$dom = new domDocument ();
		
		@$dom->loadHTML ( $html );
		$dom->preserveWhiteSpace = false;
		$xpath = new DOMXPath ( $dom );
		$nodes = $xpath->query ( "//div[@id='search-films']/div[@class='content']/ul[@class='ui-image-list js-odd-even']/li" );
		$temp_dom = new DOMDocument ();
		foreach ( $nodes as $n )
			$temp_dom->appendChild ( $temp_dom->importNode ( $n, true ) );
			// $xpath=new DOMXpath($temp_dom);
		foreach ( $nodes as $i => $node ) {
			$link = $xpath->query ( "a/@href", $node )->item ( 0 )->nodeValue;
			$id = preg_replace ( "/\A(\/film\/)(\d+)(.*\z)/", "$2", $link );
			$name = $xpath->query ( "div/h3/a", $node )->item ( 0 )->nodeValue;
			$originalName = $xpath->query ( "div/span", $node )->item ( 0 );
			if ($originalName != null) {
				$originalName = $originalName->nodeValue;
				$originalName = preg_replace ( "/\A\((.*)\)\z/", "$1", $originalName );
			} else {
				$originalName = $name;
			}
			$year = $xpath->query ( "div/p", $node )->item ( 0 )->nodeValue;
			if ($year) {
				$year = explode ( ",", $year );
				$year = trim ( $year [count ( $year ) - 1] );
			} else {
				$year = false;
			}
			if ($mv->getTitle () == $originalName) {
				if ($mv->getYear ()) {
					if ($mv->getYear () == $year) {
						return $id;
					}
				}
			}
		}
		return false;
	}

	public function getMovieDetailsById($id, $resolveOthers = false) {
		if (! $this->isReImporting ()) {
			$existingMovie = Movie::getByImdbID ( $id );
		} else {
			$existingMovie = false;
		}
		if (! $existingMovie) {
			$imdb = file_get_contents ( "http://www.imdb.com/title/tt" . $id . "/plotsummary" );
			$html = (ord ( $imdb [0] ) == 31) ? gzdecode ( $imdb ) : $imdb;
			$dom = new domDocument ();
			@$dom->loadHTML ( $html );
			$xpath = new DOMXPath ( $dom );
			$nodes = $xpath->query ( "//p[@class='plotSummary']" );
			if ($nodes->item ( 0 ) != null) {
				$popisen = $nodes->item ( 0 )->nodeValue;
			} else {
				$nodes = $xpath->query ( "//div[@id='no_content']" );
				if ($nodes->item ( 0 ) != null) {
					$popisen = null;
				} else {
					$popisen = "";
				}
			}
			$nameEN = "";
			$year = "";
			$poster = $xpath->query ( "//img[@class='poster']/@src" );
			$posterURL = "";
			if ($poster->item ( 0 )) {
				$poster = $poster->item ( 0 )->nodeValue;
				$pos = strpos ( $poster, "_V1_" );
				if ($pos) {
					$posterURL = substr ( $poster, 0, $pos ) . "_V1_UY268_CR14,0,182,268_AL_.jpg";
				}
			}
			$nodes = $xpath->query ( "//meta[@property='og:title']" );
			if ($nodes->item ( 0 ) != null) {
				$nameEN = str_replace ( "â", "-", $nodes->item ( 0 )->getAttribute ( "content" ) );
				
				$res = array ();
				preg_match ( "/\A([a-zA-Z \d_\(\)]+)(\(.*\))?\z/", $nameEN, $res );
				if (count ( $res ) == 3) {
					$nameEN = $res [1];
					$yer = $res [2];
					$yer = preg_split ( "/\b/", $yer );
					$year = false;
					foreach ( $yer as $d ) {
						if (is_numeric ( $d )) {
							$year = $d * 1;
							break;
						}
					}
				} else if (count ( $res ) == 2) {
					$year = false;
					$nameEN = $res [1];
				}
			}
			$name = trim ( $nameEN );
			$name = preg_replace ( "/( ?\(.*\)\z)/", "", $name );
			$mv = new ImportedMovieDetails ( $id, $name, $year, $popisen, $posterURL );
			if ($resolveOthers) {
				$csfd = false;
				$csfdid = $this->getCsfdID ( $mv );
				if ($csfdid) {
					$imp = new CSFDImporter ($this->isReImporting());
					$csfd = $imp->getMovieDetailsById ( $csfdid, false );
				}
				return new ImporterMovieDetailsCollection ( $csfd, $mv );
			} else {
				return $mv;
			}
		} else {
			$mvIMDB = new ImportedMovieDetails ( $existingMovie->getImdbID (), $existingMovie->getNameEN (), $existingMovie->getYear (), $existingMovie->getDescriptionEN (), $existingMovie->getPosterURL () );
			$mvCSFD = new ImportedMovieDetails ( $existingMovie->getCsfdID (), $existingMovie->getNameCZ (), $existingMovie->getYear (), $existingMovie->getDescriptionCZ (), $existingMovie->getPosterURL () );
			if ($resolveOthers) {
				return new ImporterMovieDetailsCollection ( $mvCSFD, $mvIMDB );
			} else {
				return $mvIMDB;
			}
		}
	}

}