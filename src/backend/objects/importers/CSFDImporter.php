<?php

class CSFDImporter extends Importer {

	public function getMovieDetailsById($id, $resolveOthers = false) {
		if (! $this->isReImporting ()) {
			$existingMovie = Movie::getByImdbID ( $id );
		} else {
			$existingMovie = false;
		}
		if (! $existingMovie) {
			$dom = new domDocument ();
			$csfd = file_get_contents ( "http://www.csfd.cz/film/" . $id );
			$html = (ord ( $csfd [0] ) == 31) ? my_gzdecode ( $csfd ) : $csfd;
			
			$str1 = "http://www.imdb.com/title/tt";
			$str2 = "/combined";
			$pos1 = strpos ( $html, $str1 );
			if ($resolveOthers) {
				if ($pos1 >= 0 && $pos1 !== false) {
					$pos1 += strlen ( $str1 );
					$pos2 = strpos ( $html, $str2, $pos1 );
					$imdbId = substr ( $html, $pos1, $pos2 - $pos1 );
					$imdb = new IMDBImporter ($this->isReImporting());
					$imdb = $imdb->getMovieDetailsById ( $imdbId, false );
				} else {
					$imdb = false;
				}
			}
			$str1 = "\"year\":";
			$str2 = ",";
			$pos1 = strpos ( $html, $str1 );
			if ($pos1 >= 0 && $pos1 !== false) {
				$pos1 += strlen ( $str1 );
				$pos2 = strpos ( $html, $str2, $pos1 );
				$year = substr ( $html, $pos1, $pos2 - $pos1 );
			} else {
				$year = null;
			}
			
			@$dom->loadHTML ( $html );
			$dom->preserveWhiteSpace = false;
			
			$xpath = new DOMXPath ( $dom );
			$nazvy = array ();
			$zeme = array ();
			$names_other = "";
			$nodes = $xpath->query ( "//h1[@itemprop='name']" );
			$nameCZ = $nodes->item ( 0 )->nodeValue;
			
			foreach ( $xpath->query ( "//ul[@class='names']/li/h3" ) as $li ) {
				$nazvy [] = $li->nodeValue;
			}
			foreach ( $xpath->query ( "//ul[@class='names']/li/img" ) as $li ) {
				$zeme [] = $li->getAttribute ( 'alt' );
			}
			$alts = array ();
			foreach ( $nazvy as $nid => $nazev ) {
				$zem = $zeme [$nid];
				$alts [] = array (
						"country" => $zem,
						"name" => $nazev 
				);
			}
			
			$nodes = $xpath->query ( "//h2[@class='average']" );
			$hodnoceni = str_replace ( '%', '', $nodes->item ( 0 )->nodeValue );
			
			$nodes = $xpath->query ( "//p[@class='origin']" );
			$podrobnosti = explode ( ", ", $nodes->item ( 0 )->nodeValue );
			
			$nodes = $xpath->query ( "//div[@data-truncate='570']" );
			if ($nodes->item ( 0 ) != null) {
				$popis = $nodes->item ( 0 )->nodeValue;
			} else {
				$popis = "";
			}
			$nodes = $xpath->query ( "//img[@class='film-poster']" );
			$poster_url = $nodes->item ( 0 )->getAttribute ( 'src' );
			if (substr ( $poster_url, 0, 2 ) == "//") {
				$poster_url = "http:" . $poster_url;
			}
			$mv = new ImportedMovieDetails ( $id, trim ( $nameCZ ), $year, trim ( $popis ), $poster_url );
			if ($resolveOthers) {
				return new ImporterMovieDetailsCollection ( $mv, $imdb );
			} else {
				return $mv;
			}
		} else {
			$mvIMDB = new ImportedMovieDetails ( $existingMovie->getImdbID (), $existingMovie->getNameEN (), $existingMovie->getYear (), $existingMovie->getDescriptionEN (), $existingMovie->getPosterURL () );
			$mvCSFD = new ImportedMovieDetails ( $existingMovie->getCsfdID (), $existingMovie->getNameCZ (), $existingMovie->getYear (), $existingMovie->getDescriptionCZ (), $existingMovie->getPosterURL () );
			if ($resolveOthers) {
				return new ImporterMovieDetailsCollection ( $mvCSFD, $mvIMDB );
			} else {
				return $mvIMDB;
			}
		}
	}

}