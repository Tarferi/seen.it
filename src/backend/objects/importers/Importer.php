<?php

abstract class Importer {
	private $reimport;

	public function isReImporting() {
		return $this->reimport;
	}

	public function __construct($reimport = false) {
		$this->reimport = $reimport;
	}

	/**
	 *
	 * @return ImporterMovieDetailsCollection
	 */
	public abstract function getMovieDetailsById($id, $resolveOthers);

	/**
	 *
	 * @param unknown $id
	 * @return Movie
	 */
	public function getFullMovieById($id) {
		$mv = $this->getMovieDetailsById ( $id, true );
		if ($mv) {
			if ($mv->getCSFDObj () || $mv->getIMDBObj ()) {
				$csfd = $mv->getCSFDObj ();
				$imdb = $mv->getIMDBObj ();
				$verified = true;
				if (! $csfd) {
					$csfd = $this->getEmptyDetails ( $imdb );
					$verified = false;
				} else if (! $imdb) {
					$imdb = $this->getEmptyDetails ( $csfd );
					$verified = false;
				}
				$movie = new Movie ( array (
						"csfdId" => $csfd->getID (),
						"descriptionCZ" => $csfd->getDescription (),
						"descriptionEN" => $imdb->getDescription (),
						"movieNameEN" => $imdb->getTitle (),
						"movieNameCZ" => $csfd->getTitle (),
						"imdbId" => $imdb->getID (),
						"releaseYear" => $csfd->getYear () == null ? ($imdb->getYear () == null ? "0" : $imdb->getYear ()) : $csfd->getYear (),
						"dateAdded" => time (),
						"verified" => $verified 
				) );
				$poster = $imdb->getPosterURL () != null ? $imdb->getPosterURL () : $csfd->getPosterURL ();
				$movie->setVerified ( $verified );
				if ($verified) {
					if ($m = Movie::getByCsfdID ( $csfd->getID () )) {
						// if (! $m->isVerified ()) {
						if (! $this->reimport) {
							$m->setVerified ( true );
							$m->updateDataFromAnotherObject ( $movie );
							$m->savePoster ( $poster );
							$m->save ();
						} else if ($this->reimport) {
							$m->updateDataFromAnotherObject ( $movie );
							$m->savePoster ( $poster );
							$m->save ();
						}
					} else if ($m = Movie::getByImdbID ( $imdb->getID () )) {
						// if (! $m->isVerified ()) {
						if (! $this->reimport) {
							$m->setVerified ( true );
							$m->updateDataFromAnotherObject ( $movie );
							$m->savePoster ( $poster );
							$m->save ();
						} else if ($this->reimport) {
							$m->updateDataFromAnotherObject ( $movie );
							$m->savePoster ( $poster );
							$m->save ();
						}
					} else {
						if (! $this->reimport) {
							$movie->create ();
							if ($movie->detectID ()) {
								$movie->savePoster ( $poster );
							} else {
								$movie->saveDefaultPoster ();
							}
						}
					}
				} else {
					if (! $this->reimport) {
						$movie->create ();
						if ($movie->detectID ()) {
							$movie->savePoster ( $poster );
						} else {
							$movie->saveDefaultPoster ();
						}
					} else {
						if (($m = Movie::getByCsfdID ( $csfd->getID () ) && $csfd->getID ()) != 0) {
							$m->updateDataFromAnotherObject ( $movie );
							$m->savePoster ( $poster );
							$m->save ();
						} else if (($m = Movie::getByImdbID ( $imdb->getID () )) && $imdb->getID () != 0) {
							$m->updateDataFromAnotherObject ( $movie );
							$m->savePoster ( $poster );
							$m->save ();
						}
					}
				}
			}
		}
	}

	private function getEmptyDetails($clone) {
		return new ImportedMovieDetails ( "0", $clone->getTitle (), $clone->getYear (), $clone->getDescription (), $clone->getPosterURL () );
	}

}

class ImporterMovieDetailsCollection {
	private $csfdobj;
	private $imdbobj;

	/**
	 *
	 * @return ImportedMovieDetails
	 */
	public function getCSFDObj() {
		return $this->csfdobj;
	}

	/**
	 *
	 * @return ImportedMovieDetails
	 */
	public function getIMDBObj() {
		return $this->imdbobj;
	}

	public function __construct($csfdobj, $imdbobj) {
		$this->csfdobj = $csfdobj;
		$this->imdbobj = $imdbobj;
	}

}

class ImportedMovieDetails {
	private $id;
	private $title;
	private $year;
	private $description;
	private $poster;

	public function getID() {
		return $this->id;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getYear() {
		return $this->year;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getPosterURL() {
		return $this->poster;
	}

	public function __construct($id, $title, $year, $description, $poster) {
		$this->id = $id;
		$this->title = $title;
		$this->year = $year;
		$this->description = $description;
		$this->poster = $poster;
	}

}