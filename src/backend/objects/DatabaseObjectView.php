<?php

class DatabaseViewStructure {
	
	/**
	 *
	 * @var Array of Columns
	 */
	private $selects = array ();
	
	/**
	 *
	 * @var Array of Tables
	 */
	private $fromTables = array ();
	
	/**
	 *
	 * @var Restriction
	 */
	private $restrictions = array ();
	
	/**
	 *
	 * @var Number
	 */
	private $limit = false;
	
	/**
	 *
	 * @var Column
	 */
	private $groupBy = false;
	
	/**
	 *
	 * @var Restriction
	 */
	private $having = false;
	
	/**
	 *
	 * @var Column
	 */
	private $order = false;
	private $orderType = DatabaseObject::ORDER_ASC;

	public function addSelect($selectColumn, $selectAsName) {
		$this->selects [$selectAsName] = $selectColumn;
	}

	public function addFromTable($table) {
		$this->fromTables [] = $table;
	}

	public function setGroupBy($col) {
		$this->groupBy = $col;
	}

	public function addRegistriction($restr) {
		$this->restrictions [] = $restr;
	}

	public function setOrder($column, $order) {
		$this->order = $column;
		$this->orderType = $order;
	}

}

class DatabaseViewTable {
	private $tableName;
	private $joins = array ();

	public function __construct($tableName) {
		$this->tableName = $tableName;
	}

	public function addJoin($join) {
		$this->joins [] = $join;
	}

}

class DatabaseViewValueRestriction {
	private $comparator;
	private $lvalue;
	private $rvalue;

	public function getLValue() {
		return $this->lvalue;
	}

	public function getRValue() {
		return $this->rvalue;
	}

	public function getComparator() {
		return $this->comparator;
	}

	public function __construct($lvalue, $rvalue, $comparator) {
		$this->comparator = $comparator;
		$this->lvalue = $lvalue;
		$this->rvalue = $rvalue;
	}

}

class DatabaseViewColumn {
	private $table;
	private $columnName;
	private $restriction;

	public function getTable() {
		return $this->table;
	}

	public function getColumnName() {
		return $this->columnName;
	}

	public function __construct($table, $columnName) {
		$this->table = $table;
		$this->columnName = $columnName;
	}

}

class DatabaseViewJoin {
	private $fromTable;
	private $toTable;
	private $joinName;
	private $fromTableColumn;
	private $toTableColumn;

	public function getFromTable() {
		return $this->fromTable;
	}

	public function getToTable() {
		return $this->toTable;
	}

	public function getJoinName() {
		return $this->joinName;
	}

	public function getFromTableColumn() {
		return $this->fromTableColumn;
	}

	public function getToTableColumn() {
		return $this->toTableColumn;
	}

	public function __construct($fromTable, $toTable, $joinName, $fromTableColumn, $toTableColumn) {
		$this->fromTable = $fromTable;
		$this->toTable = $toTable;
		$this->joinName = $joinName;
		$this->fromTableColumn = $fromTableColumn;
		$this->toTableColumn = $toTableColumn;
	}

}

abstract class DatabaseObjectView {

	protected abstract function getCreateSQL(&$query, &$replacements);

	protected static function classNameToViewTableName($className) {
		return Config::DATAASE_TABLE_PREFIX . "view_" . strtolower ( $className );
	}

	protected static function classNameToTableName($className) {
		return Config::DATAASE_TABLE_PREFIX . strtolower ( $className );
	}

	private static function getDerivedTables() {
		$objects = array ();
		$cls = get_declared_classes ();
		foreach ( $cls as $class ) {
			if (is_subclass_of ( $class, "DatabaseObjectView" )) {
				$rc = new ReflectionAnnotatedClass ( $class );
				if ((! $rc->hasAnnotation ( "Protect" )) || Config::DEBUG_OVERRIDE_PROTECT) {
					if (! $rc->hasAnnotation ( "Virtual" )) {
						$objects [] = $class;
					}
				}
			}
		}
		return $objects;
		/*
		 * $tables = array (); $struct = new DatabaseViewStructure (); foreach ( $objects as $obj ) { $table = new DatabaseTable ( static::classNameToViewName ( $obj ) ); $rc = new ReflectionAnnotatedClass ( $obj ); $props = $rc->getProperties (); foreach ( $props as $prop ) { if ($prop->hasAnnotation ( "Table" )) { $tableName = $prop->getAnnotation ( "Table" )->value; $table = new DatabaseViewTable ( $tableName ); $joins = $prop->getAllAnnotations ( "Joins" ); foreach ( $joins as $join ) { $from = explode ( ".", $join->value [0] ); $FromName = $from [0]; $FromColname = $from [1]; $to = explode ( ".", $join->value [1] ); $ToName = $to [0]; $ToColname = $to [1]; $table->addJoin ( new DatabaseViewJoin ( $FromName, $ToName, "JOIN", $FromColname, $ToColname ) ); } $struct->addFromTable ( $table ); } else if ($prop->hasAnnotation ( "From" )) { $from = $prop->getAnnotation ( "From" )->value; $from = explode ( ".", $from ); $FromName = $from [0]; $FromColname = $from [1]; $column = new DatabaseViewColumn ( $FromName, $FromColname ); $struct->addSelect ( $column, $prop->getName () ); if ($prop->hasAnnotation ( "GroupBy" )) { $struct->setGroupBy ( $column ); } if ($prop->hasAnnotation ( "Restriction" )) { $restrictions = $prop->getAllAnnotations ( "Restriction" ); foreach ( $restrictions as $restriction ) { $comparator = $restriction->value [0]; $lvalue = $column; $rvalue = $restriction->value [1]; $rest = new DatabaseViewValueRestriction ( $lvalue, $rvalue, $comparator ); $struct->addRegistriction ( $rest ); } } if ($prop->hasAnnotation ( "Order" )) { $order = $prop->getAnnotation ( "Order" )->value; $struct->setOrder ( $column, $order == "ASC" ? DatabaseObject::ORDER_ASC : DatabaseObject::ORDER_DESC ); } } } } return $struct;
		 */
	}

	public static function createViews() {
		$objects = static::getDerivedTables ();
		$db = Config::getDatasource ()->getSource ();
		foreach ( $objects as $object ) {
			$queries = array ();
			$replacements = array ();
			$queries [] = "CREATE VIEW ? AS";
			$replacements [] = static::classNameToViewTableName ( $object );
			$rc = new ReflectionClass ( $object );
			$instance = $rc->newInstance ();
			$instance->getCreateSQL ( $queries, $replacements );
			$queries = implode ( " ", $queries );
			$db->executeQuery ( $queries, $replacements );
		}
	}

	public static function removeViews() {
		$objects = static::getDerivedTables ();
		$queries = array ();
		$replacements = array ();
		foreach ( $objects as $object ) {
			$queries [] = "DROP VIEW ?";
			$replacements [] = static::classNameToViewTableName ( $object );
		}
		$queries = implode ( " ", $queries );
		if (count ( $replacements ) > 0) {
			$db = Config::getDatasource ()->getSource ();
			$db->executeQuery ( $queries, $replacements );
		}
	}

}