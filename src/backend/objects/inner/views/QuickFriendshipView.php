<?php

/**
 * @Protect
 * @Virtual
 * @author Tom
 *
 */
class QuickFriendShipView extends DatabaseObjectView {
	private $friendName;
	private $friendID;
	private $friendShipID;
	private $friendAccepted;
	private $meAccepted;

	public function getFriendUsername() {
		return $this->friendName;
	}

	public function getFriendID() {
		return $this->friendID;
	}

	public function getFriendShipID() {
		return $this->friendShipID;
	}

	public function friendHasAccepted() {
		return $this->friendAccepted == FriendShip::HAS_ACCEPTED;
	}

	public function friendHasRejected() {
		return $this->friendAccepted == FriendShip::HAS_REJECTED;
	}

	public function friendPending() {
		return $this->friendAccepted == FriendShip::HAS_REQUEST;
	}

	public function meAccepted() {
		return $this->meAccepted == FriendShip::HAS_ACCEPTED;
	}

	public function meRejected() {
		return $this->meAccepted == FriendShip::HAS_REJECTED;
	}

	public function mePending() {
		return $this->meAccepted == FriendShip::HAS_REQUEST;
	}

	protected function getCreateSQL(&$query, &$replacements) {
		return false;
	}

	private function __construct($row) {
		$this->friendAccepted = $row ["FriendAccepted"];
		$this->meAccepted = $row ["MeAccepted"];
		$this->friendID = $row ["FriendID"];
		$this->friendName = $row ["FriendUsername"];
		$this->friendShipID = $row ["FriendshipID"];
	}

	public static function areFriendsById($user1Id, $user2Id) {
		$friendShip = parent::classNameToTableName ( "Friendship" );
		$user = parent::classNameToTableName ( "User" );
		$res = Config::getDatasource ()->getSource ()->selectQuery ( "
				SELECT $user.username as FriendUsername, FS.*
				FROM
				(SELECT id as FriendshipID, User1Id as FriendID, User1Accepted as FriendAccepted, User2Accepted as MeAccepted FROM $friendShip WHERE User2Id = ? AND User1Id = ?
				UNION
				SELECT id as FriendshipID, User2Id as FriendID, User2Accepted as FriendAccepted, User1Accepted as MeAccepted FROM $friendShip WHERE User1Id = ? AND User2Id = ?) as FS
				,$user
				WHERE FS.FriendID = $user.id", array (
				$user1Id,
				$user2Id,
				$user1Id,
				$user2Id 
		) );
		if (is_array ( $res )) {
			if (count ( $res ) == 1) {
				$d = ( array ) $res [0];
				$u1accept = $d ["FriendAccepted"];
				$u2accept = $d ["MeAccepted"];
				return $u1accept == FriendShip::HAS_ACCEPTED && $u2accept = FriendShip::HAS_ACCEPTED;
			}
		}
		return false;
	}

	public static function getUsersFriendships($user, $userIsOnlyID = false) {
		$userId = $userIsOnlyID ? $user : $user->getID ();
		$friendShip = parent::classNameToTableName ( "Friendship" );
		$user = parent::classNameToTableName ( "User" );
		$res = Config::getDatasource ()->getSource ()->selectQuery ( "
				SELECT $user.username as FriendUsername, FS.*
				FROM
					(SELECT id as FriendshipID, User1Id as FriendID, User1Accepted as FriendAccepted, User2Accepted as MeAccepted FROM $friendShip WHERE User2Id = ?
						UNION
				SELECT id as FriendshipID, User2Id as FriendID, User2Accepted as FriendAccepted, User1Accepted as MeAccepted FROM $friendShip WHERE User1Id = ?) as FS
						,$user
				WHERE FS.FriendID = $user.id", array (
				$userId,
				$userId 
		) );
		if (! is_array ( $res )) {
			$res = array ();
		}
		$ret = array ();
		foreach ( $res as $r ) {
			$ret [] = new QuickFriendShipView ( ( array ) $r );
		}
		return $ret;
	}

}