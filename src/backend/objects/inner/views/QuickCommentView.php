<?php

/**
 * 
 * @author Tom
 *
 */
class QuickCommentView extends DatabaseObjectView {
	private $CommentID;
	private $MovieID;
	private $CreatorID;
	private $CreatorName;
	private $CommentTitle;
	private $CommentContent;
	private $isDeleted;
	private $AddedDate;
	private $CreatorUsername;

	public function getCommentID() {
		return $this->CommentID;
	}

	public function getMovieID() {
		return $this->MovieID;
	}

	public function getCreatorID() {
		return $this->CreatorID;
	}

	public function getCreatorName() {
		return $this->CreatorName;
	}

	public function getCommentTitle() {
		return $this->CommentTitle;
	}

	public function getCommentContent() {
		return $this->CommentContent;
	}

	public function isDeleted() {
		return $this->isDeleted == "1";
	}

	public function getAddedDate() {
		return $this->AddedDate;
	}

	public function getCreatorUsername() {
		return $this->CreatorUsername;
	}

	public static function getForMovieID($movieId, $evenDeleted = false) {
		if ($evenDeleted) {
			$res = Config::getDatasource ()->getSource ()->selectQuery ( "SELECT * FROM ? WHERE MovieID = ?", array (
					static::classNameToViewTableName ( get_called_class () ),
					$movieId
			) );
		} else {
			$res = Config::getDatasource ()->getSource ()->selectQuery ( "SELECT * FROM ? WHERE MovieID = ? AND CommentDeleted = ?", array (
					static::classNameToViewTableName ( get_called_class () ),
					$movieId,
					Comment::NOT_DELETED
			) );
		}
		if (! is_array ( $res )) {
			$res = array ();
		}
		$ret = array ();
		foreach ( $res as $r ) {
			$ret [] = new QuickCommentView ( ( array ) $r );
		}
		return $ret;
	}

	private function __construct($row) {
		$this->CommentID = $row ["CommentID"];
		$this->MovieID = $row ["MovieID"];
		$this->CreatorID = $row ["CreatorID"];
		$this->CommentContent = $row ["CommentContent"];
		$this->CommentTitle = $row ["CommentTitle"];
		$this->isDeleted = $row ["CommentDeleted"];
		$this->AddedDate = $row ["CommentAddDate"];
		$this->CreatorUsername = $row ["CommentCreatorName"];
	}

	public function getCreateSQL(&$query, &$replacements) {
		$comment = parent::classNameToTableName ( "Comment" );
		$user = parent::classNameToTableName ( "User" );
		$query [] = "
		SELECT
			$comment.id as CommentID,
			$comment.movieId as MovieID,
			$comment.creatorId as CreatorID,
			$comment.content as CommentContent,
			$comment.title as CommentTitle,
			$comment.isDeleted as CommentDeleted,
			$comment.addDate as CommentAddDate,
			$user.username as CommentCreatorName
		FROM
			$comment,
			$user
		WHERE
			$comment.creatorId = $user.id
		";
	}

}