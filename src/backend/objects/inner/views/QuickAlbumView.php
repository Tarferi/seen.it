<?php

/**
 * @author Tom
 *
 */
class QuickAlbumView extends DatabaseObjectView {
	public function __construct() {
	
	}
	public static function searchForUser($userId) {
		$replacements = array ();
		$dbName = parent::classNameToViewTableName ( get_called_class () );
		$query = "SELECT
						*
					FROM
						$dbName
					WHERE
						AlbumOwnerID=?
					";
		$replacements [] = $userId;
		return Config::getDatasource ()->getSource ()->selectQuery ( $query, $replacements );
	}

	public static function searchForUserFriend($userId) {
		$replacements = array ();
		$dbName = parent::classNameToViewTableName ( get_called_class () );
		$friendship = parent::classNameToTableName ( "Friendship" );
		$query = "SELECT
						*
					FROM
						$dbName
					WHERE
						AlbumOwnerID IN (SELECT user1id as id FROM $friendship WHERE user2id=? AND user1Accepted=user2Accepted UNION SELECT user2id as id FROM $friendship WHERE user1id=? AND user1Accepted=user2Accepted)
					";
		$replacements [] = $userId;
		$replacements [] = $userId;
		return Config::getDatasource ()->getSource ()->selectQuery ( $query, $replacements );
	}

	/*
	 * Select invalid records causing invalid album contents count
	 * 
	 	SELECT
			seenit_albummovie.id as AID,
			seenit_movielist.movieId as MID
		FROM
			seenit_albummovie
		LEFT JOIN
			seenit_movielist
		ON seenit_albummovie.movielistId=seenit_movielist.id
		WHERE seenit_movielist.movieId is null
	 */
	
	public function getCreateSQL(&$query, &$replacements) {
		$album = parent::classNameToTableName ( "Album" );
		$albumMovie = parent::classNameToTableName ( "AlbumMovie" );
		$user = parent::classNameToTableName ( "User" );
		$query [] = "
			SELECT 
				$album.id as AlbumID,
				$album.name as AlbumName,
				$album.addDate as AlbumAddedDate,
				$album.ownerId as AlbumOwnerID,
				$album.accessFlags as AlbumAccessFlags,
				$user.username as OwnerUsername,
				$albumMovie.id as IsEmpty,
				count($album.id) as AlbumContentCount
			FROM 
				$album JOIN $user
			LEFT JOIN $albumMovie ON 
				$album.id = $albumMovie.albumId
			WHERE 
				$album.ownerId = $user.id 
			GROUP BY
				$album.id
		";
	}

}