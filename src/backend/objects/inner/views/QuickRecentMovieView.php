<?php

/**
 * @author Tom
 *
 */
class QuickRecentMovieView extends DatabaseObjectView {
	private $movieNameCZ;
	private $movieNameEN;
	private $movieID;
	private $movieReleaseYear;
	private $alreadySeen;
	private $seenDate;
	private $inList;
	private $posterURL;

	public function getPosterURL() {
		return $this->posterURL;
	}

	public function getMovieNameCZ() {
		return $this->movieNameCZ;
	}

	public function getMovieNameEN() {
		return $this->movieNameEN;
	}

	public function getMovieID() {
		return $this->movieID;
	}

	public function getReleaseYear() {
		return $this->movieReleaseYear;
	}

	public function hasSeen() {
		return $this->alreadySeen;
	}

	public function getSeenDate() {
		return $this->seenDate;
	}

	public function isInList() {
		return $this->inList;
	}

	public static function getForUser($user, $limit = 20) {
		$db = Config::getDatasource ()->getSource ();
		$dbName = parent::classNameToViewTableName ( get_called_class () );
		$res = $db->selectQuery ( "SELECT * FROM $dbName WHERE UserID = ? OR UserID IS NULL LIMIT ?", array (
				$user->getID (),
				$limit 
		) );
		$ret = array ();
		if ($res) {
			foreach ( $res as $r ) {
				$ret [] = new QuickRecentMovieView ( ( array ) $r );
			}
		}
		return $ret;
	}

	public function __construct($row = false) {
		if (is_array ( $row )) {
			$this->movieNameCZ = $row ["MovieNameCZ"];
			$this->movieNameEN = $row ["MovieNameEN"];
			$this->movieID = $row ["MovieID"];
			$this->movieReleaseYear = $row ["MovieReleaseYear"];
			$this->alreadySeen = $row ["AlreadySeen"];
			$this->inList = $this->alreadySeen == null ? false : true;
			$this->alreadySeen = $this->alreadySeen == null ? false : $this->alreadySeen == MovieList::HAS_SEEN ? true : false;
			$this->seenDate = $row ["SeenDate"];
			$this->posterURL = Movie::getPublicPosterURL ( $this->movieID );
		}
	}

	public function getCreateSQL(&$query, &$replacements) {
		$movie = parent::classNameToTableName ( "Movie" );
		$movieList = parent::classNameToTableName ( "MovieList" );
		$query [] = "
				SELECT
					$movie.movieNameCZ as MovieNameCZ,
					$movie.movieNameEN as MovieNameEN,
					$movie.id as MovieID,
					$movie.releaseYear as MovieReleaseYear,
					$movieList.alreadySeen as AlreadySeen,
					$movieList.seenDate as SeenDate,
					$movieList.userId as UserID
				FROM
				 	$movie LEFT JOIN $movieList ON $movie.id = $movieList.movieId ORDER BY $movie.dateAdded DESC
				";
	}

}