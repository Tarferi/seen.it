<?php

/**
 * 
 * @author Tom
 *
 */
class QuickAlbumMovieView extends DatabaseObjectView {

	public function __construct() {
	}

	public static function getRowsAlbumById($albumId, $userId, $isOwner = true, $count, $offset) {
		$replacements = array ();
		$dbName = parent::classNameToViewTableName ( get_called_class () );
		$dbUser = parent::classNameToTableName ( "User" );
		$dbMovieList = parent::classNameToTableName ( "MovieList" );
		$page = ($offset - 1) * $count;
		if ($isOwner) {
			$query = "SELECT *,AlreadySeen as UserSeen, SeenDate as UserSeenDate  FROM $dbName WHERE AlbumID=? LIMIT ? OFFSET ?";
			$replacements [] = $albumId;
			$replacements [] = $count;
			$replacements [] = $page;
		} else {
			$query = "
					SELECT
						T1.*,
						T2.alreadySeen as UserSeen,
						T2.seenDate as UserSeenDate,
						T2.id as UserMovieListID
					
					FROM
						$dbName as T1
					
					LEFT JOIN $dbMovieList  as T2
					
					ON T2.movieId =T1.MovieID
					
					AND
						T2.userId = ?
					WHERE
						T1.AlbumID = ?
					LIMIT ?
					OFFSET ?
					";
			$replacements [] = $userId;
			$replacements [] = $albumId;
			$replacements [] = $count;
			$replacements [] = $page;
		}
		return Config::getDatasource ()->getSource ()->selectQuery ( $query, $replacements );
	}

	public function getCreateSQL(&$query, &$replacements) {
		$album = parent::classNameToTableName ( "Album" );
		$albumMovie = parent::classNameToTableName ( "AlbumMovie" );
		$user = parent::classNameToTableName ( "User" );
		$movieList = parent::classNameToTableName ( "MovieList" );
		$movie = parent::classNameToTableName ( "Movie" );
		$query [] = "
				
				SELECT 
					$album.id as AlbumID, 
					$album.name as AlbumName, 
					$album.ownerId as OwnerID, 
					$albumMovie.id as AlbumMovieID, 
					$albumMovie.addedDate as AddedToAlbumDate, 
					$albumMovie.itemOrder as AlbumOrder, 
				
					$user.username as OwnerUsername, 
					$movieList.id as MovieListID, 
					$movieList.addDate as AddedToMovieListDate, 
					$movieList.alreadySeen as AlreadySeen, 
					$movieList.seenDate as SeenDate, 
					$movie.movieNameEN as MovieNameEN, 
					$movie.movieNameCZ as MovieNameCZ, 
					$movie.id as MovieID 
				
				FROM $album, $albumMovie, $user, $movieList, $movie
				
					WHERE
						$album.id = $albumMovie.albumId
					AND
						$album.ownerId = $user.id
					AND
						$albumMovie.movieListId = $movieList.id
					AND
						$movieList.userId = $user.id
					AND
						$movieList.movieId = $movie.id
				";
	}

}