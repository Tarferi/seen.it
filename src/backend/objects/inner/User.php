<?php

/**
 * @Protect
 * @author Tom
 *
 */
class User extends DatabaseObject {

	const PERMISSION_DEFAULT = 1;

	const PERMISSION_VISIBLE_ALL = 0;

	const PERMISSION_VISIBLE_FRIENDS = 1;

	const PERMISSION_VISIBLE_NOBODY = 2;

	const PERMISSION_SIZE = 4;

	const OFFSET_EMAIL = 0;

	const OFFSET_FNAME = 1;

	const OFFSET_LNAME = 2;

	const OFFSET_BDAY = 3;

	public static function getForName($name) {
		return static::searchMatch ( array (
				"username" => $name 
		), true );
	}

	public static function usernameValid($username) {
		if (strlen ( $username ) > 3) {
			if (strlen ( $username ) < 20) {
				$out = array ();
				preg_match ( "/\w+(\s\w+)*/", $username, $out );
				if (count ( $out ) == 1) {
					$out = $out [0];
					if ($out == $username) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private function getVisibilityFlag($flag) {
		$val = $this->detailsVisibilityFlags;
		$size = static::PERMISSION_SIZE;
		$val = ($val >> ($flag * $size));
		$val = $val & ((1 << $size) - 1);
		return $val;
	}

	private function clearFlag($offset) {
		$val = $this->detailsVisibilityFlags;
		$realOffset = static::PERMISSION_SIZE * ($offset + 1);
		$prefix = ($val >> $realOffset) << $realOffset;
		$imagineOffset = static::PERMISSION_SIZE * $offset;
		$suffix_help = ($val >> $imagineOffset) << $imagineOffset;
		$suffix = $val ^ $suffix_help;
		$nval = $prefix | $suffix;
		$this->detailsVisibilityFlags = $nval;
	}

	private function setFlag($offset, $value) {
		$this->clearFlag ( $offset );
		$val = $this->detailsVisibilityFlags;
		$realOffset = static::PERMISSION_SIZE * ($offset);
		$realValue = $value << $realOffset;
		$val |= $realValue;
		$this->detailsVisibilityFlags = $val;
	}

	private function setVisibilityFlag($flag, $value) {
		$this->clearFlag ( $flag );
		$this->setFlag ( $flag, $value );
	}

	public function getHighestRank() {
		$ranks = RankCollection::getForUser ( $this );
		return $ranks->getHighestRank ();
	}

	public static function getPermissionFromName($permName) {
		switch ($permName) {
			case "Only_My_Friends":
				return User::PERMISSION_VISIBLE_FRIENDS;
			break;
			case "Public":
				return User::PERMISSION_VISIBLE_ALL;
			break;
			case "Only_Me":
				return User::PERMISSION_VISIBLE_NOBODY;
			break;
			default :
				return User::PERMISSION_VISIBLE_NOBODY;
			break;
		}
	}

	public function getPermission() {
		return $this->permission;
	}

	public function setPermission($perm) {
		$this->permission = $perm;
	}

	private static function SgetAvatarLocation($username) {
		$fn = "assets/user_avatars/" . md5 ( "cassie_" . strtolower ( $username ) . "_iessac" ) . ".png";
		if (file_exists ( $fn )) {
			return $fn;
		} else {
			return "assets/user_avatars/default.png";
		}
	}

	public static function getAvatarURLForUsername($username) {
		return "/" . Config::FRONTEND_URL . "/" . static::SgetAvatarLocation ( $username );
	}

	public function saveAvatar($avatarFileName) {
		copy ( $avatarFileName, static::SgetAvatarLocation ( $this->username ) );
	}

	public function getAvatarLocation() {
		return static::SgetAvatarLocation ( $this->username );
	}

	private static function packPermission($offset, $permission, $permsize = User::PERMISSION_SIZE) {
		return $permission << ($permsize * $offset);
	}
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("text")
	 * @Unique
	 */
	private $username;
	
	/**
	 * @ColumnType("text")
	 */
	private $password;
	
	/**
	 * @ColumnType("text")
	 */
	private $lastip;
	
	/**
	 * @ColumnType("int")
	 */
	private $permission;
	
	/**
	 * @ColumnType("text")
	 *
	 * @var String
	 */
	private $firstName;
	
	/**
	 * @ColumnType("text")
	 *
	 * @var String
	 */
	private $lastName;
	
	/**
	 * @ColumnType("text")
	 *
	 * @var String
	 */
	private $email;
	
	/**
	 * @ColumnType("int")
	 *
	 * @var int
	 */
	private $detailsVisibilityFlags;
	
	/**
	 * @ColumnType("int")
	 *
	 * @var int
	 */
	private $birthday;

	public function getFirstName() {
		return $this->firstName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getBirthday() {
		return $this->birthday;
	}

	public function getVisibility($offset) {
		return $this->getVisibilityFlag ( $offset );
	}

	public function setFirstNameVisibility($vis) {
		$this->setVisibilityFlag ( static::OFFSET_FNAME, $vis );
	}

	public function setLastNameVisibility($vis) {
		$this->setVisibilityFlag ( static::OFFSET_LNAME, $vis );
	}

	public function setEmailVisibility($vis) {
		$this->setVisibilityFlag ( static::OFFSET_EMAIL, $vis );
	}

	public function setBdayVisibility($vis) {
		$this->setVisibilityFlag ( static::OFFSET_BDAY, $vis );
	}

	public function setFirstName($name) {
		$this->firstName = $name;
	}

	public function setLastName($name) {
		$this->lastName = $name;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function setBday($bday) {
		$this->birthday = $bday;
	}

	public function firstNameVisible($logged, $weAreFriends = false) {
		$vf = $this->getVisibilityFlag ( static::OFFSET_FNAME );
		if ($vf == static::PERMISSION_VISIBLE_ALL) {
			return true;
		} else {
			if ($vf == static::PERMISSION_VISIBLE_FRIENDS) {
				return $weAreFriends;
			} else {
				return false;
			}
		}
	}

	public function lastNameVisible($logged, $weAreFriends = false) {
		$vf = $this->getVisibilityFlag ( static::OFFSET_LNAME );
		if ($vf == static::PERMISSION_VISIBLE_ALL) {
			return true;
		} else {
			if ($vf == static::PERMISSION_VISIBLE_FRIENDS) {
				return $weAreFriends;
			} else {
				return false;
			}
		}
	}

	public function birthdayVisible($logged, $weAreFriends = false) {
		$vf = $this->getVisibilityFlag ( static::OFFSET_BDAY );
		if ($vf == static::PERMISSION_VISIBLE_ALL) {
			return true;
		} else {
			if ($vf == static::PERMISSION_VISIBLE_FRIENDS) {
				return $weAreFriends;
			} else {
				return false;
			}
		}
	}

	public function emailVisible($logged, $weAreFriends = false) {
		$vf = $this->getVisibilityFlag ( static::OFFSET_EMAIL );
		if ($vf == static::PERMISSION_VISIBLE_ALL) {
			return true;
		} else {
			if ($vf == static::PERMISSION_VISIBLE_FRIENDS) {
				return $weAreFriends;
			} else {
				return false;
			}
		}
	}

	public static function usernameExists($username) {
		return static::searchMatch ( array (
				"username" => $username 
		), true ) !== false;
	}

	public function getAlbumsCount() {
		return Album::getAlbumCountForUser ( $this );
	}

	public function getMoviesUnseen() {
		return MovieList::getUserUnseenCount ( $this );
	}

	public static function createUser($username, $password, $email, $firstName, $lastName, $birtdayStamp, $emailVis, $firstNameVis, $lastNameVis, $birthdayVis) {
		$user = new User ( array (
				"username" => $username,
				"password" => $password,
				"email" => $email,
				"lastip" => $_SERVER ["REMOTE_ADDR"],
				"permission" => User::PERMISSION_DEFAULT,
				"firstName" => $firstName,
				"lastName" => $lastName,
				"birthday" => $birtdayStamp,
				"detailsVisibilityFlags" => static::packPermission ( User::OFFSET_EMAIL, $emailVis ) | static::packPermission ( User::OFFSET_FNAME, $firstNameVis ) | static::packPermission ( User::OFFSET_LNAME, $lastNameVis ) | static::packPermission ( User::OFFSET_BDAY, $birthdayVis ) 
		) );
		$user->addRank ( Rank::RANK_USER );
		return $user->create ();
	}

	public function addRank($rank) {
		$ranks = RankCollection::getForUser ( $this );
		$ranks->addRank ( $rank );
	}

	public static function getByCredentials($username, $password) {
		return static::searchMatch ( array (
				"username" => $username,
				"password" => $password 
		), true );
	}

	public static function searchMatch($row, $single = false) {
		return parent::searchMatch ( get_called_class (), $row, $single );
	}

	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

	public function addMovieToList($movie, $seen = false) {
		return MovieList::newItem ( $this, $movie, $seen );
	}

	public function getMovieListItems() {
		return MovieList::getForUser ( $this );
	}

	public function setSeenMovie($user, $movie, $seen = MovieList::HAS_NOT_SEEN) {
		$ml = MovieList::getMovieForUser ( $this, $movie );
		if ($ml) {
			$ml->setSeen ( $seen );
		} else {
			$ml = MovieList::newItem ( $user, $movie, 0, $seen );
		}
		return $ml;
	}

	public function getID() {
		return $this->id;
	}

	public function likeMovie($movie, $grade) {
		Like::newLike ( $movie, $this, $grade );
	}

	public function addComment($movie, $comment) {
		Comment::newComment ( $movie, $this, $content );
	}

	public function getUsername() {
		return $this->username;
	}

	public function getPassword() {
		return $this->password;
	}

}

/**
 * @Protect
 *
 * @author Tom
 *        
 */
class FriendShip extends DatabaseObject {

	const HAS_REQUEST = 1;

	const HAS_ACCEPTED = 2;

	const HAS_REJECTED = 3;
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("int");
	 *
	 * @var int
	 */
	private $user1Id;
	
	/**
	 * @ColumnType("int");
	 *
	 * @var int
	 */
	private $user2Id;
	
	/**
	 * @ColumnType("int");
	 *
	 * @var int
	 */
	private $user1Accepted;
	
	/**
	 * @ColumnType("int");
	 *
	 * @var int
	 */
	private $user2Accepted;

	public static function getQuickFriendShipsForUser($user) {
		return QuickFriendShipView::getUsersFriendships ( $user );
	}

	public static function areFriendsById($user1Id, $user2Id) {
		return QuickFriendShipView::areFriendsById ( $user1Id, $user2Id );
	}

	public function meAccepted($user) {
		if ($this->user1Id == $user->getID ()) {
			return $this->user1Accepted == FriendShip::HAS_ACCEPTED;
		} else {
			return $this->user2Accepted == FriendShip::HAS_ACCEPTED;
		}
	}

	public function theyAccepted($user) {
		if ($this->user1Id == $user->getID ()) {
			return $this->user2Accepted == FriendShip::HAS_ACCEPTED;
		} else {
			return $this->user1Accepted == FriendShip::HAS_ACCEPTED;
		}
	}

	public function meRejected($user) {
		if ($this->user1Id == $user->getID ()) {
			return $this->user1Accepted == FriendShip::HAS_REJECTED;
		} else {
			return $this->user2Accepted == FriendShip::HAS_REJECTED;
		}
	}

	public function theyRejected($user) {
		if ($this->user1Id == $user->getID ()) {
			return $this->user2Accepted == FriendShip::HAS_REJECTED;
		} else {
			return $this->user1Accepted == FriendShip::HAS_REJECTED;
		}
	}

	public function theyAreID($userID) {
		if ($this->user1Id == $user->getID ()) {
			return $this->user2id = $userID;
		} else {
			return $this->user1id = $userID;
		}
	}

	public function IDsMatch($user1ID, $user2ID) {
		if ($this->user1Id == $user1ID) {
			return $user2Id = $user2ID;
		} else if ($this->user1Id == $user2ID) {
			return $user2Id = $user1ID;
		} else {
			return false;
		}
	}

	public function getThem($user) {
		if ($this->user1Id == $user->getID ()) {
			return User::getByID ( $this->user2Id, true );
		} else {
			return User::getByID ( $this->user1Id, true );
		}
	}

	public static function getAllFriendShips($user) {
		$arr1 = static::searchMatch ( array (
				"user1Id" => $user->getID () 
		) );
		$arr2 = static::searchMatch ( array (
				"user2Id" => $user->getID () 
		) );
		return array_merge ( $arr1, $arr2 );
	}

	public static function getFriendShip($user1, $user2) {
		$fr = static::searchMatch ( array (
				"user1Id" => $user1->getID (),
				"user2Id" => $user2->getID () 
		), true );
		if ($fr !== false) {
			return $fr;
		} else {
			$fr = static::searchMatch ( array (
					"user2Id" => $user1->getID (),
					"user1Id" => $user2->getID () 
			), true );
			if ($fr !== false) {
				return $fr;
			} else {
				return false;
			}
		}
	}

	public static function newFriendShip($user1, $user2, $user1Accepted, $user2Accepted) {
		$fs = static::getFriendShip ( $user1, $user2 );
		if ($fs) {
			if ($fs->user1Id == $user1->getID ()) {
				$fs->user1Accepted = $user1Accepted;
				$fs->user2Accepted = $user2Accepted;
			} else {
				$fs->user2Accepted = $user1Accepted;
				$fs->user1Accepted = $user2Accepted;
			}
			$fs->save ();
			return $fs;
		} else {
			$fs = new FriendShip ( array (
					"user1Id" => $user1->getID (),
					"user2Id" => $user2->getID (),
					"user1Accepted" => $user1Accepted,
					"user2Accepted" => $user2Accepted 
			) );
			$fs->create ();
		}
		return $fs;
	}

	public function setUserAccept($user, $accept = true) {
		if ($this->user1Id == $user->getID ()) {
			$this->user1Accepted = $accept ? FriendShip::HAS_ACCEPTED : FriendShip::HAS_REJECTED;
		} else {
			$this->user2Accepted = $accept ? FriendShip::HAS_ACCEPTED : FriendShip::HAS_REJECTED;
		}
		$this->save ();
	}

	public function getID() {
		return $this->id;
	}

	public static function searchMatch($row, $single = false) {
		return parent::searchMatch ( get_called_class (), $row, $single );
	}

	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

}
