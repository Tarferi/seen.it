<?php

/**
 * Represents album of movies of user
 * @author Tom
 * @Protect
 *
 */
class Album extends DatabaseObject {

	/**
	 * Available to me only exclusively
	 */
	const A_PRIVATE = 0;

	/**
	 * Available to even not logged users
	 */
	const A_SHARED = 1;

	/**
	 * Available to all logged user
	 */
	const A_PUBLIC = 2;

	/**
	 * Available to all friends
	 */
	const A_FRIENDS = 3;

	const A_SIZE = 4;

	const F_DISPLAY = 0;

	const F_ADDABLE = 4;

	const F_REMOVABLE = 8;

	const F_BLOCKED = 12;

	const A_DEFAULT = Album::A_PRIVATE;

	public static function getAlbumsForUser($user) {
		return static::searchMatch ( array (
				"ownerId" => $user->getID () 
		), false );
	}

	private static function getFlag($albumFlags, $offset) {
		$val = $albumFlags;
		$size = static::A_SIZE;
		$val = ($val >> ($offset * $size));
		$val = $val & ((1 << $size) - 1);
		return $val;
	}

	private function clearFlag($offset) {
		$val = $this->accessFlags;
		$realOffset = static::A_SIZE * ($offset + 1);
		$prefix = ($val >> $realOffset) << $realOffset;
		$imagineOffset = static::A_SIZE * $offset;
		$suffix_help = ($val >> $imagineOffset) << $imagineOffset;
		$suffix = $val ^ $suffix_help;
		$nval = $prefix | $suffix;
		$this->accessFlags = $nval;
	}

	private function setFlag($offset, $value) {
		$this->clearFlag ( $offset );
		$val = $this->accessFlags;
		$realOffset = static::A_SIZE * ($offset);
		$realValue = $value << $realOffset;
		$val |= $realValue;
		$this->accessFlags = $val;
	}

	public function setPrivate() {
		$this->setFlag ( static::F_DISPLAY, static::A_PRIVATE );
	}

	public function setFriends() {
		$this->setFlag ( static::F_DISPLAY, static::A_FRIENDS );
	}

	public function setPublic() {
		$this->setFlag ( static::F_DISPLAY, static::A_PUBLIC );
	}

	public function setVisibility($flag) {
		$this->setFlag ( static::F_DISPLAY, $flag );
	}

	public static function getVisibility($albumFlags) {
		return static::getFlag ( $albumFlags, Album::F_DISPLAY );
	}

	public static function canSee($albumFlags, $logged, $areFriends) {
		$fl = static::getVisibility ( $albumFlags );
		if ($fl == Album::A_PUBLIC) {
			return true;
		} else {
			if ($fl == Album::A_SHARED) {
				return $logged;
			} else {
				if ($fl == Album::A_FRIENDS) {
					return $areFriends;
				} else {
					return false;
				}
			}
		}
	}
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("int")
	 */
	private $ownerId;
	
	/**
	 * @ColumnType("text")
	 */
	private $name;
	
	/**
	 * @ColumnType("int")
	 */
	private $addDate;
	
	/**
	 * @ColumnType("int")
	 */
	private $accessFlags;

	public function getID() {
		return $this->id;
	}

	public function setName($name) {
		$this->name = $name;
		$this->save ();
	}

	public function getName() {
		return $this->name;
	}

	public function getOwnerID() {
		return $this->ownerId;
	}

	/**
	 *
	 * @return Album
	 */
	public static function createAlbum($user, $albumName) {
		$arData = array (
				"ownerId" => $user->getID (),
				"name" => $albumName,
				"accessFlags" => Album::A_DEFAULT,
				"addDate" => time () 
		);
		$album = new Album ( $arData );
		$album->create ();
		$album = static::searchMatch ( $arData, true, "=", array (
				"id" => DatabaseObject::ORDER_DESC 
		) );
		return $album;
	}

	public static function getAlbumCountForUser($user) {
		$sm = Album::searchMatch ( array (
				"ownerId" => $user->getID () 
		) );
		if ($sm !== false) {
			return count ( $sm );
		} else {
			return 0;
		}
	}

	public function removeMovieListFromAlbum($movieList) {
		$ms = AlbumMovie::getByAlbumAndMovieList ( $this, $movieList );
		if ($ms) {
			$ms->delete ();
		}
	}

	public static function newAlbum($user, $name) {
		$album = new Album ( array (
				"targetId" => $movie->getID (),
				"ownerId" => $user->getID (),
				"name" => $name,
				"addDate" => time () 
		) );
		$album->create ();
		return $album;
	}

	public static function searchMatch($row, $single = false, $comparator = "=", $orders = array(), $limit = false) {
		return parent::searchMatch ( get_called_class (), $row, $single, $comparator, $orders, $limit );
	}

	public function userCanView($user) {
		if ($user->getID () == $this->ownerId) {
			return true;
		} else {
			$flags = static::getVisibility ( $this->accessFlags );
			if ($flags == Album::A_PUBLIC) {
				return true;
			} else {
				if ($flags == Album::A_SHARED) {
					return true;
				} else {
					if ($flags == Album::A_FRIENDS) {
						return FriendShip::areFriendsById ( $user->getID (), $this->ownerId );
					} else {
						return false;
					}
				}
			}
		}
	}

	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

}

/**
 * Quick object that is never saved to database
 *
 * @Protect We do NEVER want to create this table
 * @Virtual Same as above
 *
 * @author Tom
 */
class QuickAlbumDetails extends DatabaseObject {
	private $albumId;
	private $albumName;
	private $albumData;
	private $albumFlags;
	private $albumContentCount;
	private $ownerName;
	private $ownerID;

	public function getAlbumID() {
		return $this->albumId;
	}

	public function isShared() {
		return Album::getVisibility ( $this->albumFlags ) == Album::A_SHARED;
	}

	public function isPrivate() {
		return Album::getVisibility ( $this->albumFlags ) == Album::A_PRIVATE;
	}

	public function isPublic() {
		return Album::getVisibility ( $this->albumFlags ) == Album::A_PUBLIC;
	}

	public function isSharedBetweenFriends() {
		return Album::getVisibility ( $this->albumFlags ) == Album::A_FRIENDS;
	}

	public function canSee($logged, $areFriends) {
		return Album::canSee ( $this->albumFlags, $logged, $areFriends );
	}

	/**
	 *
	 * @return multitype:Movie
	 */
	public function getAllMovies() {
		$movies = AlbumMovie::getAlbumMoviesForID ( $this->albumId );
		if (is_array ( $movies )) {
			return $movies;
		}
		return array ();
	}

	public function getOwnerID() {
		return $this->ownerID;
	}

	public function getAlbumName() {
		return $this->albumName;
	}

	public function getAlbumData() {
		return $this->albumData;
	}

	public function getAlbumFlags() {
		return $this->albumFlags;
	}

	public function getAlbumContentCount() {
		return $this->albumContentCount;
	}

	public function getOwnerName() {
		return $this->ownerName;
	}

	public function __construct($row) {
		$this->albumId = $row ["AlbumID"];
		$this->albumName = $row ["AlbumName"];
		$this->albumData = $row ["AlbumAddedDate"];
		$this->albumFlags = $row ["AlbumAccessFlags"];
		$empty = $row ["IsEmpty"] == null;
		$this->albumContentCount = $empty ? 0 : $row ["AlbumContentCount"];
		$this->ownerName = $row ["OwnerUsername"];
		$this->ownerID = $row ["AlbumOwnerID"];
	}

	public function getQuickAlbumContents() {
		return QuickAlbumMovie::getAlbumMoviesForID ( $this->albumId );
	}

	private static function parseResult($result, $own) {
		$ra = array ();
		if ($result) {
			if (is_array ( $result )) {
				foreach ( $result as $rs ) {
					$ra [] = new QuickAlbumDetails ( ( array ) $rs );
				}
			}
		}
		return $ra;
	}

	public static function getAllForUserFriends($user) {
		return static::parseResult ( QuickAlbumView::searchForUserFriend ( $user->getID () ), false );
	}

	public static function getAllForUser($user) {
		return static::parseResult ( QuickAlbumView::searchForUser ( $user->getID () ), true );
	}

}