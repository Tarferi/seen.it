<?php
/**
 * @Protect
 * @author Tom
 *
 */
class Like {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("int")
	 */
	private $creatorId;
	
	/**
	 * @ColumnType("int")
	 */
	private $targetId;
	
	/**
	 * @ColumnType("int")
	 */
	private $grade;

	/**
	 * @ColumnType("int")
	 */
	private $addDate;
	
	public function getID() {
		return $this->id;
	}
	
	public static function newLike($movie, $user, $grade) {
		$like=new Like( array (
				"targetId" => $movie->getID (),
				"creatorId" => $user->getID (),
				"grade" => $grade,
				"addDate" => time ()
		) );
		$like->create();
		return $like;
	}
	
	public static function searchMatch($row, $single = false) {
		return parent::searchMatch ( get_called_class (), $row, $single );
	}
	
	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id
		), $single );
	}
	
}