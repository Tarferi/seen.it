<?php

/**
 * Represents movie details in user album
 * @author Tom
 * @Protect
 *
 */
class AlbumMovie extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("int")
	 */
	private $movieListId;
	
	/**
	 * @ColumnType("int")
	 */
	private $albumId;
	
	/**
	 * @ColumnType("int")
	 */
	private $addedDate;
	
	/**
	 * @ColumnType("int")
	 */
	private $itemOrder;

	public function getID() {
		return $this->id;
	}

	public static function deleteMovieList($movieListId) {
		$db = Config::getDatasource ()->getSource ();
		$db->executeQuery ( "DELETE FROM `?` WHERE movieListId=?", array (
				parent::classNameToTableName ( get_called_class () ),
				$movieListId 
		) );
	}

	public static function deleteMovieListArr($movieListAr) {
		foreach ( $movieListAr as $movieList ) {
			static::deleteMovieList ( $movieList->getID () );
			$movieList->delete ();
		}
	}

	public function deleteFromAlbum($album, $movieList) {
		$db = Config::getDatasource ()->getSource ();
		$db->executeQuery ( "DELETE FROM `?` WHERE movieListId=? AND albumId=?", array (
				parent::classNameToTableName ( get_called_class () ),
				$movieList->getID (),
				$album->getID () 
		) );
	}

	public static function deleteMovies($album, $movieListAr) {
		foreach ( $movieListAr as $movieList ) {
			static::deleteFromAlbum ( $album, $movieList );
		}
	}

	public static function moveMovies($fromAlbum, $toAlbum, $movieList) {
		$ok = 0;
		$fid = $fromAlbum->getID ();
		$tid = $toAlbum->getID ();
		foreach ( $movieList as $mlist ) {
			$db = Config::getDatasource ()->getSource ();
			$db->executeQuery ( "UPDATE `?` SET albumId = ? WHERE albumId = ? AND movieListId = ?", array (
					parent::classNameToTableName ( get_called_class () ),
					$tid,
					$fid,
					$mlist->getID () 
			) );
		}
		return $ok;
	}

	public static function addMovies($album, $movieList) {
		$ok = 0;
		foreach ( $movieList as $mlist ) {
			if (! static::getByAlbumAndMovieList ( $album, $mlist )) {
				static::newItem ( $album, $mlist );
				$ok ++;
			}
		}
		return $ok;
	}

	public static function deleteAllFromAlbum($album) {
		$db = Config::getDatasource ()->getSource ();
		$db->executeQuery ( "DELETE FROM `?` WHERE albumId=?", array (
				parent::classNameToTableName ( get_called_class () ),
				$album->getID () 
		) );
	}

	public static function getAlbumMoviesForID($albumId) {
		return static::searchMatch ( array (
				"albumId" => $albumId 
		), false, "=", array (
				"itemOrder" => DatabaseObject::ORDER_DESC 
		) );
	}

	public static function getAlbumMovies($album) {
		return static::getAlbumMoviesForID ( $album->getID () );
	}

	public static function getByAlbumAndMovieList($album, $movieList) {
		return static::searchMatch ( array (
				"albumId" => $album->getID (),
				"movieListId" => $movieList->getID () 
		), true );
	}

	public static function newItem($album, $movieList, $addDate = null) {
		$addDate = $addDate == null ? time () : $addDate;
		$highestItemOrder = static::searchMatch ( array (
				"albumId" => $album->getID () 
		), true, "=", array (
				"itemOrder" => DatabaseObject::ORDER_DESC 
		) );
		$nextOrder = 0;
		if ($highestItemOrder) {
			$nextOrder = $highestItemOrder->itemOrder + 1;
		}
		$item = new AlbumMovie ( array (
				"movieListId" => $movieList->getID (),
				"albumId" => $album->getID (),
				"addedDate" => $addDate,
				"itemOrder" => "" . $nextOrder 
		) );
		$item->create ();
		return $item;
	}

	public static function searchMatch($row, $single = false, $comparator = "=", $orders = array()) {
		return parent::searchMatch ( get_called_class (), $row, $single );
	}

	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

}

/**
 * @Protect Never to create a table
 * @Virtual Same as above
 *
 * @author Tom
 *        
 */
class QuickAlbumMovie extends DatabaseObject {
	private $albumId;
	private $movieId;
	private $albumListId;
	private $order;
	private $albumOwnerId;
	private $albumOwnerUsername;
	private $movieName;
	private $seen;
	private $seenDate;
	private $movieListId;
	private $isInList;

	public function getAlbumID() {
		return $this->albumId;
	}

	public function getMovieID() {
		return $this->movieId;
	}

	public function getAlbumListID() {
		return $this->albumListId;
	}

	public function getOrder() {
		return $this->order;
	}

	public function getAlbumOwnerID() {
		return $this->albumOwnerId;
	}

	public function getAlbumOwnerUsername() {
		return $this->albumOwnerUsername;
	}

	public function getMovieName() {
		return $this->movieName;
	}

	public function hasSeen() {
		return $this->seen == MovieList::HAS_SEEN;
	}

	public function isInList() {
		return $this->isInList;
	}

	public function getSeenDate() {
		return $this->seenDate;
	}

	public function getMovieListtID() {
		return $this->movieListId;
	}

	public function __construct($row, $isOwner) {
		$this->albumId = $row ["AlbumID"];
		$this->movieId = $row ["MovieID"];
		$this->albumListId = $row ["AlbumMovieID"];
		$this->order = $row ["AlbumOrder"];
		$this->albumOwnerId = $row ["OwnerID"];
		$this->albumOwnerUsername = $row ["OwnerUsername"];
		$this->movieName = $row ["MovieNameEN"];
		if ($isOwner) {
			$this->seen = $row ["AlreadySeen"];
			$this->seenDate = $row ["SeenDate"];
			$this->movieListId = $row ["MovieListID"];
		} else {
			$this->seen = $row ["UserSeen"];
			$this->seenDate = $row ["UserSeenDate"];
			$this->movieListId = $row ["UserMovieListID"];
		}
		$this->isInList = $this->seen != null;
	}

	public static function getAlbumMoviesForID($albumID, $userID, $count, $offset, &$totalCount) {
		$db = Config::getDatasource ()->getSource ();
		$album = Album::getByID ( $albumID, true );
		$isOwner = false;
		if ($album) {
			$isOwner = $album->getOwnerID () == $userID;
		}
		$res = QuickAlbumMovieView::getRowsAlbumById ( $albumID, $userID, $isOwner, $count, $offset );
		$totalCount = static::getMovieCountInAlbum ( $albumID );
		if (! $totalCount) {
			$totalCount = 0;
		}
		$ret = array ();
		if (! is_array ( $res )) {
			$res = array ();
		}
		foreach ( $res as $rs ) {
			$ret [] = new QuickAlbumMovie ( ( array ) $rs, $isOwner );
		}
		return $ret;
	}

	private static function getMovieCountInAlbum($albumID) {
		$a = Config::getDatasource ()->getSource ()->selectQuery ( "SELECT count(id) FROM ? WHERE albumId = ?", array (
				parent::classNameToTableName ( "AlbumMovie" ),
				$albumID 
		), true );
		if (is_array ( $a )) {
			$a = ( array ) $a [0];
			return $a ["count(id)"];
		}
		return 0;
	}

}

/**
 * @Protect This is not a table
 * @Virtual Same as above
 *
 * @author Tom
 *        
 */
class QuickMovie extends QuickAlbumMovie {

	private static function getAllMoviesCount($userId) {
		$a = Config::getDatasource ()->getSource ()->selectQuery ( "SELECT count(id) FROM ? WHERE userId = ?", array (
				parent::classNameToTableName ( "MovieList" ),
				$userId 
		), true );
		if (is_array ( $a )) {
			$a = ( array ) $a [0];
			return $a ["count(id)"];
		}
		return 0;
	}

	public static function getAllMovies($userId, $count, $page, &$total) {
		$page = ($page - 1) * $count;
		$total = static::getAllMoviesCount ( $userId );
		$db = Config::getDatasource ()->getSource ();
		$db1 = parent::classNameToTableName ( "Album" );
		$db2 = parent::classNameToTableName ( "AlbumMovie" );
		$db3 = parent::classNameToTableName ( "User" );
		$db4 = parent::classNameToTableName ( "MovieList" );
		$db5 = parent::classNameToTableName ( "Movie" );
		$res = $db->selectQuery ( "
			SELECT
				-1 as AlbumID,
				null as AlbumName,
				null as OwnerID,
				null as AlbumMovieID,
				null as AddedToAlbumDate,
				null as AlbumOrder,
	
				`?`.username as OwnerUsername,
				`?`.id as MovieListID,
				`?`.addDate as AddedToMovieListDate,
				`?`.alreadySeen as AlreadySeen,
				`?`.seenDate as SeenDate,
				`?`.movieNameEN as MovieNameEN,
				`?`.movieNameCZ as MovieNameCZ,
				`?`.id as MovieID
	
	
			FROM  `?`, `?`, `?`
			WHERE
				`?`.movieId = `?`.id
			AND
					`?`.id = ?
			AND
				`?`.userId = ?
			LIMIT ?
			OFFSET ?
			", array (
							/*
							$db1, // SELECT 1 (id)
							$db1, // SELECT 2 (name)
							$db1, // SELECT 3 (ownerIdD)
							$db2, // SELECT 4 (id)
							$db2, // SELECT 5 (addedDate)
							$db2, // SELECT 6 (itemOrder)
							*/
							$db3, // SELECT 7 (username)
				$db4, // SELECT 8 (id)
				$db4, // SELECT 9 (addDate)
				$db4, // SELECT A (alreadySeen)
				$db4, // SELECT B (seenDate)
				$db5, // SELECT C (movieNameEN)
				$db5, // SELECT D (movieNameCZ)
				$db5, // SELECT E(id)
				      // $db1, // FROM 1
				      // $db2, // FROM 2
				$db3, // FROM 3
				$db4, // FROM 4
				$db5, // FROM 5
				      // $db3, // WHERE 1
				      // $db1, // WHERE 1
				      // $db1, // WHERE 2
				      // $albumID, // WHERE 2
				      // $db2, // WHERE 3
				      // $db4, // WHERE 3
				      // $db2, // WHERE 4
				      // $db1, // WHERE 4
				      // $db4, // WHERE 5
				      // $db1, // WHERE 5
				$db4, // WHERE 6
				$db5,
				$db3,
				$userId,
				$db4,
				$userId,
				$count,
				$page 
		) );
		$ret = array ();
		if (! is_array ( $res )) {
			$res = array ();
		}
		foreach ( $res as $rs ) {
			$ret [] = new QuickAlbumMovie ( ( array ) $rs, true );
		}
		return $ret;
	}

}