<?php

/**
 * @Protect
 * @Virtual
 * @author Tom
 *
 */
class Rank extends DatabaseObject {

	const RANK_USER = 0;

	const RANK_VIP = 1;

	const RANK_DONATOR = 3;

	const RANK_MOD = 5;

	const RANK_ADMIN = 8;

	const RANK_OWNER = 10;

	const RANK_OFFSET_SIZE = 11;

	const RANK_OFFSET_START = 20;
	private $name = false;
	private $backgroundColor = false;
	private $underline = false;
	private $bold = false;
	private $icon = false;

	public function __construct($name = false, $backgroundColor = false, $underline = false, $bold = false, $icon = false) {
		$this->name = $name;
		$this->backgroundColor = $backgroundColor;
		$this->underline = $underline;
		$this->bold = $bold;
		$this->icon = $icon;
	}

	public function getName() {
		return $this->name ? $this->name : "";
	}

	public function getSafeHTML() {
		$data = array ();
		if ($this->name) {
			$data [] = '<span style="display:inline-block;border:1px solid #000000;padding: 3px;border-radius: 3px;';
			if ($this->backgroundColor) {
				$data [] = "background-color:" . $this->backgroundColor . ";";
			}
			if ($this->bold) {
				$data [] = "text-decoration:underline;";
			}
			if ($this->bold) {
				$data [] = "font-weight:bold;";
			}
			$data [] = '">' . Safe::escape ( $this->name ) . '</span>';
		}
		return implode ( "", $data );
	}

}

/**
 * @Protect
 * @Virtual
 *
 * @author Tom
 *        
 */
class RankCollection extends DatabaseObject {
	private static $ranks = array ();
	private $fullPermissionValue;
	private $rank;
	private $user;

	public function __construct($original, $rank, $user) {
		$this->fullPermissionValue = $original;
		$this->rank = $rank;
		$this->user = $user;
	}

	public function addRank($rank) {
		$val = $this->user->getPermission ();
		$val |= (pow ( 2, $rank ) << Rank::RANK_OFFSET_START);
		$this->user->setPermission ( $val );
	}

	public function removeRank($rank) {
		$val = $this->user->getPermission ();
		$val &= ~ (pow ( 2, $rank ) << Rank::RANK_OFFSET_START);
		$user->setPermission ( $val );
	}

	private static function getById($id) {
		switch ($id) {
			case Rank::RANK_USER:
				return new Rank ();
			case Rank::RANK_VIP:
				return new Rank ( "VIP" );
			case Rank::RANK_DONATOR:
				return new Rank ( "Donator" );
			case Rank::RANK_MOD:
				return new Rank ( "Moderator", "#aaffff" );
			case Rank::RANK_ADMIN:
				return new Rank ( "Administrator", "#ffff5f" );
			case Rank::RANK_OWNER:
				return new Rank ( "Owner", "#87ff00", true );
			default :
				return new Rank ( false );
		}
	}

	public function getHighestRank() {
		$val = $this->rank;
		$i = 0;
		while ( $val != 0 ) {
			$val = $val >> 1;
			$i ++;
		}
		$highest = $i - 1;
		return static::getById ( $highest );
	}

	public static function getForUser($user) {
		$rn = $user->getPermission ();
		$rank = $rn >> (Rank::RANK_OFFSET_START);
		return new RankCollection ( $rn, $rank, $user );
	}

}
