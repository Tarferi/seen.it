<?php

/**
 * @Protect
 * @author Tom
 *
 */
class Comment extends DatabaseObject {

	const IS_DELETED = 1;

	const NOT_DELETED = 2;
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("int")
	 */
	private $movieId;
	
	/**
	 * @ColumnType("int")
	 */
	private $creatorId;
	
	/**
	 * @ColumnType("text")
	 */
	private $content;
	
	/**
	 * @ColumnType("text")
	 */
	private $title;
	
	/**
	 * @ColumnType("int")
	 */
	private $isDeleted;
	
	/**
	 * @ColumnType("int")
	 */
	private $addDate;

	public function getID() {
		return $this->id;
	}

	public function getCreatorID() {
		return $this->creatorId;
	}

	public function setDeleted($deleted) {
		$this->isDeleted = $deleted ? Comment::IS_DELETED : Comment::NOT_DELETED;
		$this->save ();
	}

	public static function getForMovieID($movieId, $evenDeleted = false) {
		if ($evenDeleted) {
			return static::searchMatch ( array (
					"movieId" => $movieId 
			), false );
		} else {
			return static::searchMatch ( array (
					"movieId" => $movieId,
					"isDeleted" => Comment::NOT_DELETED 
			), false );
		}
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function setContents($contents) {
		$this->content = $contents;
	}

	public static function getQuickDetails($movieId, $evenDeleted = false) {
		return QuickCommentView::getForMovieID ( $movieId, $evenDeleted );
	}

	public static function newComment($movie, $user, $title, $content) {
		$comment = new Comment ( array (
				"movieId" => $movie->getID (),
				"creatorId" => $user->getID (),
				"content" => $content,
				"title" => $title,
				"isDeleted" => Comment::NOT_DELETED,
				"addDate" => time () 
		) );
		$comment->create ();
		return $comment;
	}

	public function deleteComment() {
		$this->isDeleted = 1;
		$this->save ();
	}

	public static function searchMatch($row, $single = false) {
		return parent::searchMatch ( get_called_class (), $row, $single );
	}

	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

}