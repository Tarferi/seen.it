<?php

/**
 * @Protect
 * @author Tom
 *
 */
class Movie extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("text")
	 */
	private $movieNameCZ;
	
	/**
	 * @ColumnType("text")
	 *
	 * @var unknown
	 */
	private $movieNameEN;
	
	/**
	 * @ColumnType("int")
	 */
	private $csfdId;
	
	/**
	 * @ColumnType("text")
	 * @AllowNull
	 */
	private $imdbId;
	
	/**
	 * @ColumnType("int")
	 * @AllowNull
	 */
	private $releaseYear;
	
	/**
	 * @ColumnType("text")
	 * @AllowNull
	 */
	private $descriptionCZ;
	
	/**
	 * @ColumnType("text")
	 * @AllowNull
	 */
	private $descriptionEN;
	
	// /**
	// * @ColumnType("text")
	// */
	// private $posterURL;
	
	/**
	 * @ColumnType("text")
	 *
	 * @var unknown
	 */
	private $dateAdded;
	
	/**
	 * @ColumnType("int")
	 */
	private $verified;

	const VERIFIED_YES = 2;

	const VERIFIED_NO = 1;

	public function detectID() {
		$movie = $this->searchMatch ( array (
				"csfdId" => $this->csfdId,
				"imdbId" => $this->imdbId,
				"releaseYear" => $this->releaseYear,
				"dateAdded" => $this->dateAdded 
		), true );
		if ($movie) {
			$this->id = $movie->id;
			return true;
		} else {
			return false;
		}
	}

	public function saveDefaultPoster() {
		$target = $this->getAvatarURL ();
		$tfile = Config::BACK2FRONT . "/" . $target;
		copy ( Config::BACK2FRONT . "/" . $this->getDefaultAvatarURL (), $tfile );
	}

	public function savePoster($avatarURL) {
		$target = $this->getAvatarURL ();
		$tfile = Config::BACK2FRONT . "/" . $target;
		if (substr ( $avatarURL, 0, 1 ) != "/") {
			file_put_contents ( $tfile, file_get_contents ( $avatarURL ) );
		}
	}

	public function updateDataFromAnotherObject($movie) {
		$this->csfdId = $movie->csfdId;
		$this->descriptionCZ = $movie->descriptionCZ;
		$this->descriptionEN = $movie->descriptionEN;
		$this->imdbId = $movie->imdbId;
		$this->movieNameCZ = $movie->movieNameCZ;
		$this->movieNameEN = $movie->movieNameEN;
		$this->releaseYear = $movie->releaseYear;
	}

	public function isVerified() {
		return $this->verified == Movie::VERIFIED_YES;
	}

	public function setVerified($ver) {
		$this->verified = $ver ? Movie::VERIFIED_YES : Movie::VERIFIED_NO;
	}

	public function getID() {
		return $this->id;
	}

	public function getDescriptionEN() {
		return $this->descriptionEN;
	}

	public function getDescriptionCZ() {
		return $this->descriptionCZ;
	}

	public function getPosterURL() {
		return static::getPublicPosterURL ( $this->id );
	}

	public function getDefaultAvatarURL() {
		return "assets/default_movie.png";
	}

	public function getAvatarURL() {
		return "assets/movie_avatars/" . (md5 ( "movie_" . $this->id . "_movie" )) . ".png";
	}

	public static function getPublicPosterURL($movieId) {
		return "/frontend/assets/movie_avatars/" . (md5 ( "movie_" . $movieId . "_movie" )) . ".png";
	}

	public function getYear() {
		return $this->releaseYear;
	}

	public function getNameEN() {
		return $this->movieNameEN;
	}

	public function getNameCZ() {
		return $this->movieNameCZ;
	}

	public function getCsfdID() {
		return $this->csfdId;
	}

	public function getImdbID() {
		return $this->imdbId;
	}

	public static function getAllRecentMovies($limit = 20) {
		return static::searchMatch ( array (), false, "=", array (
				"dateAdded" => DatabaseObject::ORDER_DESC 
		), $limit );
	}

	public static function getUserRecentMovies($user) {
		$cl = parent::classNameToTableName ( "Movie" );
		$ml = parent::classNameToTableName ( "MovieList" );
		$uid = $user->getID ();
		$res = Config::getDatasource ()->getSource ()->selectQuery ( "SELECT * FROM `?` WHERE id IN (SELECT id FROM `?` WHERE userId=`?`) ORDER BY dateAdded DESC LIMIT 100", array (
				$cl,
				$ml,
				$uid 
		) );
		return parent::searchMatchFromQueryResult ( "Movie", false, $res );
	}

	/**
	 * @TODO: Challenge
	 *
	 * @param unknown $user
	 */
	public static function getFriendsRecentMovies($user) {
	}

	public static function searchMatch($row, $single = false, $comparator = "=", $orders = array(), $limit = false) {
		return parent::searchMatch ( get_called_class (), $row, $single, $comparator, $orders, false );
	}

	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

	public static function getByCsfdID($id) {
		return static::searchMatch ( array (
				"csfdId" => $id 
		), true );
	}

	public static function getByImdbID($id) {
		return static::searchMatch ( array (
				"imdbId" => $id 
		), true );
	}

	public static function movieExistsByCsfdId($csfdId) {
		return static::searchMatch ( array (
				"csfdId" => $csfdId 
		), true ) !== false;
	}

	public static function movieExistsByImdbId($imdbId) {
		return static::searchMatch ( array (
				"imdbId" => $imdbId 
		), true ) !== false;
	}

	/**
	 *
	 * @param User $creatorUser
	 * @param int $csfdId
	 * @return Movie
	 */
	public static function importCsfdMovie($csfdId, $reImport = false) {
		$importer = new CSFDImporter ( $reImport );
		static::commitImport ( $importer, $csfdId );
		$mv = static::getByCsfdID ( $csfdId );
		return $mv;
	}

	/**
	 *
	 * @param User $creatorUser
	 * @param int $csfdId
	 * @return Movie
	 */
	public static function importImdbMovie($imdbId, $reImport = false) {
		$importer = new IMDBImporter ( $reImport );
		static::commitImport ( $importer, $imdbId );
		return static::getByImdbID ( $imdbId );
	}

	public function reImportMovie() {
		if ($this->csfdId != 0) {
			static::importCsfdMovie ( $this->csfdId, true );
		} else if ($this->imdbId != 0) {
			static::importImdbMovie ( $this->imdbId, true );
		}
	}

	private static function commitImport($importer, $id) {
		$importer->getFullMovieById ( $id );
	}

}