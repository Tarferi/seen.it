<?php

/**
 * Represents movie details for each user and each movie
 * @author Tom
 * @Protect
 *
 */
class MovieList extends DatabaseObject {

	const WOULD_RECOMMEND = 1;

	const WOULD_NOT_RECOMMEND = 2;

	const HAS_SEEN = 2;

	const HAS_NOT_SEEN = 1;
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 */
	private $id;
	
	/**
	 * @ColumnType("int")
	 */
	private $userId;
	
	/**
	 * @ColumnType("int")
	 */
	private $movieId;
	
	/**
	 * @ColumnType("int")
	 */
	private $addDate;
	
	/**
	 * @ColumnType("int")
	 */
	private $alreadySeen;
	
	/**
	 * @ColumnType("int")
	 * @AllowNull
	 */
	private $seenDate;
	
	/**
	 * 0 = Not voted
	 * 1 = Would recommend
	 * 2 = Would not recommend
	 * @ColumnType("int")
	 * @AllowNull
	 *
	 * @var int
	 */
	private $recommend;

	public function getUserID() {
		return $this->userId;
	}

	public function getSeenDate() {
		return $this->seenDate;
	}

	/**
	 *
	 * @param unknown $user
	 * @param unknown $movie
	 * @return MovieList
	 */
	public static function getMovieForUser($user, $movie) {
		return static::searchMatch ( array (
				"userId" => $user->getID (),
				"movieId" => $movie->getID () 
		), true );
	}

	public static function getItems($user, $idlist, &$createdCount = 0) {
		$arl = array ();
		$userID = $user->getID ();
		foreach ( $idlist as $id ) {
			$d = static::searchMatch ( array (
					"userId" => $userID,
					"movieId" => $id 
			), true );
			if ($d) {
				$arl [] = $d;
				$createdCount ++;
			}
		}
		return $arl;
	}

	public static function newItems($user, $idList, &$createdCount = 0) {
		$arl = array ();
		foreach ( $idList as $id ) {
			$arl [] = static::newItem ( $user, $id, 0, false, true, $createdCount );
		}
		return $arl;
	}

	public static function newItem($user, $movie, $wouldRecommend = 0, $seen = false, $movieIsId = false, &$createdCount = 0) {
		$movieId = $movieIsId ? $movie : $movie->getID ();
		$seenId = $seen ? MovieList::HAS_SEEN : MovieList::HAS_NOT_SEEN;
		$item = static::searchMatch ( array (
				"userId" => $user->getID (),
				"movieId" => $movieId 
		), true );
		if (! $item) {
			$item = new MovieList ( array () );
			$item->addDate = time ();
			$item->userId = $user->getID ();
			$item->alreadySeen = $seenId;
			$item->movieId = $movieId;
			$item->recommend = $wouldRecommend;
			$item->seenDate = $seen ? time () : null;
			$item->create ();
			$createdCount ++;
			$item = static::searchMatch ( array (
					"userId" => $user->getID (),
					"movieId" => $movieId 
			), true );
			
			return $item;
		} else {
			/*
			 * $update = false; if ($item->hasSeen () != $seen) { $item->setSeen ( $seen ); $update = true; } if ($item->getRecommendation () != $wouldRecommend) { $item->setRecommend ( $recommend ); $update = true; } if ($update) { $item->save (); }
			 */
			return $item;
		}
	}

	public function setRecommend($recommend) {
		$this->recommend = $recommend;
		$this->save ();
	}

	public function hasSeen() {
		return $this->alreadySeen == MovieList::HAS_SEEN;
	}

	public static function getAllRecommendCount($movie) {
		return parent::searchCountMatch ( get_called_class (), array (
				"movieId" => $movie->getID () 
		) );
	}

	public static function getRecommendCount($movie) {
		return parent::searchCountMatch ( get_called_class (), array (
				"recommend" => MovieList::WOULD_RECOMMEND,
				"movieId" => $movie->getID () 
		) );
	}

	/**
	 *
	 * @deprecated Can amd should be calculated
	 * @param unknown $movie
	 * @return unknown number
	 */
	public static function getNotRecommendCount($movie) {
		return parent::searchCountMatch ( get_called_class (), array (
				"recommend" => MovieList::WOULD_NOT_RECOMMEND,
				"movieId" => $movie->getID () 
		) );
	}

	public static function getUsercountSeenMovie($movie) {
		return parent::searchCountMatch ( get_called_class (), array (
				"alreadySeen" => MovieList::HAS_SEEN,
				"movieId" => $movie->getID () 
		) );
	}

	public static function getUserUnseenCount($user) {
		return parent::searchCountMatch ( get_called_class (), array (
				"userId" => $user->getID (),
				"alreadySeen" => MovieList::HAS_NOT_SEEN 
		) );
	}

	public static function getUserTotalMovieCount($user) {
		return parent::searchCountMatch ( get_called_class (), array (
				"userId" => $user->getID () 
		) );
	}

	public function getID() {
		return $this->id;
	}

	public function setSeen($seen) {
		$this->alreadySeen = $seen;
		$this->seenDate = $seen == MovieList::HAS_SEEN ? time () : null;
		$this->save ();
	}

	public function getRecommendation() {
		return $this->recommend;
	}

	public static function searchMatch($row, $single = false, $comparator = "=", $orders = array()) {
		return parent::searchMatch ( get_called_class (), $row, $single );
	}

	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

}