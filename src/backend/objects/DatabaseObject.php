<?php

class DatabaseTable {
	private $name;
	private $columns = array ();

	public function __construct($name) {
		$this->name = $name;
	}

	public function getTableName() {
		return $this->name;
	}

	public function addColumn($col) {
		$this->columns [] = $col;
	}

	public function getColumns() {
		return $this->columns;
	}

}

class DatabaseColumn {
	private $table;
	private $type;
	private $primary;
	private $allowNull;
	private $name;

	public function getColumnName() {
		return $this->name;
	}

	public function getTable() {
		return $this->table;
	}

	public function getType() {
		return $this->type;
	}

	public function isPrimary() {
		return $this->primary;
	}

	public function isNullAllowed() {
		return $this->allowNull;
	}

	public function __construct($table, $name, $type, $primary = false, $allowNull = false) {
		$this->table = $table;
		$this->type = $type;
		$this->name = $name;
		$this->primary = $primary;
		$this->allowNull = $allowNull;
	}

}

abstract class DatabaseObject {

	const ORDER_ASC = "ASC";

	const ORDER_DESC = "DESC";
	private $__tableName;
	private $__className;

	protected static function classNameToTableName($className) {
		return Config::DATAASE_TABLE_PREFIX . strtolower ( $className );
	}

	protected static function TableNameToClassName($tableName) {
		return substr ( $tableName, strlen ( Config::DATAASE_TABLE_PREFIX ) );
	}

	protected static function searchCountMatch($className, $row, $comparator = "=") {
		$rc = new ReflectionClass ( $className );
		$replacements = array ();
		$query = array ();
		$query [] = "SELECT";
		$query [] = "count(*) as COUNT";
		$query [] = "FROM";
		$query [] = "`?`";
		$replacements [] = static::classNameToTableName ( $className );
		if (count ( $row ) > 0) {
			$query [] = "WHERE";
			$selects = array ();
			foreach ( $row as $key => $val ) {
				$selects [] = "`?`" . $comparator . "'?'";
				$replacements [] = $key;
				$replacements [] = $val;
			}
			$query [] = implode ( " AND ", $selects );
		}
		$query = implode ( " ", $query );
		$db = Config::getDatasource ()->getSource ();
		$res = $db->selectQuery ( $query, $replacements );
		if (is_array ( $res )) {
			$res = ( array ) $res [0];
			return $res ["COUNT"];
		}
		throw new DatabaseException ();
	}

	protected static function searchMatch($className, $row, $single = false, $comparator = "=", $orders = array(), $limit = false) {
		$rc = new ReflectionClass ( $className );
		$replacements = array ();
		$query = array ();
		$query [] = "SELECT";
		$query [] = "*";
		$query [] = "FROM";
		$query [] = "`?`";
		$replacements [] = static::classNameToTableName ( $className );
		if (count ( $row ) > 0) {
			$query [] = "WHERE";
			$selects = array ();
			foreach ( $row as $key => $val ) {
				$selects [] = "`?`" . $comparator . "'?'";
				$replacements [] = $key;
				$replacements [] = $val;
			}
			$query [] = implode ( " AND ", $selects );
		}
		$orderLength = count ( $orders );
		if ($orderLength > 0) {
			$tmpquery = array ();
			$query [] = "ORDER BY";
			foreach ( $orders as $field => $order ) {
				$tmpquery [] = "`?` " . $order;
				$replacements [] = $field;
			}
			$query [] = implode ( ", ", $tmpquery );
		}
		if ($single) {
			$query [] = "LIMIT 1";
		} else if ($limit !== false) {
			$query [] = "LIMIT " . $limit;
		}
		$query = implode ( " ", $query );
		$db = Config::getDatasource ()->getSource ();
		$res = $db->selectQuery ( $query, $replacements );
		return static::searchMatchFromQueryResult ( $className, $single, $res );
	}

	protected static function searchMatchFromQueryResult($className, $single, $res) {
		if (is_array ( $res )) {
			$ret = array ();
			foreach ( $res as $r ) {
				$ar = ( array ) $r;
				$rc = new ReflectionClass ( $className );
				$b = $rc->newInstanceArgs ( array (
						$ar 
				) );
				$ret [] = $b;
			}
			return $single ? count ( $ret ) > 0 ? $ret [0] : false : $ret;
		}
		return false;
	}

	private static function getDerivedTables() {
		$objects = array ();
		$cls = get_declared_classes ();
		foreach ( $cls as $class ) {
			if (is_subclass_of ( $class, "DatabaseObject" )) {
				$rc = new ReflectionAnnotatedClass ( $class );
				if ((! $rc->hasAnnotation ( "Protect" )) || Config::DEBUG_OVERRIDE_PROTECT) {
					if (! $rc->hasAnnotation ( "Virtual" )) {
						$objects [] = $class;
					}
				}
			}
		}
		$tables = array ();
		foreach ( $objects as $obj ) {
			$table = new DatabaseTable ( static::classNameToTableName ( $obj ) );
			$rc = new ReflectionAnnotatedClass ( $obj );
			$props = $rc->getProperties ();
			foreach ( $props as $prop ) {
				$name = $prop->getName ();
				$primary = $prop->getAnnotation ( "Primary" ) !== false;
				$allownull = $prop->getAnnotation ( "AllowNull" ) !== false;
				if ($prop->hasAnnotation ( "ColumnType" )) {
					$type = $prop->getAnnotation ( "ColumnType" )->value;
					$col = new DatabaseColumn ( $table, $name, $type, $primary, $allownull );
					$table->addColumn ( $col );
				}
			}
			$tables [] = $table;
		}
		return $tables;
	}

	protected static function getObjectName() {
		return "TABLE";
	}

	public static function removeDatabase() {
		DatabaseObjectView::removeViews ();
		$tables = DatabaseObject::getDerivedTables ();
		$queries = array ();
		foreach ( $tables as $table ) {
			$queries [] = array (
					"DROP " . static::getObjectName () . " IF EXISTS `?`",
					array (
							$table->getTableName () 
					) 
			);
		}
		$db = Config::getDatasource ()->getSource ();
		foreach ( $queries as $queryd ) {
			$query = $queryd [0];
			$replacements = $queryd [1];
			$db->executeQuery ( $query, $replacements );
		}
	}

	public static function recreateDatabase() {
		DatabaseObject::removeDatabase ();
		DatabaseObject::createDatabase ();
	}

	protected static function createDatabaseFromTable($table, &$queries, &$replacements) {
		$replacements = array (
				$table->getTableName () 
		);
		$columns = array ();
		$cols = $table->getColumns ();
		$primary = false;
		foreach ( $cols as $col ) {
			$columns [] = "`?` " . $col->getType () . ($col->isNullAllowed () ? "" : " NOT NULL") . ($col->isPrimary () ? " AUTO_INCREMENT" : "");
			$replacements [] = $col->getColumnName ();
			if ($col->isPrimary ()) {
				$primary = $col;
			}
		}
		if ($primary === false) {
			throw new DatabaseException ();
		}
		$columns [] = "PRIMARY KEY (`?`)";
		$replacements [] = $primary->getColumnName ();
		$columns = implode ( ",\n", $columns );
		$query = array (
				"CREATE " . static::getObjectName () . " IF NOT EXISTS `?` (\n" . $columns . "\n) ",
				$replacements 
		);
		$queries [] = $query;
	}

	public static function createDatabase() {
		$tables = DatabaseObject::getDerivedTables ();
		$queries = array ();
		foreach ( $tables as $table ) {
			static::createDatabaseFromTable ( $table, $queries, $replacement );
		}
		$db = Config::getDatasource ()->getSource ();
		$db->executeQuery ( "ALTER DATABASE ? CHARACTER SET utf8 COLLATE utf8_general_ci;", array (
				Config::DATABASE_NAME 
		) );
		foreach ( $queries as $queryd ) {
			$query = $queryd [0];
			$replacements = $queryd [1];
			$db->executeQuery ( $query, $replacements );
		}
		DatabaseObjectView::createViews ();
	}

	public function save() {
		if ($this->uniqueAvailable ()) {
			return Config::getDatasource ()->saveObject ( $this );
		}
		return false;
	}

	private function uniqueAvailable() {
		$reflectionClass = new ReflectionAnnotatedClass ( $this->__className );
		$props = $reflectionClass->getProperties ();
		$vls = array ();
		foreach ( $props as $prop ) {
			$unique = $prop->getAnnotation ( "Unique" ) !== false;
			if ($unique) {
				$orin = $prop->isPublic ();
				$prop->setAccessible ( true );
				$vls [$prop->getName ()] = $prop->getValue ( $this );
				$prop->setAccessible ( $orin );
			}
		}
		if (count ( $vls ) > 0) {
			$objs = DatabaseObject::searchMatch ( $this->__className, $vls );
			if ($objs) {
				foreach ( $objs as $obj ) {
					if ($obj->getID () != $this->getID ()) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public function create() {
		if ($this->uniqueAvailable ()) {
			return Config::getDatasource ()->createObject ( $this );
		}
		return false;
	}

	public function delete() {
		return Config::getDatasource ()->deleteObject ( $this );
	}

	public function __construct($rows) {
		$this->__tableName = static::classNameToTableName ( get_class ( $this ) );
		$this->__className = get_class ( $this );
		$reflectionClass = new ReflectionClass ( $this->__className );
		foreach ( $rows as $key => $value ) {
			$vl = $reflectionClass->getProperty ( $key );
			$orin = $vl->isPublic ();
			$vl->setAccessible ( true );
			$vl->setValue ( $this, $value );
			$vl->setAccessible ( $orin );
		}
	}

	public function getCreateSQL() {
		$fields = array ();
		$values = array ();
		$replacements = array (
				$this->__tableName 
		);
		$rc = new ReflectionClass ( $this->__className );
		$props = $rc->getProperties ();
		$vls = array ();
		foreach ( $props as $prop ) {
			$orin = $prop->isPublic ();
			$prop->setAccessible ( true );
			$name = $prop->getName ();
			$val = $prop->getValue ( $this );
			$prop->setAccessible ( $orin );
			$vls [$name] = $val;
		}
		$id = false;
		foreach ( $vls as $val => $value ) {
			if (substr ( $val, 0, 2 ) != "__" && $val != "id") {
				$fields [] = $val;
				if ($value != null || $value == 0) {
					$values [] = "'?'";
					$replacements [] = $value;
				} else {
					$values [] = "null";
				}
			} else if ($val == "id") {
				$id = $val;
			}
		}
		$fields = implode ( ", ", $fields );
		$values = implode ( ", ", $values );
		if ($id === false) {
			throw new DatabaseException ();
		}
		$sql = "INSERT INTO `?` (" . $fields . ") VALUES (" . $values . ")";
		return array (
				$sql,
				$replacements 
		);
	}

	public function getSaveSQL() {
		$updates = array ();
		$replacements = array (
				$this->__tableName 
		);
		$rc = new ReflectionClass ( $this->__className );
		$props = $rc->getProperties ();
		foreach ( $props as $prop ) {
			$orin = $prop->isPublic ();
			$prop->setAccessible ( true );
			$name = $prop->getName ();
			$val = $prop->getValue ( $this );
			$prop->setAccessible ( $orin );
			$vls [$name] = $val;
		}
		$id = false;
		foreach ( $vls as $val => $value ) {
			if (substr ( $val, 0, 2 ) != "__" && $val != "id") {
				if ($value != null) {
					$updates [] = $val . "='?'";
					$replacements [] = $value;
				} else {
					$updates [] = $val . "=null";
				}
			} else if ($val == "id") {
				$id = $value;
			}
		}
		if ($id === false) {
			throw new DatabaseException ();
		}
		$fields = implode ( ", ", $updates );
		$sql = "UPDATE `?` SET " . $fields . " WHERE id='?'";
		$replacements [] = $id;
		return array (
				$sql,
				$replacements 
		);
	}

	public function getDeleteSQL() {
		$updates = array ();
		$replacements = array (
				$this->__tableName 
		);
		$rc = new ReflectionClass ( $this->__className );
		$props = $rc->getProperties ();
		$id = false;
		foreach ( $props as $prop ) {
			$orin = $prop->isPublic ();
			$prop->setAccessible ( true );
			$name = $prop->getName ();
			if ($name == "id") {
				$val = $prop->getValue ( $this );
				$id = $val;
				$prop->setAccessible ( $orin );
				break;
			}
			$prop->setAccessible ( $orin );
		}
		if ($id === false) {
			throw new DatabaseException ();
		}
		$fields = implode ( ", ", $updates );
		$sql = "DELETE FROM `?` WHERE id='?'";
		$replacements [] = $id;
		return array (
				$sql,
				$replacements 
		);
	}

}