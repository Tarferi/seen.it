<?php

/**
 * @Target("property")
 */
class ColumnType extends Annotation {
	public $value;

}

/**
 * @Target("property")
 */
class Primary extends Annotation {

}

/**
 * @Target("property")
 */
class AllowNull extends Annotation {

}

/**
 * @Target("property")
 */
class Unique extends Annotation {

}

/**
 * @Target("method")
 */
class Inject extends Annotation {

}

/**
 * @Target("class")
 */
class Protect extends Annotation {

}

/**
 * @Target("class")
 *
 */
class Virtual extends Annotation {
	
}

/**
 * @Target("property")
 */
class Table extends Annotation {

}

/**
 * @Target("property")
 */
class Joins extends Annotation {

}

/**
 * @Target("property")
 */
class From extends Annotation {

}

/**
 * @Target("property")
 */
class Restriction extends Annotation {

}

/**
 * @Target("property")
 */
class Order extends Annotation {

}

/**
 * @Target("property")
 */
class GroupBy extends Annotation {

}

/**
 * @Target("property")
 */
class Having extends Annotation {

}