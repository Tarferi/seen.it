<?php

class Lock {
	private static $instances = array ();

	public static function get($className) {
		if (isset ( $instances [$className] )) {
			return $instances [$className];
		} else {
			$lock = new Lock ( "lock." . $className );
		}
	}
	private $fileHandler;

	private function __construct($file) {
		$this->fileHandler = fopen ( $file, "r" );
	}

	private function lock() {
		if (flock ( $this->fileHandler, LOCK_EX )) {
			return true;
		} else {
			throw new SystemException ();
		}
	}

	private function unlock() {
		if (flock ( $this->fileHandler, LOCK_UN )) {
			return true;
		} else {
			throw new SystemException ();
		}
	}

}