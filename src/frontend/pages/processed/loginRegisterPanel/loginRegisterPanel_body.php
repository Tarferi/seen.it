<?php
if (isset ( $_POST ["loginRegisterPanel_login"] )) {
	$username = isset ( $_POST ["loginRegisterPanel_username"] ) ? $_POST ["loginRegisterPanel_username"] : false;
	$password = isset ( $_POST ["loginRegisterPanel_password"] ) ? $_POST ["loginRegisterPanel_password"] : false;
	if ($username && $password) {
		$user = User::getByCredentials ( $username, $password );
		if ($user) {
			$session=Config::getConfig()->getSession();
			$session->setLogged(true);
			$session->setUser($user);
			Config::redirect(null);
		} else {
			Config::redirect("invalidLogin");
		}
	}
}
?>
<div class="container-fluid">
		<form class="navbar-form navbar-right action=" method="POST">
			<div class="form-group">
				<input type="text" placeholder="Username" name="loginRegisterPanel_username" class="form-control" />
</div>
			<div class="form-group">
				<input type="password" name="loginRegisterPanel_password" placeholder="Password" class="form-control" />
</div>
			<div class="form-group">
				<div class="btn-toolbar" role="toolbar">
					<div class="btn-group">
						<input type="submit" class="btn btn-success" name="loginRegisterPanel_login" value="Login" />
</div>
					<div class="btn-group">
				<?php
				echo "<a href=\"" . Config::PUBLIC_WEB_ROOT . "/register\" class=\"btn btn-info\" role=\"button\">Register</a>";
				?>

</div>
				</div>
			</div>
		</form>
	</div>