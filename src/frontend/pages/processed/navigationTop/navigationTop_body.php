<nav class="navbar navbar-default"><div class="container-fluid">
			<ul class="nav navbar-nav">
<li><a href="/web"><img src="/frontend/assets/logo.png" width="328" height="130" /></a></li>
				<li>
					<div style="display: block; height: 328; background: red">
				<?php preProcessor::get()->includeBody("searchPanel")?>

</div>
				</li>
			</ul>
<ul class="nav navbar-nav navbar-right">
<li>
					<?php
					if (viewState::isRegistering ()) {
						echo "<h3>Registering just now!</h3>";
					} else {
						$session = Config::getConfig ()->getSession ();
						if ($session->isLogged ()) {
							preProcessor::get ()->includeBody ( "loggedInPanel" );
						} else {
							preProcessor::get ()->includeBody ( "loginRegisterPanel" );
						}
					}
					?>
</li>
			</ul>
</div>
		
	</nav>