<?php
$user = Config::getConfig ()->getSession ()->getUser ();
$recents = QuickRecentMovieView::getForUser ( $user );

$currentData = viewState::getAllMoviesForUser ( $user );

$putMovie = function ($movie, $currentData) {
	$user = Config::getConfig ()->getSession ()->getUser ();
	$seen = $movie->hasSeen ();
	$inList = $movie->isInList ();
	$addedBy = "System";
	$poster=Safe::escape ( $movie->getPosterURL () );
	echo '
    		<div class="container-fluid well recentlyAddedMovies_smallPanel">
			<div class="recentlyAddedMovies_imagePanel">
				<img src="' . $poster . '" width="100" height="120" / />
			</div>
			<div class="recentlyAddedMovies_titlePanel">
				<div class="recentlyAddedMovies_titlePanelIn">
					<label class="recentlyAddedMovies_movieTitle"><u><a class="pointer" href="/web/movie/' . $movie->getMovieID () . '" target="_blank"> ' . Safe::escape ( $movie->getMovieNameEN () ) . ' (' . Safe::escape ( $movie->getReleaseYear () ) . ')</a></u></label><br / />
					<div class="qtable">
						<div class="qtable-row">
							<div class="qtable-cell-header recentlyAddedMovies_th nowrap">Added by:</div>
							<div class="qtable-cell recentlyAddedMovies_td fullwidth"><i>' . Safe::escape($addedBy) . '</i></div>
						</div>
						<div class="qtable-row">
							<div class="qtable-cell-header recentlyAddedMovies_th nowrap">Already seen:</div>
							<div class="qtable-cell recentlyAddedMovies_td">
								<span class="glyphicon glyphicon-' . ($seen ? "ok" : "remove") . ' text-' . ($seen ? "success" : "danger") . '"></span>
							</div>
						</div>
							
						<div class="recentlyAddedMovies_InfoPanel qtable-row' . ($inList ? " hidden" : "") . '"" movieId="' . $movie->getMovieID () . '">
							<div class="qtable-cell-header recentlyAddedMovies_th nowrap" valign="top">Add to:</div>
							<div class="qtable-cell recentlyAddedMovies_td text-info">
								<select>
									<option value="-1">All movies</option>
									<optgroup label="My albums">';
	foreach ( $currentData as $album ) {
		echo '<option value="' . $album->getAlbumID () . '">' . Safe::escape($album->getAlbumName ()) . '</option>';
	}
	echo '
									</optgroup>
								</select>
								<span class="glyphicon glyphicon-plus text-info pointer" onClick=recentlyAddedMovies_instance.importMovie(this,' . $movie->getMovieID () . ');></span>
								<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate text-info hidden"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>';
};

?>
<div class="container-fluid seenit_panel recentlyAddedMovies_mainPanel well">
		<div class="seenit_panel_title">Recently added movies</div>
		<div class="recentlyAddedMovies_mainPanelWrapper" id="recentlyAddedMovies_mainPanelWrapper">
			<br /><?php
			foreach ( $recents as $recent ) {
				$putMovie ( $recent, $currentData );
			}
			?>

</div>
	</div>