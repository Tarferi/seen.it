<?php
if (isset ( $_POST ["loggedInPanel_action"] )) {
	$action = $_POST ["loggedInPanel_action"];
	switch ($action) {
		case "logout":
			$session = Config::getConfig ()->getSession ();
			$session->setLogged ( false );
			Config::redirect ( null );
			die ();
		break;
		case "profile":
			$userId=Config::getConfig()->getSession()->getUser()->getID();
			Config::redirect("user/".$userId);
			die();
		break;
	}
}
$user = Config::getConfig ()->getSession ()->getUser ();
$current = viewState::getAllMoviesForUser ( $user );
$albumCount = count ( $current );
?>
<div class="container-fluid flex-container-row loggedInTable_panel">
		<div class="flex-container ">
			<div class="qtable loggedInTable_table nowrap">
				<div class="qtable-row">
					<div class="qtable-cell-header text-right">
						<label>Logged in:</label>
					</div>
					<div class="qtable-cell">
						<label class="underline"><?php echo $user->getUsername(); ?></label>
					</div>
				</div>
				<div class="qtable-row">
					<div class="qtable-cell-header text-right">
						<label>Albums:</label>
					</div>
					<div class="qtable-cell">
						<label id="loggedInPanel_albumCount"><?php echo $albumCount ?></label>
					</div>
				</div>
				<div class="qtable-row">
					<div class="qtable-cell-header text-right">
						<label>Movies unseen:</label>
					</div>
					<div class="qtable-cell">
						<label id="loggedInPanel_unseenCount"><?php echo $user->getMoviesUnseen(); ?></label>
					</div>
				</div>
			</div>
		</div>
		<div class="loggedInTable_right">
			<div class="flex-container-column">
				<div class="btn-sm btn-danger margin-bottom-5 pointer" id="loggedInPanel_logout">
					<span class="glyphicon glyphicon-off"></span>
					 Logout
				</div>
				<div class="btn-sm btn-info margin-bottom-5 pointer" id="loggedInPanel_profile">
					<span class="glyphicon glyphicon-wrench"></span>
					 Profile
				</div>
				<div class="btn-sm btn-success margin-bottom-5 pointer" id="loggedInPanel_message">
					<span class="glyphicon glyphicon-envelope"></span>
					 Messages
				</div>
			</div>
		</div>
	</div>