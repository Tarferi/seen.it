<?php
if (! isset ( $headerFunctions )) {
	$headerFunctions = function () {
	};
}
if (! isset ( $bodyHeaderFunction )) {
	$bodyHeaderFunction = function () {
	};
}
if (! isset ( $bodyFunction )) {
	$bodyFunction = function () {
	};
}
if (! isset ( $bodyFooterFunction )) {
	$bodyFooterFunction = function () {
	};
}
if (! isset ( $pageTitle )) {
	$pageTitle = "Default page title";
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo $pageTitle; ?></title>
<?php $headerFunctions(); preProcessor::get()->appendHeadItems(); ?>
</head>
<body>
	 <?php
		$bodyHeaderFunction ();
		$bodyFunction ();
		$bodyFooterFunction ();
		?>
</body>
</html>

