<html>
<head>
<title>Seen.it Sandbox</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/ajax.js"></script>
<script src="/frontend/js/safe.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>
<script src="/frontend/js/bootstrap-dialog.min.js"></script>
<link href="/frontend/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/frontend/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="/frontend/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
</head>
<style>
.qbtn {
	border: 1px solid #000000;
	border-radius: 2px;
	padding: 3px;
}

.login-dialog .modal-dialog {
	width: 300px;
}

.pointer {
	cursor: pointer;
}
</style>
<script>
var btn0_click=function() {
	//Redirect to seen.it safe page
}
var btn1_click=function() {
	var q=document.createElement("div");
	var inputUsername=document.createElement("input");
	inputUsername.classList.add("form-control");
	inputUsername.type="text";
	inputUsername.placeholder="Username";

	var ulabel=document.createElement("label");
	ulabel.innerHTML="Username:";
	var plabel=document.createElement("label");
	plabel.innerHTML="Password:";
	
	inputPassword=document.createElement("input");
	inputPassword.classList.add("form-control");
	inputPassword.type="password";
	inputPassword.placeholder="Password";

	q.appendChild(ulabel);
	q.appendChild(inputUsername);
	q.appendChild(document.createElement("br"));
	q.appendChild(plabel);
	q.appendChild(inputPassword);

	var tryLogin=function(dialog) {
		dialog.setClosable(false);
		dialog.enableButtons(false);
		inputUsername.setAttribute("readonly","true");
		inputPassword.setAttribute("readonly","true");
		var ajax=new Ajax();
		ajax.asyncQuery("tryLogin",{"username":inputUsername.value,"password":inputPassword.value},function(result) {
			var res;
			try {
				res=JSON.parse(result);
			} catch (err) {
				res={};
			}
			if(res) {
				if(res["Status"]=="Ok") {
					dialog.close();
					location.reload();
					return;
				}
			} 
			dialog.setClosable(true);
			dialog.enableButtons(true);
			inputUsername.removeAttribute("readonly");
			inputPassword.removeAttribute("readonly");
		});
	};

	BootstrapDialog.show({
		closeable: false,
		type : BootstrapDialog.TYPE_PRIMARY,
		title : 'Seen.it login',
		message : q,
	 	cssClass: 'login-dialog',
		buttons: [
				{
					label : 'Login',
					cssClass : 'btn-primary',
					action : function(dialog) {
						tryLogin(dialog);
					}
				},
		          {
			label : 'Cancel',
			cssClass : 'btn-default',
			action : function(dialog) {
				dialog.close();
			}
		},
      ]
	});
	
}
var btn5_click=function() {
	var q=document.createElement("div");
	var sel=document.createElement("select");
	var mainOpt=document.createElement("option");
	mainOpt.innerHTML="All movies";
	mainOpt.value="-1";
	sel.appendChild(mainOpt);
	var mainCat=document.createElement("optgroup");
	sel.appendChild(mainCat);
	mainCat.setAttribute("label","My albums");
	sel.classList.add("form-control");
	var ajax=new Ajax();


	var processing=document.createElement("span");
	processing.classList.add("glyphicon");
	processing.classList.add("glyphicon-refresh");
	processing.classList.add("glyphicon-refresh-animate");
	processing.classList.add("text-info");

	var lbl=document.createElement("label");
	lbl.innerHTML="Loading album list&nbsp;";
	q.appendChild(lbl);
	q.appendChild(processing);
	var term=false;

	var addBtn=document.createElement("div");
	addBtn.classList.add("btn");
	addBtn.classList.add("btn-success");
	addBtn.classList.add("pointer");
	
	var addGlyph=document.createElement("span");
	addGlyph.classList.add("glyphicon");
	addGlyph.classList.add("glyphicon-plus");
	addBtn.appendChild(addGlyph);

	var addText=document.createElement("span");
	addText.innerHTML="&nbsp;Import";
	addBtn.appendChild(addText);
	addBtn.style.marginTop="10px";

	var canAdd=true;
	var movieId=0;
	addBtn.addEventListener("click",function() {
		if(canAdd) {
			canAdd=false;
			addBtn.classList.add("disabled");
			var qajax=new Ajax();
			var albumId=sel.options[sel.selectedIndex].value;
			var action="copyMovies";
			if(albumId==-1) {
				action="importMovies";
			}	
			qajax.asyncQuery(action,{"data":movieId,"albumId":albumId},function(result) {
				if(term) {
					return;
				}
				var res;
				try {
					res=JSON.parse(result);
				} catch (err) {
					res={};
				}
				if(res["Status"]=="Ok") {
					canAdd=true;
					addBtn.classList.remove("disabled");
					return;
				}
				BootstrapDialog.show({
					type : BootstrapDialog.TYPE_DANGER,
					title : 'Import failed',
					message : "Movie import failed. Please try it again later.",
				});
				canAdd=true;
				addBtn.classList.remove("disabled");
			});
		}
	});

	
	var dialog=BootstrapDialog.show({
		type : BootstrapDialog.TYPE_PRIMARY,
		title : 'Seen.it import movie',
		message : q,
		buttons: [
		          {
			label : 'Close',
			cssClass : 'btn-primary',
			action : function(dialog) {
				term=true;
				dialog.close();
			}
		},
      ]
	});
	
	ajax.asyncQuery("getAlbumList",[],function(result) {
		if(term) {
			return;
		}
		var res;
		try {
			res=JSON.parse(result);
		} catch (err) {
			res={};
		}
		if(res) {
			if(res["Status"]=="Ok") {
				var data=res["Data"];
				for(var i=0;i<data.length;i++) {
					var aname=data[i];
					var elem=document.createElement("option");
					elem.innerHTML=Safe.escape(aname);
					mainCat.appendChild(elem);
				}
				lbl.innerHTML="Select album:";
				processing.remove();
				q.appendChild(sel);
				q.appendChild(addBtn);
			}
		} 
	});
	
}

$(document).ready(function(){
	$('[data-toggle="popover"]').popover({
		trigger : 'hover',
		'placement' : 'bottom'
	});
});
</script>
<body>
	<center>
		<br /> <br />
		<table style="border: 1px solid #000000; border-collapse: separate; border-spacing: 40px;">
			<tr>
				<th>#</th>
				<th>Component name</th>
				<th>Preview</th>
			</tr>
			<tr>
				<td>1</td>
				<td align="center">Unavailable button</td>
				<td align="center">
					<div onClick="btn0_click();" class="qbtn pointer btn btn-warning" title="Service unavailable" data-toggle="popover" data-trigger="hover" data-content="The Seen.it service is not available at the moment. This usually happens during version updates, when bugs are being fixed or you have outdated plugin. Click the button to be redirected to page with further informations">
						<span class="glyphicon glyphicon-exclamation-sign"></span>
						&nbsp;Service unavailable
					</div>
				</td>
			</tr>
			<tr>
				<td>2</td>
				<td align="center">Not logged in button</td>
				<td align="center">
					<div onClick="btn1_click();" class="qbtn pointer btn btn-danger" title="Click to login">
						<span class="glyphicon glyphicon glyphicon-alert"></span>
						&nbsp;Not logged in
					</div>
				</td>
			</tr>
			<tr>
				<td>3</td>
				<td align="center">Seen button</td>
				<td align="center">
					<div class="qbtn btn btn-success" title="Already seen movie" data-toggle="popover" data-trigger="hover" data-content="You have already seen this movie.">
						<span class="glyphicon glyphicon glyphicon-ok"></span>
						&nbsp;Seen movie
					</div>
				</td>
			</tr>
			<tr>
				<td>4</td>
				<td align="center">Unseen button</td>
				<td align="center">
					<div class="qbtn btn btn-info" title="Unseen movie" data-toggle="popover" data-trigger="hover" data-content="You have not yet seen this movie, but you already have it in your list.">
						<span class="glyphicon glyphicon-eye-close"></span>
						&nbsp;Unseen movie
					</div>
				</td>
			</tr>
			<tr>
				<td>5</td>
				<td align="center">Unlisted button</td>
				<td align="center">
					<div class="qbtn btn btn-default" onClick="btn5_click();" title="Unseen movie" data-toggle="popover" data-trigger="hover" data-content="You have not yet seen this movie. Click the button to add it to your list">
						<span class="glyphicon glyphicon-eye-close"></span>
						&nbsp;Unseen movie
					</div>
				</td>
			</tr>
			<tr>
				<td>6</td>
				<td align="center">Loading button</td>
				<td align="center">
					<div class="qbtn btn btn-default disabled" title="Loading data" title="Loading Seen.it data" data-toggle="popover" data-trigger="hover" data-content="If loading data takes too long, try refreshing the page.">
						<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
						&nbsp;Loading...
					</div>
				</td>
			</tr>
		</table>
	</center>
</body>
</html>