<?php

viewState::setRegistering(true);

$headerFunctions = function () {
	preProcessor::get ()->includeHead ( "navigationTop" );
	preProcessor::get ()->includeHead ( "pageFooter" );
	preProcessor::get ()->includeHead ( "registerPanel" );
};

$bodyHeaderFunction = function () {
	preProcessor::get ()->includeBody ( "navigationTop" );
};

$bodyFunction = function () {
	preProcessor::get ()->includeBody ( "registerPanel" );
};

$bodyFooterFunction = function () {
	preProcessor::get ()->includeBody ( "pageFooter" );
};

$pageTitle = "Seen.it";
include "defaultPage.php";

?>