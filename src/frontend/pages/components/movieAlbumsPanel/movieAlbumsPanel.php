<!DOCTYPE html>
<html lang="en">
<head>
    <?php preProcessor::get()->includeHead("loginRegisterPanel")?>
    <?php preProcessor::get()->includeHead("loggedInPanel")?>
    <?php preProcessor::get()->includeHead("searchPanel")?>
	<script generic="true" src="js/jquery.min.js"></script>
<script generic="true" src="js/bootstrap.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
<link generic="true" rel="stylesheet" href="css/theme.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-dialog.min.css">
<link generic="false" rel="stylesheet" href="movieAlbumsPanel.css">
<script generic="false" src="movieAlbumsPanel.js"></script>
<script generic="true" src="js/ajax.js"></script>
<script generic="true" src="js/bus.js"></script>
<script generic="true" src="js/safe.js"></script>
<script generic="true" src="js/sorttable.js"></script>
<script generic="true" src="js/bootstrap-dialog.min.js"></script>
</head>
<body>
<?php
$user = Config::getConfig ()->getSession ()->getUser ();
if (! $user) { // TODO: This should never happen, yet it happens a lot actually
	redirect ( "" );
}
$currentData = $currentData = viewState::getAllMoviesForUser ( $user );
$totalcount = MovieList::getUserTotalMovieCount ( $user );
$friendsAlbums = QuickAlbumDetails::getAllForUserFriends ( $user );
$fas = array ();
foreach ( $friendsAlbums as $album ) {
	$fas [$album->getOwnerName ()] [] = $album;
}
?>
	<div class="container-fluid seenit_panel well">
		<div class="seenit_panel_title">My movies</div>


		<div class="flex-container-row">
			<div class="fiex-item movieAlbumPanel_navPanel">
				<div>
					<ul class="nav nav-tabs nav-stacked" role="tablist" id="mainAlbumsPanel_albumRoot">
						<li id="mainAlbumsPanel_loadFirst" class="pointer"><a role="tab"><span class="badge float-right"><?php echo $totalcount;?></span> <spna>All my movies</spna></a></li>
						<li class="tree-toggle li-level-1"><span class="glyphicon glyphicon-user"></span> <label>My albums</label>
							<ul class="nav nav-tabs nav-stacked" role="tablist" id="movieAlbumsPanel_appender">
						<?php
						foreach ( $currentData as $data ) {
							$pub = "<span></span>";
							if ($data->isPublic ()) {
								$pub = '<span class="glyphicon glyphicon-globe bigGlyph" title="Public album"></span>&nbsp;';
							} else if ($data->isSharedBetweenFriends ()) {
								$pub = '<span class="glyphicon glyphicon-user bigGlyph" title="Shared with friends"></span>&nbsp;';
							}
							echo '<li class="li-level-1 pointer" onClick="movieAlbumPanel_instance.loadAlbum(this);"><a role="tab" class="movieAlbumsPanel_disablable">' . $pub . '<span class="badge float-right">' . $data->getAlbumContentCount () . '</span><span ours="' . ($data->getOwnerID () == $user->getID () ? "true" : "false") . '" name="album_' . $data->getAlbumID () . '">' . Safe::escape ( $data->getAlbumName () ) . '</span></a></li>';
						}
						?>
						<li class="li-level-1 pointer" id="movieAlbumsPanel_addAlbumRow"><a role="tab" onClick="movieAlbumPanel_instance.newAlbumHandler(1);"><span class="glyphicon glyphicon-plus text-success"></span> New album</a> <a role="tab" class="hidden">
										<div class="flex-container-row flex-vcenter">
											<div>
												<input type="text" class="form-control">
											</div>
											<div class="flex-item">
												<span class="glyphicon bigGlyph glyphicon-ok text-success pointer" onClick="movieAlbumPanel_instance.newAlbumHandler(2);"></span>
												<span class="glyphicon bigGlyph glyphicon-remove text-danger pointer" onClick="movieAlbumPanel_instance.newAlbumHandler(3);"></span>
											</div>
										</div>
								</a> <a role="tab" class="hidden"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate text-info"></span> Creating album</a></li>
							</ul></li>
						<li class="tree-toggle li-level-1"><span class="glyphicon glyphicon-heart"></span> <label>Friends albums</label>
							<ul class="nav nav-tabs nav-stacked" role="tablist">
						<?php
						foreach ( $fas as $friend => $albums ) {
							echo '<li class="tree-toggle li-level-1"><span class="glyphicon glyphicon-user"></span> <label>' . Safe::escape ( $friend ) . '</label><ul class="nav nav-tabs nav-stacked" role="tablist">';
							foreach ( $albums as $album ) {
								$pub = "<span></span>";
								if ($album->isPublic ()) {
									$pub = '<span class="glyphicon glyphicon-globe bigGlyph" title="Public album"></span>&nbsp;';
								} else if ($album->isSharedBetweenFriends ()) {
									$pub = '<span class="glyphicon glyphicon-user bigGlyph" title="Shared with friends"></span>&nbsp;';
								}
								if ($pub != "<span></span>") {
									echo '<li class="li-level-1 pointer" onClick="movieAlbumPanel_instance.loadAlbum(this);"><a role="tab" class="movieAlbumsPanel_disablable">' . $pub . '<span class="badge float-right">' . $album->getAlbumContentCount () . '</span><span ours="' . ($album->getOwnerID () == $user->getID () ? "true" : "false") . '" name="album_' . $album->getAlbumID () . '">' . Safe::escape ( $album->getAlbumName () ) . '</span></a></li>';
								}
								// echo '<li class="li-level-1 pointer" onClick="movieAlbumPanel_instance.loadAlbum(this);" ><a role="tab"><span class="badge float-right">' . $album->getAlbumContentCount () . '</span><span ours="' . ($album->getOwnerID () == $user->getID () ? "true" : "false") . '" name="album_' . $album->getAlbumID () . '">' . Safe::escape ( $album->getAlbumName () ) . '</span></a></li>';
							}
							echo '</ul></li>';
						}
						?></ul></li>
					</ul>
				</div>
			</div>
			<div class="flex-item flex-container-column movieAlbumPanel_navWrapper">
				<div class="tab-content flex-item fullheight">
					<div role="tabpanel" class="tab-pane active" id="overview">
						<div class="movieAlbumPanel_overview"></div>
					</div>
					<div class="fullwidth">
						<div class="flex-container border-bottom padding-bottom-5 padding-top-5">
							<div class="flex-item">
								<input type="text" class="form-control" placeholder="Filtering">
							</div>
							<div class="movieAlbumPanel_pagerWrapper">
								<label>Movies on page:</label>
							</div>
							<div class="movieAlbumPanel_selectWrapper">
								<select class="form-control" id="movieAlbumPanel_onPageSel">
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select>
							</div>
						</div>
						<table class="table sortable tab-contents">
							<thead>
								<tr>
									<th class="sorttable_nosort" id="movieAlbumsPanel_selectAllTh"><input type="checkbox" /></th>
									<th>Movie name</th>
									<th class="text-center">Already seen</th>
									<th class="text-center">Seen date</th>
									<th class="sorttable_nosort text-right">Actions</th>
								</tr>
							</thead>
							<tbody id="movieAlbumsPanel_albumContents">
							</tbody>
						</table>
					</div>
				</div>
				<div class="text-center">
					<ul class="pagination" id="movieAlbumsPanel_pagination">
					</ul>
				</div>
				<div class="well flex-row" id="movieAlbumsPanel_bottomPanel">
					<div class="flex-item">
						<label>Album name:</label>
						<span>
							<label class="text-muted"></label>
							<span class="margin-left-5 glyphicon glyphicon-edit text-info pointer bigGlyph" title="Change"></span>
							<span class="margin-left-5 glyphicon glyphicon-minus-sign text-danger pointer bigGlyph" title="Delete album"></span>
						</span>
						<span class="hidden">
							<input size=10 type="text" />
							<span class="margin-left-5 glyphicon glyphicon-ok text-success pointer bigGlyph" title="Save"></span>
							<span class="margin-left-5 glyphicon glyphicon-remove text-danger pointer bigGlyph" title="Cancel"></span>
						</span>
						<span class="hidden">
							<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate text-info" title="Saving"></span>
						</span>

					</div>
					<div>
						<div class="form-group form-inline">
							<label for="sel1">With selected:</label> <select class="form-control selectpicker" id="movieAlbumsPanel_targetList">
								<option value="-1">All movies</option>
								<optgroup label="My albums">
								<?php
								foreach ( $currentData as $album ) {
									echo '<option value="' . $album->getAlbumID () . '">' . Safe::escape ( $album->getAlbumName () ) . '</option>';
								}
								?>
								</optgroup>
							</select>
							<div class="btn-group" id="movieAlbumsPanel_opBtns">
								<button type="button" class="btn btn-info">Copy</button>
								<button type="button" class="btn btn-info">Move</button>
								<button type="button" class="btn btn-danger">Delete</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>