var movieAlbumPanel_instance;

var movieAlbumPanel_M = function() {

	this.selectedAlbum = false;
	this.isLoadingAlbumContents = false;
	this.currentlyLoadingAlbum = false;
	this.currentAlbum = false;

	var instance = this;
	this.root = document.getElementById('mainAlbumsPanel_albumRoot');
	this.mroot = document.getElementById('movieAlbumsPanel_albumContents');
	this.allMovies = new movieAlbumPanel_allMovies_M(instance.root.children[0]);
	this.checkAllObj = document.getElementById('movieAlbumsPanel_selectAllTh').children[0];

	this.movieImportedCallback=function(movieData) {
		var movieId=movieData["MovieID"];
		var toAllMovies=movieData["ToAllMovies"];
		var albumId=movieData["AlbumID"];
		var created=movieData["Created"];
		var added=movieData["Added"];
		instance.allMovies.setCount(instance.allMovies.getCount()+created);
		if(!toAllMovies) {
			var album=instance.getAlbumById(albumId);
			if(album) {
				album.setCount(album.getCount()+added);		
			}
		}
		
	}
	
	this.getAlbums = function() {
		var albums = [];
		var objs = instance.root.children[1].children[2].children;
		for (var i = 0; i < objs.length - 1; i++) {
			var album = new movieAlbumPanel_album_M(objs[i]);
			albums.push(album);
		}
		return albums;
	}

	this.getCurrentMovies = function() {
		var movies = [];
		var objs = instance.mroot.children;
		for (var i = 0; i < objs.length; i++) {
			var movie = new movieAlbumPanel_movie_M(objs[i]);
			movies.push(movie);
		}
		return movies;
	}

	this.selectAlbum = function(obj) {
		instance.selectedAlbum = new movieAlbumPanel_M(obj);
	}

	this.getSelectedAlbum = function() {
		return instance.selectedAlbum;
	}
	
	this.currentAlbumPage=1;

	this.loadAllMovies = function() {
		if (!instance.isLoadingAlbumContents) {
			if(instance.currentAlbum !== false) {
				instance.currentAlbumPage=1;
			}
			instance.currentAlbum = false;
			instance.setLoadingAlbumContents(true, instance.allMovies.obj);
			var ajax = new Ajax();
			ajax.asyncQuery("getAllMyMovies",{"page":instance.currentAlbumPage,"count":instance.getToDisplayMoviesCount()}, function(result) {
				var movieCount = instance.populateMovieData(false, true, -1, result);
				instance.setLoadingAlbumContents(false, instance.allMovies.obj);
			});
		}
	}
	
	this.reloadAlbum=function() {
		if(instance.currentAlbum) {
			instance.loadAlbum(instance.currentAlbum.albumObj);
		} else {
			instance.loadAllMovies();
		}
	}

	this.loadAlbum = function(albumObject) {
		if (!instance.isLoadingAlbumContents) {
			instance.setLoadingAlbumContents(true, albumObject);
			var album = new movieAlbumPanel_album_M(albumObject);
			if(instance.currentAlbum) {
				if(instance.currentAlbum.getAlbumID()!=album.getAlbumID()) {
					instance.currentAlbumPage=1;
				}
			} else {
				instance.currentAlbumPage=1;
			}
			instance.currentAlbum = album;
			var ajax = new Ajax();
			ajax.asyncQuery("getMoviesInAlbum", {
				"albumId" : album.getAlbumID(),
				"page":instance.currentAlbumPage,
				"count":instance.getToDisplayMoviesCount()
			}, function(result) {
				var movieCount = instance.populateMovieData(album.isOurs(), false, album.getAlbumID(), result);
				instance.setLoadingAlbumContents(false, albumObject);
			});
		}
	}

	this.populateMovieData = function(ourAlbum, allMovies, albumId, rawData) {
		instance.updateBottomPanel(ourAlbum, allMovies);
		instance.checkAllObj.checked = false;
		var res = JSON.parse(rawData);
		if (res.Status == "Ok") {
			var data = res.Data;
			var total=res["Total"];
			if(allMovies) {
				instance.allMovies.setCount(total);
			} else {
				instance.currentAlbum.setCount(total);
			}
			var tb = instance.mroot;
			tb.innerHTML = "";
			for (var i = 0; i < data.length; i++) {
				var d = data[i];
				var movie = instance.getMovieRowFromData(albumId, ourAlbum, allMovies, d);
				EventBus.dispatchEvent("SeenChanged",{"MovieID":movie.getMovieID(),"Seen":movie.hasSeen(),"Origin":"Load"});
				tb.appendChild(movie.obj);
			}
			
			var page=res["Page"];
			var count=res["Count"];
			var pages=Math.ceil(total/count);
			if(page > pages) {
				page=1;
			}
			instance.setPageCount(pages);
			instance.setActivePage(page);
			return total;
		} else {
			instance.setOperationErrorDialog();
		}
		return 0;
	}

	this.setLoadingAlbumContents = function(loading, albumObject) {
		if (loading) {
			if (!instance.isLoadingAlbumContents) {
				instance.isLoadingAlbumContents = true;
				if (instance.currentlyLoadingAlbum) {
					instance.currentlyLoadingAlbum.classList.remove("active");
				}
				instance.currentlyLoadingAlbum = albumObject;
				var tb = instance.mroot;
				tb.innerHTML = "";
				var qtr = document.createElement("tr");
				var qtd = document.createElement("td");
				qtd.colSpan = 4;
				qtd.classList.add("text-center");
				var td_l = document.createElement("span");
				td_l.classList.add("bigGlyph");
				td_l.classList.add("glyphicon");
				td_l.classList.add("glyphicon-refresh");
				td_l.classList.add("glyphicon-refresh-animate");
				td_l.classList.add("text-info");
				td_l.title = "Processing";
				qtd.appendChild(td_l);
				qtr.appendChild(qtd);
				tb.appendChild(qtr);
			}
		} else {
			if (instance.isLoadingAlbumContents) {
				albumObject.classList.add("active");
				instance.isLoadingAlbumContents = false;
			}
		}

	}

	this.init = function() {
		document.getElementById('mainAlbumsPanel_loadFirst').addEventListener("click", instance.loadAllMovies);
		instance.loadAllMovies();
	}

	this.getMovieRowFromData = function(albumId, ourAlbum, allMovies, data) {
		var movieId = data["movieId"];
		var albumMovieId = data["albumMovieId"];
		var movieListId = data["movieListId"];
		var name = data["name"];
		var seen = data["seen"] ? true : false;
		var seenTitle = seen ? "Already seen" : "Not seen yet";
		var seenDate = data["date"];
		var seenHidden = seen ? "hidden" : "";
		var seenColor = seen ? "text-warning" : "text-success";
		var seenColor2 = seen ? "text-success" : "text-danger";
		var seenGlyph = seen ? "glyphicon-ok" : "glyphicon-remove";
		var seenText = seen ? "Mark as unseen" : "Mark as seen";
		var seenEye = seen ? "glyphicon-eye-close" : "glyphicon-eye-open";

		var tr = document.createElement("tr");
		var td0 = document.createElement("td");
		var td1 = document.createElement("td");
		var td2 = document.createElement("td");
		td2.classList.add("text-center");
		var td3 = document.createElement("td");
		td3.classList.add("text-center");
		var td4 = document.createElement("td");
		td4.classList.add("text-right");

		var td0_cb = document.createElement("input");
		td0_cb.type = "checkbox";
		td0_cb.setAttribute("movieId", movieId);
		td0.appendChild(td0_cb);

		var td1_span = document.createElement("a");
		td1_span.href = '/web/movie/' + movieId;
		td1_span.innerHTML = Safe.escape(name);
		td1.appendChild(td1_span);

		var td2_span = document.createElement("span");
		td2_span.classList.add("bigGlyph");
		td2_span.classList.add("glyphicon");
		td2_span.classList.add(seenGlyph);
		td2_span.classList.add(seenColor2);
		td2_span.title = seenTitle;
		td2.appendChild(td2_span);

		td3.innerHTML = Safe.escape(seenDate);

		var td4_span_1 = document.createElement("span");
		var td4_span_2 = document.createElement("span");
		var td4_span_3 = document.createElement("span");

		td4_span_1.classList.add("bigGlyph");
		td4_span_1.classList.add("glyphicon");
		td4_span_1.classList.add("pointer");
		td4_span_1.classList.add(seenEye);
		td4_span_1.classList.add(seenColor);
		td4_span_1.title = seenText;
		td4_span_1.style.marginRight = "5px";

		td4_span_2.classList.add("bigGlyph");
		td4_span_2.classList.add("glyphicon");
		td4_span_2.classList.add("glyphicon-refresh");
		td4_span_2.classList.add("glyphicon-refresh-animate");
		td4_span_2.classList.add("hidden");
		td4_span_2.classList.add("text-info");
		td4_span_2.title = "Processing";

		td4_span_3.classList.add("bigGlyph");
		td4_span_3.classList.add("glyphicon");
		td4_span_3.classList.add("pointer");
		td4_span_3.classList.add("text-danger");
		td4_span_3.classList.add("glyphicon-minus-sign");
		td4_span_3.title = allMovies?"Remove from my list":"Remove from album";

		td4.appendChild(td4_span_1);
		td4.appendChild(td4_span_2);
		if (ourAlbum || allMovies) {
			td4.appendChild(td4_span_3);
		}

		tr.appendChild(td0);
		tr.appendChild(td1);
		tr.appendChild(td2);
		tr.appendChild(td3);
		tr.appendChild(td4);

		var movie = new movieAlbumPanel_movie_M(tr);
		td4_span_1.addEventListener("click", function() {
			movie.seenClicked();
		});
		td4_span_3.addEventListener("click", function() {
			if (ourAlbum) {
				movie.deleteMovieFromAlbum(albumId, movieListId);
			} else if (allMovies) {
				movie.deleteMovieFromMyMovies(movieListId);
			}
		});
		return movie;
	}

	this.setOperationErrorDialog = function() {
		BootstrapDialog.show({
			type : BootstrapDialog.TYPE_DANGER,
			title : 'Operation failed',
			message : 'Your last operation was not saved. Please refresh the page.',
		});
	}

	this.newAlbumRow = document.getElementById('movieAlbumsPanel_addAlbumRow');
	this.newAlbumRowTitleObj = this.newAlbumRow.children[0];
	this.newAlbumRowEditObj = this.newAlbumRow.children[1];
	this.newAlbumRowLoadingObj = this.newAlbumRow.children[2];
	this.newAlbumRowInput = this.newAlbumRowEditObj.children[0].children[0].children[0];

	this.newAlbumRowInput.addEventListener("keydown", function(key) {
		if (key.keyCode == 13) {
			instance.newAlbumHandler(2);
		} else if (key.code == "Escape") {
			instance.newAlbumHandler(3);
		}
	});

	this.newAlbumHandler = function(status) {
		if (status == 1) {
			instance.newAlbumRowTitleObj.classList.add("hidden");
			instance.newAlbumRowEditObj.classList.remove("hidden");
			instance.newAlbumRowInput.focus();
			instance.newAlbumRowInput.value="";
		} else if (status == 2) {
			instance.newAlbumRowEditObj.classList.add("hidden");
			instance.newAlbumRowLoadingObj.classList.remove("hidden");
			var albumName = instance.newAlbumRowInput.value;
			var ajax = new Ajax();
			ajax.asyncQuery("newAlbum", {
				"albumName" : albumName
			}, function(result) {
				var res = JSON.parse(result);
				if (res) {
					if (res["Status"] == "Ok") {
						var data = res["Data"];
						var albumId = data["AlbumID"];
						var albumCount = data["AlbumCount"];
						var albumName = data["AlbumName"];

						var pli = document.createElement("li");
						pli.classList.add("li-level-1");
						pli.classList.add("pointer");
						
						var aspan=document.createElement("span");

						var pla = document.createElement("a");
						pla.setAttribute("role", "tab");
						pla.classList.add("movieAlbumsPanel_disablable");
						pla.appendChild(aspan);

						var plspan = document.createElement("span");
						plspan.classList.add("badge");
						plspan.classList.add("float-right");
						plspan.innerHTML = Safe.escape(albumCount);

						var plispan = document.createElement("span");
						plispan.setAttribute("name", "album_" + albumId);
						plispan.setAttribute("ours", "true");
						plispan.innerHTML = Safe.escape(albumName);

						pla.appendChild(plspan);
						pla.appendChild(plispan);
						pli.appendChild(pla);

						var root = document.getElementById('movieAlbumsPanel_appender');
						root.insertBefore(pli, root.children[root.children.length - 1]);

						pli.addEventListener("click", function() {
							instance.loadAlbum(pli);
						});
						instance.newAlbumRowTitleObj.classList.remove("hidden");
						instance.newAlbumRowLoadingObj.classList.add("hidden");
						var album = new movieAlbumPanel_album_M(pli);
						instance.albumAdded(album);
						return;
					}
				}
				instance.setOperationErrorDialog();
			});

		} else if (status == 3) {
			instance.newAlbumRowTitleObj.classList.remove("hidden");
			instance.newAlbumRowEditObj.classList.add("hidden");
		}
	}

	this.editAlbumNameRow = document.getElementById('movieAlbumsPanel_bottomPanel').children[0];
	this.editAlbumNameTitleObj = this.editAlbumNameRow.children[1];
	this.editAlbumNameTitleLabel = this.editAlbumNameTitleObj.children[0];
	this.editAlbumNameEditObj = this.editAlbumNameRow.children[2];
	this.editAlbumNameLoadingObj = this.editAlbumNameRow.children[3];
	this.editAlbumNameInput = this.editAlbumNameEditObj.children[0];
	this.editAlbumNameIconOK = this.editAlbumNameEditObj.children[1];
	this.editAlbumNameIconCancel = this.editAlbumNameEditObj.children[2];
	this.editAlbumNameIconEdit = this.editAlbumNameTitleObj.children[1];
	this.editAlbumNameIconRemove = this.editAlbumNameTitleObj.children[2];

	this.editAlbumNameInput.addEventListener("keydown", function(key) {
		if (key.keyCode == 13) {
			instance.albumNameHandler(2);
		} else if (key.code == "Escape") {
			instance.albumNameHandler(3);
		}
	});
	this.editAlbumNameIconEdit.addEventListener("click", function() {
		instance.albumNameHandler(1);
	});
	this.editAlbumNameIconOK.addEventListener("click", function() {
		instance.albumNameHandler(2);
	});
	this.editAlbumNameIconCancel.addEventListener("click", function() {
		instance.albumNameHandler(3);
	});
	this.editAlbumNameIconRemove.addEventListener("click", function() {
		instance.deleteCurrentAlbum();
	});

	this.albumNameHandler = function(status) {
		if (status == 1) {
			instance.editAlbumNameTitleObj.classList.add("hidden");
			instance.editAlbumNameEditObj.classList.remove("hidden");
			instance.editAlbumNameInput.value = Safe.descape(instance.editAlbumNameTitleLabel.innerHTML.trim());
			instance.editAlbumNameInput.focus();
		} else if (status == 2) {
			instance.editAlbumNameEditObj.classList.add("hidden");
			instance.editAlbumNameLoadingObj.classList.remove("hidden");
			var albumName = instance.editAlbumNameInput.value;
			var albumId = instance.currentAlbum.getAlbumID();
			var ajax = new Ajax();
			ajax.asyncQuery("changeAlbumName", {
				"albumId" : albumId,
				"albumName" : albumName
			}, function(result) {
				var res = JSON.parse(result);
				if (res["Status"] == "Ok") {
					var data = res["Data"]["AlbumName"];
					instance.editAlbumNameTitleLabel.innerHTML = Safe.escape(data);
					instance.currentAlbum.setName(data);
					instance.editAlbumNameLoadingObj.classList.add("hidden");
					instance.editAlbumNameTitleObj.classList.remove("hidden");
					instance.albumRenamed(albumId, data);
					return;
				}
				instance.editAlbumNameLoadingObj.classList.add("hidden");
				instance.editAlbumNameTitleObj.classList.remove("hidden");
				instance.setOperationErrorDialog();
			});
		} else if (status == 3) {
			instance.editAlbumNameTitleObj.classList.remove("hidden");
			instance.editAlbumNameEditObj.classList.add("hidden");
		}
	}

	this.selectAllClicked = function() {
		var select = instance.checkAllObj.checked;
		var movies = instance.getCurrentMovies();
		movies.forEach(function(movie) {
			movie.setChecked(select);
		});
	}

	instance.checkAllObj.addEventListener("click", this.selectAllClicked);

	this.getSelectedMovies = function() {
		var movies = instance.getCurrentMovies();
		var sel = [];
		movies.forEach(function(movie) {
			if (movie.isChecked()) {
				sel.push(movie);
			}
		});
		return sel;
	}

	this.albumTargetList = document.getElementById('movieAlbumsPanel_targetList');

	this.albumAdded = function(album) {
		var qsel = instance.albumTargetList.children[1];
		var qnew = document.createElement("option");
		qnew.setAttribute("value", album.getAlbumID());
		qnew.innerHTML = Safe.escape(album.getName());
		qsel.appendChild(qnew);
		EventBus.dispatchEvent("AlbumAdded",{"AlbumID":album.getAlbumID(),"AlbumName":album.getName()});
	}

	this.albumDeleted = function(album) {
		var qsel = instance.albumTargetList.children[1];
		var qp = qsel.children;
		albumId = album.getAlbumID();
		for (var i = 0; i < qp.length; i++) {
			var qs = qp[i];
			var aid = qs.getAttribute("value");
			if (aid == albumId) {
				qs.remove();
				EventBus.dispatchEvent("AlbumDeleted",{"AlbumID":album.getAlbumID(),"AlbumName":album.getName()});
				return;
			}
		}
	}

	this.albumRenamed = function(albumId, albumNewName) {
		var qsel = instance.albumTargetList.children[1];
		var qp = qsel.children;
		for (var i = 0; i < qp.length; i++) {
			var qs = qp[i];
			var aid = qs.getAttribute("value");
			if (aid == albumId) {
				qs.innerHTML = Safe.escape(albumNewName);
				EventBus.dispatchEvent("AlbumRenamed",{"AlbumID":albumId,"AlbumName":albumNewName});
				return;
			}
		}
	}

	this.updateBottomPanel = function(ourAlbum, allMovies) {
		instance.albumNameHandler(3);
		instance.editAlbumNameRow.classList.remove("hidden");
		instance.btnCopy.classList.remove("hidden");
		instance.btnMove.classList.remove("hidden");
		instance.btnDelete.classList.remove("hidden");
		if (allMovies) {
			instance.editAlbumNameRow.classList.add("hidden");
			instance.btnMove.classList.add("hidden");
		} else {
			var album = instance.currentAlbum;
			instance.editAlbumNameIconEdit.classList.remove("hidden");
			instance.editAlbumNameIconRemove.classList.remove("hidden");
			instance.editAlbumNameTitleLabel.innerHTML = Safe.escape(album.getName());
			if (!ourAlbum) {
				instance.editAlbumNameIconEdit.classList.add("hidden");
				instance.editAlbumNameIconRemove.classList.add("hidden");
				instance.btnMove.classList.add("hidden");
				instance.btnDelete.classList.add("hidden");
			}
		}
	}

	this.processingDialog = false;

	this.showProcessingDialog = function() {
		if (!instance.processingDialog) {
			instance.processingDialog = BootstrapDialog.show({type:BootstrapDialog.TYPE_SUCCESS,title:"Processing",message:"Processing"});
			var qbody = instance.processingDialog.getModalBody()[0];
			var rc = qbody.children[0];
			rc.innerHTML = '<p>Processing</p><div class="progress progress-striped active"><div class="progress-bar progress-bar-striped progress-bar-info active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div></div>';
			instance.processingDialog.setClosable(false);
		}
	}


	this.hideProcessingDialog = function(timeout=false) {
		if(timeout) {
			window.setTimeout(function() {instance.hideProcessingDialog(false);},timeout);
		} else {
			if (instance.processingDialog) {
				instance.processingDialog.close();
				instance.processingDialog=false;
			}
		}
	}


	this.deleteAlbum = function(dialog, album) {
		var qbody = dialog.getModalBody()[0];
		var rc = qbody.children[0];
		rc.innerHTML = '<p>Processing</p><div class="progress progress-striped active"><div class="progress-bar progress-bar-striped progress-bar-danger active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div></div>';
		var ajax = new Ajax();
		ajax.asyncQuery("deleteAlbum", {
			"albumId" : album.getAlbumID()
		}, function(result) {
			var res = JSON.parse(result);
			if (res) {
				if (res["Status"] == "Ok") {
					dialog.close();
					instance.albumDeleted(album);
					album.remove();
					instance.loadAllMovies();
					return;
				}
			}
			instance.setOperationErrorDialog();
		});
	}

	this.deleteCurrentAlbum = function() {
		var album = instance.currentAlbum;
		if (album) {
			BootstrapDialog.show({
				message : 'You are about to remove entire album!\nMovies inside the album are not deleted, you will still be able to find them in other albums and All movies.\nPlease confirm that',
				type : BootstrapDialog.TYPE_DANGER,
				buttons : [ {
					title : 'Deleting album',
					label : 'Delete',
					cssClass : 'btn-danger',
					action : function(dialog) {
						dialog.setClosable(false);
						dialog.enableButtons(false);
						instance.deleteAlbum(dialog, album);
					},
					icon : 'glyphicon glyphicon-delete',
				}, {
					label : 'Cancel',
					action : function(dialog) {
						dialog.close();
					}
				} ]
			});
		}
	}

	this.getMovieIds = function(movieList) {
		var lst = [];
		movieList.forEach(function(movie) {
			lst.push(movie.getMovieID());
		});
		return lst;
	}

	this.bottomPanelObj = document.getElementById('movieAlbumsPanel_targetList');

	this.getBottomPanelSelectedAlbum = function() {
		return instance.getAlbumById(instance.bottomPanelObj.options[instance.bottomPanelObj.selectedIndex].value * 1);
	}

	this.getAlbumById = function(albumId) {
		if (albumId == -1) {
			return instance.allMovies;
		} else {
			var albums = instance.getAlbums();
			var res = false;
			albums.forEach(function(album) {
				if (album.getAlbumID() * 1 == albumId * 1) {
					res = album;
					return;
				}
			});
		}
		return res;
	}

	// TODO: Add confirmation
	this.deleteSelectedMovies=function() {
		var omovies = instance.getSelectedMovies();
		var movies = instance.getMovieIds(omovies).join(":");
		if (omovies.length > 0) {
			BootstrapDialog.show({
				message : 'Are you sure to delete selected movies ('+omovies.length+') from current album? ',
				type : BootstrapDialog.TYPE_DANGER,
				buttons : [ {
					label : 'Cancel',
					action : function(dialog) {
						dialog.close();
					}
				}, {
					label : 'Delete',
					cssClass : 'btn-danger',
					action : function(dialog) {
						dialog.close();
						instance.deleteSelectedMovieConfirmed(omovies,movies);
					},
					icon : 'glyphicon glyphicon-delete',
				} ]
			});
		}};
			
	this.deleteSelectedMovieConfirmed=function(omovies, movies) {
		var fromAlbum=instance.currentAlbum;
		var action;
		var albumId=-1;
		if(!fromAlbum) {
			action="deleteMoviesFromList";
		} else {
			action="deleteMovies";
			if (!fromAlbum.isOurs()) {
				instance.setOperationErrorDialog();
				return;
			}
			albumId=fromAlbum.getAlbumID();
		}
		instance.showProcessingDialog();
		var ajax = new Ajax();
		ajax.asyncQuery(action, {
			"data" : movies,
			"albumId" : albumId
		}, function(result) {
			var res = JSON.parse(result);
			if (res) {
				if (res["Status"] == "Ok") {
					var deleted = res["Deleted"];
					instance.allMovies.setCount(instance.allMovies.getCount() - deleted);
					if(fromAlbum) {
						fromAlbum.setCount(fromAlbum.getCount()-deleted);
					}
					omovies.forEach(function(movie){movie.remove();});
					instance.hideProcessingDialog(1000);
					return;
				}
			}
			instance.hideProcessingDialog();
			instance.setOperationErrorDialog();
		});
	}

	this.moveSelectedMovies = function() {
		var omovies = instance.getSelectedMovies();
		var movies = instance.getMovieIds(omovies).join(":");
		if (movies.length > 0) {
			var toAlbum = instance.getBottomPanelSelectedAlbum();
			var fromAlbum=instance.currentAlbum;
			if(!fromAlbum) {
				instance.setOperationErrorDialog();
			}
			var fromAlbumId=fromAlbum.getAlbumID();
			if (!toAlbum) {
				instance.setOperationErrorDialog();
				return;
			}
			if (toAlbum.isAllMovies()) {
				instance.setOperationErrorDialog();
				return;
			}
			if (!toAlbum.isOurs()) {
				instance.setOperationErrorDialog();
				return;
			}
			instance.showProcessingDialog();
			var ajax = new Ajax();
			ajax.asyncQuery("moveMovies", {
				"data" : movies,
				"albumId" : toAlbum.getAlbumID(),
				"sourceId" : fromAlbumId
			}, function(result) {
				var res = JSON.parse(result);
				if (res) {
					if (res["Status"] == "Ok") {
						var added = res["Added"];
						toAlbum.setCount(toAlbum.getCount() + added);
						fromAlbum.setCount(fromAlbum.getCount() - added);
						omovies.forEach(function(movie){movie.remove();});
						instance.hideProcessingDialog(1000);
						return;
					}
				}
				instance.hideProcessingDialog();
				instance.setOperationErrorDialog();
			});
		}
	}
	
	this.copySelectedMovies = function() {
		var movies = instance.getSelectedMovies();
		movies = instance.getMovieIds(movies).join(":");
		if (movies.length > 0) {
			var toAlbum = instance.getBottomPanelSelectedAlbum();
			if (!toAlbum) {
				console.log("A");
				instance.setOperationErrorDialog();
				return;
			}
			if (toAlbum.isAllMovies()) {
				if(instance.currentAlbum.isOurs()) {
					instance.setOperationErrorDialog();
					return;
				}
			}
			var action;
			var albumId;
			if(toAlbum.isAllMovies()) {
				action="importMovies";
				albumId=-1;
			} else {
				if (!toAlbum.isOurs()) {
					instance.setOperationErrorDialog();
					return;
				}
				action="copyMovies";
				albumId=toAlbum.getAlbumID();
			}
			instance.showProcessingDialog();
			var ajax = new Ajax();
			ajax.asyncQuery(action, {
				"data" : movies,
				"albumId" : albumId
			}, function(result) {
				var res = JSON.parse(result);
				if (res) {
					if (res["Status"] == "Ok") {
						var added = res["Added"];
						var created = res["Created"];
						instance.allMovies.setCount(instance.allMovies.getCount() + created);
						toAlbum.setCount(toAlbum.getCount() + added);
						instance.hideProcessingDialog(1000);
						return;
					}
				}
				instance.hideProcessingDialog();
				instance.setOperationErrorDialog();
			});
		}
	}

	this.btnRow = document.getElementById('movieAlbumsPanel_opBtns');
	this.btnCopy = instance.btnRow.children[0];
	this.btnMove = instance.btnRow.children[1];
	this.btnDelete = instance.btnRow.children[2];

	this.btnCopy.addEventListener("click", instance.copySelectedMovies);
	this.btnMove.addEventListener("click", instance.moveSelectedMovies);
	this.btnDelete.addEventListener("click", instance.deleteSelectedMovies);
	
	
	this.pagination=document.getElementById('movieAlbumsPanel_pagination');
	this.pageSelect=document.getElementById('movieAlbumPanel_onPageSel');
	
	this.pageSelect.addEventListener("change",function() {
		instance.currentAlbumPage=1;
		instance.reloadAlbum();
	});
	
	this.getToDisplayMoviesCount=function() {
		return instance.pageSelect.options[instance.pageSelect.selectedIndex].value;
	}
	
	this.setPageCount=function(count) {
		instance.pagination.innerHTML="";
		for(var i=0;i<count;i++) {
			var elem=instance.getGeneratedPage(i);
			instance.pagination.appendChild(elem);
		}
	}
	
	this.getGeneratedPage=function(index) {
		var elem=document.createElement("li");
		var a=document.createElement("a");
		elem.appendChild(a);
		a.innerHTML=(index+1)+"";
		a.classList.add("pointer");
		a.addEventListener("click",function(){instance.setActivePage(index+1)});
		return elem;
	}

	this.setActivePage=function(pageNumber) {
		var page=instance.pagination.children[pageNumber-1];
		var pages=instance.pagination.children.length;
		for(var i=0;i<pages;i++) {
			var pg=instance.pagination.children[i];
			pg.classList.remove("active");
		}
		if(page) {
			page.classList.add("active");
			instance.currentAlbumPage=pageNumber;
			instance.reloadAlbum();
		}
	}
}

var movieAlbumPanel_allMovies_M = function(object) {
	this.obj = object;
	var instance = this;
	this.countObject = this.obj.children[0].children[0];
	this.albumTitle = this.obj.children[0].children[1];
	this.getCount = function() {
		return instance.countObject.innerHTML * 1;
	}
	this.setCount = function(count) {
		instance.countObject.innerHTML = count;
	}
	this.isAllMovies = function() {
		return true;
	}
}

var movieAlbumPanel_album_M = function(albumObject) {
	this.albumObj = albumObject;
	this.albumNameObject = albumObject.children[0].children[2];
	this.countObject = albumObject.children[0].children[1];
	this.albumID = this.albumNameObject.getAttribute("name").split("_")[1] * 1;
	this.isOursv = this.albumNameObject.getAttribute("ours") == "true" ? true : false;
	var instance = this;
	
	this.isOurs = function() {
		return instance.isOursv;
	}

	this.isAllMovies = function() {
		return false;
	}

	this.setName = function(name) {
		instance.albumNameObject.innerHTML = Safe.escape(name);
	}

	this.getName = function() {
		return Safe.descape(instance.albumNameObject.innerHTML);
	}

	this.getAlbumID = function() {
		return instance.albumID;
	}

	this.getCount = function() {
		return instance.countObject.innerHTML * 1;
	}

	this.setCount = function(count) {
		instance.countObject.innerHTML = count;
	}

	this.remove = function() {
		instance.albumObj.remove();
	}

}

var movieAlbumPanel_movie_M = function(object) {
	this.obj = object;
	var instance = this;
	this.checkBox = this.obj.children[0].children[0];
	this.movieNameObj = this.obj.children[1].children[0];
	this.seenObj = this.obj.children[2].children[0];
	this.seenDateObj = this.obj.children[3];
	this.actionsObj = this.obj.children[4].children;
	this.movieId = this.checkBox.getAttribute("movieId");

	this.seenIcon = this.actionsObj[0];
	this.loadingIcon = this.actionsObj[1];

	this.removable = this.actionsObj.length == 3;

	if (this.removable) {
		this.removeIcon = this.actionsObj[2];
	}

	this.deleteMovieFromMyMovies = function(movieListId) {
		instance.setLoading(true);
		var ajax = new Ajax();
		ajax.asyncQuery("DeleteFromList", {
			"movieListId" : movieListId
		}, function(result) {
			var res = JSON.parse(result);
			if (res) {
				if (res["Status"] == "Ok") {
					var seen=instance.hasSeen();
					instance.remove();
					movieAlbumPanel_instance.allMovies.setCount(movieAlbumPanel_instance.allMovies.getCount() - 1);
					EventBus.dispatchEvent("MovieDeleted",{"MovieID":instance.movieId,"Seen":seen});
					return;
				}
			}
			instance.setLoading(false);
			movieAlbumPanel_instance.setOperationErrorDialog();
		});
	}
	
	this.remove=function() {
		instance.obj.remove();
	}

	this.deleteMovieFromAlbum = function(albumId, movieListId) {
		var album = movieAlbumPanel_instance.getAlbumById(albumId);
		if (album) {
			instance.setLoading(true);
			var ajax = new Ajax();
			ajax.asyncQuery("DeleteFromAlbum", {
				"movieListId" : movieListId,
				"albumId" : albumId
			}, function(result) {
				var res = JSON.parse(result);
				if (res) {
					if (res["Status"] == "Ok") {
						instance.remove();
						album.setCount(album.getCount() - 1);
						return;
					}
				}
				instance.setLoading(false);
				movieAlbumPanel_instance.setOperationErrorDialog();
			});
		} else {
			movieAlbumPanel_instance.setOperationErrorDialog();
		}
	}

	this.seenClicked = function() {
		instance.setLoading(true);
		var seen = instance.hasSeen() ? "IHaveNotSeen" : "IHaveSeen";
		var ajax = new Ajax();
		ajax.asyncQuery(seen, {
			"movieId" : instance.movieId
		}, function(result) {
			var res = JSON.parse(result);
			if (res) {
				if (res["Status"] == "Ok") {
					var date = res["Date"];
					instance.setSeen(!instance.hasSeen());
					instance.setLoading(false);
					instance.setSeenDate(date);
					return;
				}
			}
			instance.setLoading(false);
			movieAlbumPanel_instance.setOperationErrorDialog();
		});
	}

	this.movieID = this.checkBox.getAttribute("movieId");

	this.setLoading = function(loading) {
		if (loading) {
			instance.seenIcon.classList.add("hidden");
			if (instance.removable) {
				instance.removeIcon.classList.add("hidden");
			}
			instance.loadingIcon.classList.remove("hidden");
		} else {
			instance.seenIcon.classList.remove("hidden");
			if (instance.removable) {
				instance.removeIcon.classList.remove("hidden");
			}
			instance.loadingIcon.classList.add("hidden");
		}
	}

	this.getMovieID = function() {
		return instance.movieID;
	}
	this.isChecked = function() {
		return instance.checkBox.checked && !instance.obj.classList.contains("hidden");
	}
	this.setChecked = function(check) {
		instance.checkBox.checked = check;
	}
	this.getMovieName = function() {
		return Safe.descape(instance.movieNameObj.innerHTML);
	}
	this.hasSeen = function() {
		return instance.seenIcon.classList.contains("text-warning");
	}

	this.getSeenDate = function() {
		return Safe.descape(instance.seenDateObj.innerHTML);
	}

	this.setSeenDate = function(qdate) {
		instance.seenDateObj.innerHTML = Safe.escape(qdate);
	}

	this.setSeen = function(seen) {
		instance.seenIcon.classList.remove("glyphicon-eye-open");
		instance.seenIcon.classList.remove("glyphicon-eye-close");
		instance.seenIcon.classList.remove("text-warning");
		instance.seenIcon.classList.remove("text-success");
		instance.seenObj.classList.remove("glyphicon-ok");
		instance.seenObj.classList.remove("glyphicon-remove");
		instance.seenObj.classList.remove("text-success");
		instance.seenObj.classList.remove("text-danger");
		if (seen) {
			instance.seenIcon.classList.add("text-warning");
			instance.seenIcon.classList.add("glyphicon-eye-close");
			instance.seenIcon.title = "Mark as unseen";
			instance.seenObj.title = "Already seen";
			instance.seenObj.classList.add("text-success");
			instance.seenObj.classList.add("glyphicon-ok");
		} else {
			instance.seenIcon.classList.add("text-success");
			instance.seenIcon.classList.add("glyphicon-eye-open");
			instance.seenIcon.title = "Mark as seen";
			instance.seenObj.title = "Not seen yet";
			instance.seenObj.classList.add("text-danger");
			instance.seenObj.classList.add("glyphicon-remove");
		}
		EventBus.dispatchEvent("SeenChanged",{"MovieID":instance.movieId,"Seen":seen,"Origin":"Click"});
	}

}

document.addEventListener('DOMContentLoaded', function() {
	$("ul > li.tree-toggle").click(function(e) {
		e.stopPropagation();
	});
	$('li.tree-toggle').click(function(evt) {
		if (evt.toElement.localName == "label") {
			$(this).children('ul').toggle(200);
		}
	});
	movieAlbumPanel_instance = new movieAlbumPanel_M();
	movieAlbumPanel_instance.init();
	EventBus.addListener("MovieImported", movieAlbumPanel_instance.movieImportedCallback);
});
