<!DOCTYPE html>
<html lang="en">
<head>
<script generic="true" src="js/jquery.min.js"></script>
<script generic="true" src="js/bootstrap.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
<link generic="false" rel="stylesheet" href="mainUserPanel.css">
<link generic="trze" rel="stylesheet" href="css/theme.css">
<script generic="false" src="mainUserPanel.js"></script>
<script generic="true" src="js/ajax.js"></script>
<script generic="true" src="js/safe.js"></script>
<script generic="true" src="js/bootstrap-dialog.min.js"></script>
<script generic="true" src="js/sorttable.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap-dialog.min.css">
</head>
<body>
<?php
$userId = viewState::getViewUserID ();
$user = User::getByID ( $userId, true );
if (! $user) {
	die ( "There is no such user" );
}
$isMe = false;
$weAreFriends = false;
$meLogged = false;
if (Config::getConfig ()->getSession ()->isLogged ()) {
	$meLogged = true;
	$me = Config::getConfig ()->getSession ()->getUser ();
	$isMe = $me->getID () == $user->getID ();
	$weAreFriends = FriendShip::getFriendShip ( $me, $user ) !== false;
	
	if ($isMe) {
		if (isset ( $_POST ["mainUserPanel_saveBtn"] )) {
			$td = function ($index) {
				if (isset ( $_POST [$index] )) {
					return $_POST [$index];
				} else {
					return false;
				}
			};
			$visVal = function ($vis) {
				if (! $vis && $vis != 0) {
					return false;
				}
				return ($vis == 0 || $vis == 1 || $vis == 2);
			};
			$nameVal = function ($name) {
				if (! $name) {
					return false;
				}
				return strlen ( $name ) < 20;
			};
			
			if (isset ( $_FILES ["avatar"] )) {
				$fn = $_FILES ["avatar"];
				if ($fn ['error'] === UPLOAD_ERR_OK) {
					$info = getimagesize ( $fn ['tmp_name'] );
					if ($info !== FALSE) {
						$type = $info [2];
						$valid = false;
						$originalFile = $fn ['tmp_name'];
						if ($type == IMAGETYPE_GIF) {
							$img = imageCreateFromGIF ( $originalFile );
							$valid = true;
						} else if ($type == IMAGETYPE_JPEG) {
							$img = imageCreateFromJPEG ( $originalFile );
							$valid = true;
						} else if ($type == IMAGETYPE_PNG) {
							$img = imageCreateFromPNG ( $originalFile );
							$valid = true;
						}
						if ($valid) {
							$newWidth = 200;
							$newHeight = 200;
							$info = getimagesize ( $originalFile );
							list ( $width, $height ) = getimagesize ( $originalFile );
							$tmp = imagecreatetruecolor ( $newWidth, $newHeight );
							imagealphablending ( $tmp, false );
							imagesavealpha ( $tmp, true );
							imagecopyresampled ( $tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height );
							imagepng ( $tmp, $user->getAvatarLocation () );
						}
					}
				}
			}
			
			$fname = $td ( "fname" );
			if ($nameVal ( $fname )) {
				$user->setFirstName ( $fname );
			}
			$vfname = $td ( "vfname" );
			
			if ($visVal ( $vfname )) {
				$user->setFirstNameVisibility ( $vfname );
			}
			
			$lname = $td ( "lname" );
			if ($nameVal ( $lname )) {
				$user->setLastName ( $lname );
			}
			$vlname = $td ( "vlname" );
			if ($visVal ( $vfname )) {
				$user->setFirstNameVisibility ( $vfname );
			}
			if ($visVal ( $vlname )) {
				$user->setLastNameVisibility ( $vlname );
			}
			$email = $td ( "email" );
			if (QInputObject::isValidEmail ( $email )) {
				$user->setEmail ( $email );
			}
			$vemail = $td ( "vemail" );
			if ($visVal ( $vemail )) {
				$user->setEmailVisibility ( $vemail );
			}
			$bday = $td ( "bday" );
			if (QInputObject::isValidDate ( $bday )) {
				if (QInputObject::dateIsPast ( $bday )) {
					$bday = QInputObject::dateToStamp ( $bday );
					$user->setBday = $bday;
				}
			}
			$vbday = $td ( "vbday" );
			if ($visVal ( $vbday )) {
				$user->setBdayVisibility ( $vbday );
			}
			$user->save ();
		}
		if (isset ( $_POST ["mainUserPanel_action"] )) {
			$action = $_POST ["mainUserPanel_action"];
			if (isset ( $_POST ["friendId"] ) && isset ( $_POST ["fsid"] )) {
				$userId = $_POST ["friendId"];
				$friendShipID = $_POST ["fsid"];
				switch ($action) {
					default :
						die ( $action );
					case "removeFriend":
						$fs = FriendShip::getByID ( $friendShipID, true );
						if ($fs) {
							if ($fs->IDsMatch ( $userId, $me->getID () )) {
								$fs->delete ();
							}
						}
					break;
					case "addFriend":
						$myuser = User::getByID ( $userId, true );
						if ($myuser) {
							FriendShip::newFriendShip ( $me, $myuser, FriendShip::HAS_ACCEPTED, FriendShip::HAS_REQUEST );
						}
					break;
					case "cancelRequest":
						$fs = FriendShip::getByID ( $friendShipID, true );
						if ($fs) {
							if ($fs->IDsMatch ( $userId, $me->getID () )) {
								$fs->delete ();
							}
						}
					break;
					case "acceptFriend":
						$fs = FriendShip::getByID ( $friendShipID, true );
						if ($fs) {
							if ($fs->IDsMatch ( $userId, $me->getID () )) {
								$fs->setUserAccept ( $me, true );
							}
						}
					break;
					case "rejectRequest":
						$fs = FriendShip::getByID ( $friendShipID, true );
						if ($fs) {
							if ($fs->IDsMatch ( $userId, $me->getID () )) {
								$fs->setUserAccept ( $me, false );
							}
						}
					break;
					case "createAlbum":
						$albumName = $userId;
						Album::createAlbum ( $user, $albumName );
						viewState::forceUpdateAllMoviesForUser ( $user );
					break;
				}
			}
		}
	}
}

$safeUsername = Safe::escape ( $user->getUsername () );
$safeFirstName = Safe::escape ( $user->getFirstName () );
$safeLastName = Safe::escape ( $user->getLastName () );
$safeEmail = Safe::escape ( $user->getEmail () );
$safeBDay = Safe::escape ( $user->getBirthday () );

$friendShips = FriendShip::getQuickFriendShipsForUser ( $user );
$emailVis = $isMe ? true : $user->emailVisible ( $meLogged, $weAreFriends );
$fnameVis = $isMe ? true : $user->firstNameVisible ( $meLogged, $weAreFriends );
$lnameVis = $isMe ? true : $user->lastNameVisible ( $meLogged, $weAreFriends );
$bdayVis = $isMe ? true : $user->birthdayVisible ( $meLogged, $weAreFriends );

$safeRank = $user->getHighestRank ()->getSafeHTML ();
$safeRankName = Safe::escape ( $user->getHighestRank ()->getName () );
$safeAvatar = User::getAvatarURLForUsername ( $user->getUsername () );

$arVis = function ($visibility) {
	$ar = array (
			"",
			"",
			"",
			"" 
	);
	$ar [$visibility] = "selected ";
	return $ar;
};

$arFnameVis = $arVis ( $user->getVisibility ( User::OFFSET_FNAME ) );
$arLnameVis = $arVis ( $user->getVisibility ( User::OFFSET_LNAME ) );
$arEmailVis = $arVis ( $user->getVisibility ( User::OFFSET_EMAIL ) );
$arBDayVis = $arVis ( $user->getVisibility ( User::OFFSET_BDAY ) );

if ($isMe) {
	echo '<form enctype="multipart/form-data" method=POST action="" name=mainUserPanel_Main>';
}

?>

	<div class="flex-container-column well mainUserPanel_mainPanel">
		<div>
			<h1><?php echo $safeUsername;?></h1>
			<h4><?php echo $safeRankName;?></h4>
		</div>
		<div class="flex-container-row">
			<div>
		<?php
		echo '<img class="border-3" src=' . $safeAvatar . ' width=200 height=200 alt="Missing avatar" />';
		?>
		</div>
			<div class="qtable mainUserPanel_setTable">
				<div class="qtable-row">
					<div class="qtable-cell-header text-right">Username:</div>
					<div class="qtable-cell padding-left-10">
						<h5><?php echo $safeUsername?></h5>
					</div>
					<div class="qtable-cell"></div>
				</div>
				<?php
				
				if ($fnameVis) {
					echo '
					<div class="qtable-row">
					<div class="qtable-cell-header text-right">First name:</div>
					<div class="qtable-cell padding-left-10">';
					if ($isMe) {
						echo '<input name="fname" class="form-control" type="text" value="' . $safeFirstName . '" >
							</div>
							<div class="qtable-cell padding-left-10">
								<select name="vfname" class="form-control">
									<option ' . $arFnameVis [2] . 'value=2>Private</option>
									<option ' . $arFnameVis [1] . 'value=1>Only friends</option>
									<option ' . $arFnameVis [0] . 'value=0>Public</option>
								</select>
							</div>
								';
					} else {
						echo '<h5>' . $safeFirstName . '</h5></div>';
					}
					echo '</div>';
				}
				
				if ($lnameVis) {
					echo '
					<div class="qtable-row">
					<div class="qtable-cell-header text-right">Last name:</div>
					<div class="qtable-cell padding-left-10">';
					if ($isMe) {
						echo '<input name="lname" class="form-control" type="text" value="' . $safeLastName . '" >
							</div>
							<div class="qtable-cell padding-left-10">
								<select name="vlname" class="form-control">
									<option ' . $arLnameVis [2] . 'value=2>Private</option>
									<option ' . $arLnameVis [1] . 'value=1>Only friends</option>
									<option ' . $arLnameVis [0] . 'value=0>Public</option>
								</select>
							</div>
								';
					} else {
						echo '<h5>' . $safeLastName . '</h5></div>';
					}
					echo '</div>';
				}
				
				if ($emailVis) {
					echo '
					<div class="qtable-row">
					<div class="qtable-cell-header text-right">Email:</div>
					<div class="qtable-cell padding-left-10">';
					if ($isMe) {
						echo '<input name="email" class="form-control" type="text" value="' . $safeEmail . '" >
							</div>
							<div class="qtable-cell padding-left-10">
								<select name="vemail" class="form-control">
									<option ' . $arEmailVis [2] . 'value=2>Private</option>
									<option ' . $arEmailVis [1] . 'value=1>Only friends</option>
									<option ' . $arEmailVis [0] . 'value=0>Public</option>
								</select>
							</div>
								';
					} else {
						echo '<h5>' . $safeEmail . '</h5></div>';
					}
					echo '</div>';
				}
				
				if ($bdayVis) {
					echo '
					<div class="qtable-row">
					<div class="qtable-cell-header text-right">Birthday:</div>
					<div class="qtable-cell padding-left-10">';
					if ($isMe) {
						echo '<input name="bday" class="form-control" type="date" value="' . Config::timestampToInputDate ( $safeBDay ) . '" >
							</div>
							<div class="qtable-cell padding-left-10">
								<select name="vbday" class="form-control">
									<option ' . $arBDayVis [2] . 'value=2>Private</option>
									<option ' . $arBDayVis [1] . 'value=1>Only friends</option>
									<option ' . $arBDayVis [0] . 'value=0>Public</option>
								</select>
							</div>
								';
					} else {
						echo '<h5>' . Config::timestampToTextDay ( $safeBDay ) . '</h5></div>';
					}
					echo '</div>';
				}
				if ($isMe) {
					echo '
					<div class="qtable-row">
							<div class="qtable-cell-header text-right">Avatar:</div>
							<div class="qtable-cell padding-left-10">
							<input name="avatar" class="form-control" type="file" accept="image/*">
							</div>
							<div class="qtable-cell"></div>
					</div>';
				}
				?>
			</div>
			<div class="flex-item"></div>
			<div class="mainUserPanel_rPanel rpanel">
				<h3>Friends</h3>
				<div class="mainUserPanel_friendsPanel well well-sm">
					
			<?php
			$friendEchoF = function ($friendShip) {
				$meAccepted = $friendShip->meAccepted ();
				$heAccepted = $friendShip->friendHasAccepted ();
				$friends = $meAccepted && $heAccepted;
				if (! $friends) {
					return;
				}
				echo '<div class="mainUserPanel_friendPanel flex-container-row fullwidth well well-sm border">' . Safe::escape ( $friendShip->getFriendUsername () ) . '</div>';
			};
			
			$friendEchoM = function ($friendShip) {
				$friendID = $friendShip->getFriendID ();
				$friendShipID = $friendShip->getFriendShipID ();
				$meAccepted = $friendShip->meAccepted ();
				$heAccepted = $friendShip->friendHasAccepted ();
				
				$meRejected = $friendShip->meRejected ();
				$heRejected = $friendShip->friendHasRejected ();
				
				$mePending = $friendShip->mePending ();
				$hePending = $friendShip->friendPending ();
				$friends = $meAccepted && $heAccepted;
				$notFriends = $meRejected && $heRejected;
				$pending = $mePending || $hePending;
				
				if ($meRejected) { // TODO: Do not display rejected requests
					return;
				}
				
				$pendingStr = $pending ? "Pending" : ($heRejected ? "Request rejected" : "");
				$showAccept = $mePending ? "" : "hidden ";
				$showEnvelope = $friends ? "" : "hidden ";
				$removeTitle = "";
				$removeAction = "null";
				if ($friends) {
					$removeTitle = "Remove friend";
					$removeAction = "removeFriend";
				} else {
					if ($hePending) {
						$removeTitle = "Cancel friendship request";
						$removeAction = "cancelRequest";
					} else {
						if ($mePending) {
							$removeTitle = "Reject request";
							$removeAction = "rejectRequest";
						} else {
							if ($heRejected) {
								$removeTitle = "Cancel friendship request";
								$removeAction = "cancelRequest";
							}
						}
					}
				}
				echo '<div class="mainUserPanel_friendPanel flex-container-row fullwidth well well-sm border">
						<div style="padding: 5px">' . Safe::escape ( $friendShip->getFriendUsername () ) . '</div>
						<span class="hidden glyphicon glyphicon-comment bigGlyph pointer" style="padding: 5px" title="Unread messages"></span>
						<span onClick="mainUserPanel_instance.sendMessage(' . $friendID . ',' . $friendShipID . ')" class="' . $showEnvelope . 'glyphicon glyphicon-envelope bigGlyph pointer" style="padding: 5px" title="Send a message"></span>
						<div class="flex-item text-center" style="padding:5px">' . $pendingStr . '</div>
						<div>
							<span onClick="mainUserPanel_instance.acceptRequest(' . $friendID . ',' . $friendShipID . ')" class="' . $showAccept . 'pointer" title="Accept friendship">
								<span class="glyphicon glyphicon-user bigGlyph" style="padding: 5px"></span>
								<span class="glyphicon glyphicon-ok text-success" style="position: relative; left: -20px"></span>
							</span>
							<span onClick="mainUserPanel_instance.' . $removeAction . '(' . $friendID . ',' . $friendShipID . ')" class="pointer" title="' . $removeTitle . '">
								<span class="glyphicon glyphicon-user bigGlyph" style="padding: 5px"></span>
								<span class="glyphicon glyphicon-remove text-danger" style="padding: 5px; position: relative; left: -20px"></span>
							</span>
						</div>
					</div>';
			};
			$callback = $isMe ? $friendEchoM : $friendEchoF;
			foreach ( $friendShips as $friendShip ) {
				$callback ( $friendShip );
			}
			if ($isMe) {
				echo '
					<div class="flex-item"></div>
					<div class="flex-container">
						<div class="flex-item has-feedback" id="mainUserPanel_newFriend">
							<input type="text" class="form-control" placeholder="Friend name">
							<span class="glyphicon glyphicon-ok text-success form-control-feedback hidden"></span>
							<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate text-info form-control-feedback hidden"></span>
							<span class="glyphicon glyphicon-remove text-danger form-control-feedback hidden"></span>
						</div>
						<div class="btn btn-success margin-left-20" style="height: 32px">
							<span class="glyphicon glyphicon-plus"></span>
							&
					nbsp;Addd Friend
						</div>
					</div>';
			}
			?>
				</div>
			</div>
		</div>
		<div class="fullwidth border-top margin-top-30"></div>
		<div class="well margin-top-30" style="padding-top: 0px">
			<h3>Albums</h3>
			<div style="max-height: 400px; overflow: auto; max-width: 50%;">
				<table class="table sortable tab-contents">
					<thead>
						<tr>
							<th>Album name</th>
							<th class="text-center">Movies in album</th>
							<?php
							if ($isMe) {
								echo '
								<th class="text-center">Access management&nbsp; 
		<span class="glyphicon glyphicon-info-sign pointer" data-toggle="popover" data-trigger="hover" data-content="Changing this only changes who can view this album. Nobody but you will be able to modify (change name, add/remove movies) it in any way."></span>
		</th>
								<th class="sorttable_nosort text-right">Actions</th>';
							}
							?>
						</tr>
					</thead>
					<tbody id="mainUserPanel_albumContents">
						<?php
						$albums = viewState::getAllMoviesForUser ( $user );
						$userId = $user->getID ();
						$albumCallback_M = function ($album) use($meLogged, $weAreFriends, $userId) {
							$albumId = $album->getAlbumID ();
							if ($album->getOwnerID () != $userId) {
								return;
							}
							$priv = $album->isPrivate () ? "text-success" : "text-muted text-info-hover";
							$friends = $album->isSharedBetweenFriends () ? "text-success" : "text-muted text-info-hover";
							$public = $album->isPublic () ? "text-success" : "text-muted text-info-hover";
							echo '
							<tr>
								<td>' . Safe::escape ( $album->getAlbumName () ) . '	
								</td>
							<td class="text-center">
							' . $album->getAlbumContentCount () . '
							</td>
							<td class="text-center">
							<span onClick="mainUserPanel_instance.setAlbumPrivate(' . $albumId . ',this);" class="glyphicon glyphicon-lock bigGlyph ' . $priv . ' pointer" title="Private album"></span>&nbsp;
							<span onClick="mainUserPanel_instance.setAlbumFriends(' . $albumId . ',this)";  class="glyphicon glyphicon-user bigGlyph ' . $friends . ' pointer" title="Only me and friends can access"></span>&nbsp;
							<span onClick="mainUserPanel_instance.setAlbumPublic(' . $albumId . ',this)";  class="glyphicon glyphicon-globe bigGlyph ' . $public . ' pointer" title="Everyone can access"></span>&nbsp;
							</td>
							<td class="text-right">
							<div>
								<span class="glyphicon glyphicon-edit bigGlyph pointer text-info-hover" title="Edit album" onClick="mainUserPanel_instance.editAlbumName(this,' . $albumId . ');"></span>&nbsp;
								<span class="glyphicon glyphicon-trash bigGlyph pointer text-danger-hover" onClick="mainUserPanel_instance.deleteAlbum(this,' . $albumId . ');" title="Delete album"></span>
							</div>
							</td>
							</tr>
							';
						};
						$albumCallback_F = function ($album) use($meLogged, $weAreFriends, $userId) {
							$albumId = $album->getAlbumID ();
							if ($album->getOwnerID () != $userId) {
								return;
							}
							if ($album->canSee ( $meLogged, $weAreFriends )) {
								echo '<tr>
								<td>' . Safe::escape ( $album->getAlbumName () ) . '
								</td>
								<td class="text-center">
								' . $album->getAlbumContentCount () . '
								</td>
								</tr>';
							}
						};
						$callback = $isMe ? $albumCallback_M : $albumCallback_F;
						foreach ( $albums as $album ) {
							$callback ( $album );
						}
						
						?>
					</tbody>
				</table>
			</div>
			<div class="btn btn-success" onClick="mainUserPanel_instance.newAlbum();">
				<span class="glyphicon glyphicon-plus"></span>
				&nbsp; New album
			</div>
		</div><?php
		if ($isMe) {
			echo '
		<div class="text-center fullwidth well well-sm">
			<input class="btn btn-default" type="reset" value="Discard changes" /> <input class="btn btn-primary" type="submit" name="mainUserPanel_saveBtn" value="Save profile" />
		</div>';
		}
		?>
	</div>
	<?php
	if ($isMe) {
		echo '<form method=POST action="" name=mainUserPanel_Main>';
	}
	?>
	
</body>
</html>
