var mainUserPanel_M = function() {
	var instance = this;
	this.newUser = new mainUserPanel_input_M(document.getElementById('mainUserPanel_newFriend'));

	this.setAlbumPrivate = function(id, obj) {
		instance.updateAlbumVis(id, "setAlbumPrivate", obj.parentElement, 0);
	}
	this.setAlbumFriends = function(id, obj) {
		instance.updateAlbumVis(id, "setAlbumFriends", obj.parentElement, 1);
	}
	this.setAlbumPublic = function(id, obj) {
		instance.updateAlbumVis(id, "setAlbumPublic", obj.parentElement, 2);
	}

	this.updateAlbumVis = function(albumId, callback, iconParents, icon) {
		var icon1 = iconParents.children[0];
		var icon2 = iconParents.children[1];
		var icon3 = iconParents.children[2];
		var icons = [ icon1, icon2, icon3 ];
		var removeStyle = function(icon) {
			icon.classList.remove("text-muted");
			icon.classList.remove("text-success");
			icon.classList.remove("text-info-hover");
		}
		var addStyle = function(icon, muted) {
			icon.classList.add(muted ? "text-muted" : "text-success");
			if (muted) {
				icon.classList.add("text-info-hover");
			}
		}
		removeStyle(icon1);
		removeStyle(icon2);
		removeStyle(icon3);
		addStyle(icon1, true);
		addStyle(icon2, true)
		addStyle(icon3, true);
		var ajax = new Ajax();
		ajax.asyncQuery(callback, {
			"albumId" : albumId
		}, function(result) {
			var res = JSON.parse(result);
			if (res) {
				if (res["Status"] == "Ok") {
					removeStyle(icons[icon]);
					addStyle(icons[icon], false);
					return;
				}
			}
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Operation failed',
				message : 'Your last operation was not saved. Please refresh the page.',
			});
		});
	}

	this.processingDialog=false;
	
	this.showProcessingDialog = function(title) {
		if (!instance.processingDialog) {
			instance.processingDialog = BootstrapDialog.show({type:BootstrapDialog.TYPE_SUCCESS,title:"Processing",message:"Processing"});
			var qbody = instance.processingDialog.getModalBody()[0];
			var rc = qbody.children[0];
			rc.innerHTML = '<p>'+Safe.escape(title)+'</p><div class="progress progress-striped active"><div class="progress-bar progress-bar-striped progress-bar-info active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div></div>';
			instance.processingDialog.setClosable(false);
		}
	}


	this.hideProcessingDialog = function(timeout=false) {
		if(timeout) {
			window.setTimeout(function() {instance.hideProcessingDialog(false);},timeout);
		} else {
			if (instance.processingDialog) {
				instance.processingDialog.close();
				instance.processingDialog=false;
			}
		}
	}
	
	this.createAlbum=function(dialog,albumName) {
		if(albumName.trim().length == 0) {
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Invalid album name',
				message : 'Album name must not be empty!.',
			});
			return
		}
		instance.sendData("createAlbum",{"friendId":albumName,"fsid":-1});
	}
	
	this.newAlbum=function() {
		var input = document.createElement("input");
		input.type = "text";
		input.classList.add("form-control");
		input.placeholder = "Album name";
		BootstrapDialog.show({
			type : BootstrapDialog.TYPE_SUCCESS,
			title : 'New album',
			message : input,
			buttons : [ {
				label : 'Cancel',
				cssClass : 'btn-default',
				action : function(dialog) {
					dialog.close();
				}
			}, {
				label : 'Create',
				cssClass : 'btn-success',
				hotkey : 13,
				action : function(dialog) {
					instance.createAlbum(dialog, input.value);
				}
			} ],
			 onshown: function(dialogRef){
	               input.focus();
            }
		});
	}
	
	this.saveAlbum = function(dialog, albumName,albumId,saveToObj) {
		if(albumName.trim().length == 0) {
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Invalid album name',
				message : 'Album name must not be empty!.',
			});
			return
		}
		dialog.close();
		instance.showProcessingDialog("Saving")
		var ajax=new Ajax();
		ajax.asyncQuery("changeAlbumName",{"albumId":albumId,"albumName":albumName},function(result) {
			var res=JSON.parse(result);
			if(res) {
				instance.hideProcessingDialog(1000);
				saveToObj.innerHTML=Safe.escape(albumName);
				return;
			}
			instance.hideProcessingDialog();
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Operation failed',
				message : 'Your last operation was not saved. Please refresh the page.',
			});
		});
	}
	

	this.deleteAlbum=function(obj, albumId) {
		instance.confirmDanger("Are you sure you want to delete this album? This action will not delete any movie.",function() {
			var ajax=new Ajax();
			instance.showProcessingDialog("Deleting")
			ajax.asyncQuery("deleteAlbum",{"albumId":albumId},function(result) {
				var res=JSON.parse(result);
				if(res) {
					if(res["Status"]=="Ok") {
						instance.hideProcessingDialog(1000);
						obj.parentElement.parentElement.parentElement.remove();
						return;
					}
				}
				BootstrapDialog.show({
					type : BootstrapDialog.TYPE_DANGER,
					title : 'Operation failed',
					message : 'Your last operation was not saved. Please refresh the page.',
				});
			});
		})
	}
	
	this.editAlbumName = function(obj,albumId) {
		var input = document.createElement("input");
		input.type = "text";
		input.classList.add("form-control");
		input.placeholder = "Album name";
		var titleObj=obj.parentElement.parentElement.parentElement.children[0];
		input.value = Safe.descape(titleObj.innerHTML.trim());
		BootstrapDialog.show({
			type : BootstrapDialog.TYPE_INFO,
			title : 'Edit album',
			message : input,
			buttons : [ {
				label : 'Cancel',
				cssClass : 'btn-default',
				action : function(dialog) {
					dialog.close();
				}
			}, {
				label : 'Save',
				cssClass : 'btn-primary',
				hotkey : 13,
				action : function(dialog) {
					instance.saveAlbum(dialog, input.value,albumId,titleObj);
				}
			} ],
			 onshown: function(dialogRef){
	               input.focus();
            }
		});
	}

	this.newUser.setButtonCallback(function() {
		var userName = instance.newUser.getValue();
		if (userName.length > 0) {
			var ajax = new Ajax();
			instance.newUser.lockButton(true);
			instance.newUser.setProcessing();
			instance.newUser.lockInput(true);
			ajax.asyncQuery("canAddFriend", {
				"name" : userName
			}, function(result) {
				var res = JSON.parse(result);
				if (res["Status"] == "Ok") {
					userId = res["UserID"];
					instance.newUser.setSuccess();
					instance.sendData("addFriend", {
						"friendId" : userId,
						"fsid" : -1
					});
				} else {
					instance.newUser.lockButton(false);
					instance.newUser.setProcessing();
					instance.newUser.setError();
					instance.newUser.lockInput(false);
					instance.newUser.lockButton(false);
				}
			});

		}
	});

	this.confirmWarn = function(question, callback) {
		instance.confirm(question, callback, "Warning", BootstrapDialog.TYPE_WARNING, "btn-warning");
	}

	this.confirmDanger = function(question, callback) {
		instance.confirm(question, callback, "Warning", BootstrapDialog.TYPE_DANGER, "btn-danger");
	}

	this.confirm = function(question, callback, title, type, btn) {
		var cb = callback;
		BootstrapDialog.show({
			title : title,
			message : question,
			type : type,
			buttons : [ {
				label : 'Cancel',
				action : function(dialog) {
					dialog.close();
				}
			}, {
				label : 'Confirm',
				action : function(dialog) {
					cb();
					dialog.close();
				},
				cssClass : btn
			} ]
		});
	}

	this.sendData = function(action, data) {
		var form = document.createElement("form");
		form.action = "";
		form.method = "POST";
		var getValue = function(key, value) {
			var elem = document.createElement("input");
			elem.type = "hidden";
			elem.name = key;
			elem.value = value;
			return elem;
		}
		form.appendChild(getValue("mainUserPanel_action", action));
		for ( var a in data) {
			var val = data[a];
			form.appendChild(getValue(a, val));
		}
		form.submit();
	}

	this.sendMessage = function(targetId, fsid) {
	}

	this.acceptRequest = function(targetId, fsid) {
		instance.sendData("acceptFriend", {
			"friendId" : targetId,
			"fsid" : fsid
		});
	}
	this.removeFriend = function(targetId, fsid) {
		var userId = targetId;
		if (instance.confirmDanger("Are you sure you want to remove this friend?\nThis operation cannot be undone!", function() {
			instance.sendData("removeFriend", {
				"friendId" : targetId,
				"fsid" : fsid
			});
		}))
			;
	}
	this.cancelRequest = function(targetId, fsid) {
		var userId = targetId;
		if (instance.confirmWarn("Are you sure you want to cancel the request?\nThis operation cannot be undone!", function() {
			instance.sendData("cancelRequest", {
				"friendId" : targetId,
				"fsid" : fsid
			});
		}))
			;
	}
	this.rejectRequest = function(targetId, fsid) {
		var userId = targetId;
		if (instance.confirmWarn("Are you sure you want to reject the request?\nThis operation cannot be undone!", function() {
			instance.sendData("rejectRequest", {
				"friendId" : targetId,
				"fsid" : fsid
			});
		}))
			;
	}
}

var mainUserPanel_instance;

var mainUserPanel_input_M = function(rootElem) {
	var instance = this;

	this.root = rootElem;
	this.input = this.root.children[0];
	this.okIcon = this.root.children[1];
	this.processIcon = this.root.children[2];
	this.errorIcon = this.root.children[3];
	this.btn = this.root.parentElement.children[1];

	this.callbacks = [];
	this.locked = false;

	this.input.addEventListener("keydown", function(key) {
		if (key.keyCode == "13") {
			for ( var callback in instance.callbacks) {
				instance.callbacks[callback]();
			}
		}
	});

	this.setButtonCallback = function(callback) {
		instance.callbacks.push(callback);
		instance.btn.addEventListener("click", callback);
	}

	this.lockButton = function(lock) {
		if (lock) {
			if (!instance.locked) {
				for (callback in instance.callbacks) {
					instance.btn.removeEventListener("click", callback);
				}
				instance.btn.classList.add("disabled");
				instance.locked = true;
			}
		} else {
			if (instance.locked) {
				for (callback in instance.callbacks) {
					instance.btn.addEventListener("click", callback);
				}
				instance.btn.classList.remove("disabled");
				instance.locked = false;
			}
		}
	}

	this.hideIcon = function(icon) {
		if (!icon.classList.contains("hidden")) {
			icon.classList.add("hidden");
		}
	}
	this.showIcon = function(icon) {
		if (icon.classList.contains("hidden")) {
			icon.classList.remove("hidden");
		}
	}

	this.setRootStyle = function(style) {
		instance.root.classList.remove("has-success");
		instance.root.classList.remove("has-error");
		instance.root.classList.remove("has-info");
		instance.root.classList.add("has-" + style);
	}

	this.setError = function() {
		instance.hideIcon(instance.okIcon);
		instance.hideIcon(instance.processIcon);
		instance.hideIcon(instance.errorIcon);
		instance.showIcon(instance.errorIcon);
		instance.setRootStyle("error");
	}

	this.setProcessing = function() {
		instance.hideIcon(instance.okIcon);
		instance.hideIcon(instance.processIcon);
		instance.hideIcon(instance.errorIcon);
		instance.showIcon(instance.processIcon);
		instance.setRootStyle("info");
	}
	this.setSuccess = function() {
		instance.hideIcon(instance.okIcon);
		instance.hideIcon(instance.processIcon);
		instance.hideIcon(instance.errorIcon);
		instance.showIcon(instance.okIcon);
		instance.setRootStyle("success");
	}
	this.lockInput = function(lock) {
		instance.input.readOnly = lock;
	}

	this.getValue = function() {
		return instance.input.value;
	}

}

document.addEventListener('DOMContentLoaded', function() {
	mainUserPanel_instance = new mainUserPanel_M();
	$('[data-toggle="popover"]').popover({
		trigger : 'hover',
		'placement' : 'top'
	});
});
