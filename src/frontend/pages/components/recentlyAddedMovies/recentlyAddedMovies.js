var recentlyAddedMovies_M = function() {
	var instance = this;

	this.movieIds = {};

	this.panelWrapper = document.getElementById('recentlyAddedMovies_mainPanelWrapper');

	this.detectMovieIds = function() {
		instance.movieIds = {};
		var objs = instance.panelWrapper.getElementsByClassName('recentlyAddedMovies_InfoPanel');
		for (var i = 0; i < objs.length; i++) {
			var obj = objs[i];
			var movieId = obj.getAttribute("movieId");
			instance.movieIds[movieId] = obj.parentElement;
		}
	}
	this.detectMovieIds();

	this.importMovie = function(obj, movieId) {
		var sel = obj.parentElement.children[0];
		var loadObj = obj.parentElement.children[2];
		var id = sel.options[sel.selectedIndex].value;
		var action = "copyMovies";
		if (id == -1) {
			action = "importMovies";
		}
		var ajax = new Ajax();
		obj.classList.add("hidden");
		loadObj.classList.remove("hidden");
		ajax.asyncQuery(action, {
			"data" : movieId,
			"albumId" : id
		}, function(result) {
			obj.classList.remove("hidden");
			loadObj.classList.add("hidden");
			var res = JSON.parse(result);
			if (res) {
				if (res["Status"] == "Ok") {
					var created = res["Created"];
					var added = res["Added"];
					EventBus.dispatchEvent("MovieImported", {
						"ToAllMovies" : (id == -1),
						"AlbumID" : id,
						"MovieID" : movieId,
						"Created" : created,
						"Added" : added,
						"Origin" : "Click",
						"Seen" : false
					});
					return;
				}
			}
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Operation failed',
				message : 'Your last operation was not saved. Please refresh the page.',
			});
		});
	}

	this.table = document.getElementById('recentlyAddedMovies_mainPanelWrapper');

	this.getSelects = function() {
		return [].slice.call(instance.table.getElementsByTagName("select"));
	}

	this.addMovieToSelect = function(select, albumName, albumId) {
		var grp = select.children[1];
		var opt = document.createElement("option");
		opt.innerHTML = Safe.escape(albumName);
		opt.value = albumId;
		grp.appendChild(opt);
	}

	this.removeMovieFromSelect = function(select, albumId) {
		var grp = select.children[1];
		var chld = [].slice.call(grp.children);
		chld.forEach(function(opt) {
			if (opt.value == albumId) {
				opt.remove();
			}
		});
	}

	this.changeMovieNameInSelect = function(select, albumName, albumId) {
		var grp = select.children[1];
		var chld = [].slice.call(grp.children);
		chld.forEach(function(opt) {
			if (opt.value == albumId) {
				opt.innerHTML = Safe.escape(albumName);
			}
		});
	}

	this.albumAdded = function(albumData) {
		(instance.getSelects()).forEach(function(sel) {
			instance.addMovieToSelect(sel, albumData["AlbumName"], albumData["AlbumID"]);
		});
	}

	this.albumDeleted = function(albumData) {
		(instance.getSelects()).forEach(function(sel) {
			instance.removeMovieFromSelect(sel, albumData["AlbumID"]);
		});
	}

	this.albumRenamed = function(albumData) {
		(instance.getSelects()).forEach(function(sel) {
			instance.changeMovieNameInSelect(sel, albumData["AlbumName"], albumData["AlbumID"]);
		});
	}

	this.movieDeleted = function(movieData) {
		var movieId = movieData["MovieID"];
		if (movieId in instance.movieIds) {
			var movieObj = instance.movieIds[movieId];
			var seenObj = movieObj.children[1].children[1].children[0];
			var addObj = movieObj.children[2];
			if (addObj.classList.contains("hidden")) {
				addObj.classList.remove("hidden");
			}
			if (seenObj.classList.contains("text-success")) {
				seenObj.classList.remove("text-success");
				seenObj.classList.add("text-danger");
				seenObj.classList.add("glyphicon-remove");
				seenObj.classList.remove("glyphicon-ok");
			}
		}
	}

	this.seenChanged = function(movieData) {
		var movieId = movieData["MovieID"];
		var seen = movieData["Seen"];
		if (movieId in instance.movieIds) {
			var movieObj = instance.movieIds[movieId];
			var seenObj = movieObj.children[1].children[1].children[0];
			var addObj = movieObj.children[2];
			if (seen) {
				if (!addObj.classList.contains("hidden")) {
					addObj.classList.add("hidden");
				}
				if (!seenObj.classList.contains("text-success")) {
					seenObj.classList.add("text-success");
					seenObj.classList.remove("text-danger");
					seenObj.classList.remove("glyphicon-remove");
					seenObj.classList.add("glyphicon-ok");
				}
			} else {
				if (addObj.classList.contains("hidden")) {
					addObj.classList.remove("hidden");
				}
				if (seenObj.classList.contains("text-success")) {
					seenObj.classList.remove("text-success");
					seenObj.classList.add("text-danger");
					seenObj.classList.add("glyphicon-remove");
					seenObj.classList.remove("glyphicon-ok");
				}
			}
		}
	}
}

var recentlyAddedMovies_instance;

document.addEventListener('DOMContentLoaded', function() {
	recentlyAddedMovies_instance = new recentlyAddedMovies_M();

	EventBus.addListener("AlbumAdded", recentlyAddedMovies_instance.albumAdded);
	EventBus.addListener("AlbumDeleted", recentlyAddedMovies_instance.albumDeleted);
	EventBus.addListener("AlbumRenamed", recentlyAddedMovies_instance.albumRenamed);

	EventBus.addListener("SeenChanged", recentlyAddedMovies_instance.seenChanged);
	EventBus.addListener("MovieDeleted", recentlyAddedMovies_instance.movieDeleted);

});