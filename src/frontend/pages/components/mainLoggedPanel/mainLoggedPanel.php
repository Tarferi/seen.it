<!DOCTYPE html>
<html lang="en">
<head>
    <?php preProcessor::get()->includeHead("recentlyAddedMovies")?>
    <?php preProcessor::get()->includeHead("movieAlbumsPanel")?>
	<script generic="true" src="js/jquery.min.js"></script>
<script generic="true" src="js/bootstrap.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
<link generic="false" rel="stylesheet" href="mainLoggedPanel.css">
</head>
<body>
	<div class="flex-container-row fillwidth">
		<div class="mainLoggedPanel_leftPanel flex-item">
			<div class="container-fluid">
				<?php preProcessor::get()->includeBody("movieAlbumsPanel"); ?>
			</div>
		</div>
		<div class="mainLoggedPanel_rightPanel">
			<div class="container-fluid"><?php preProcessor::get()->includeBody("recentlyAddedMovies"); ?></div>
		</div>
	</div>
</body>
</html>
