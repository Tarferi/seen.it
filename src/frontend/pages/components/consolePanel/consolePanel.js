function consolePanel_select_console(obj) {
	var cObj = document.getElementById('consolePanel_select_console_input');
	if (document.activeElement != cObj) {
		cObj.focus();
	}
}
function consolePanel_keyPress(e) {
	var cObj = document.getElementById('consolePanel_select_console_input');
	if (document.activeElement != cObj) {
		cObj.focus();
	} else {
		if (e.keyCode == 13) {
			var command = cObj.value;
			if(command.length > 0) {
				cObj.value = "";
				console.log("Executing command \"" + command + "\"");
				var cpa=command.split(" ");
				Console.executeCommand(cpa[0],cpa,command);
			}
		}
	}
}

var Console = function() {
	var instance = this;
	this.commands = {};
	this.appender=false;
	this.definedCommands=["help"];
	
	this.setAppender=function(obj) {
		instance.appender=obj;
	}
	
	this.log=function(str) {
		if(instance.appender) {
			instance.appender(str+"\n");
		} else {
			console.error("Appender missing!");
			console.log(str);
		}
	}
	
	this.addCommand = function(command, callback=false) {
		if(!callback) {
			callback=instance[command];
		}
		this.commands[command] = callback;
	}
	this.executeCommand=function(command,fullArray,fullString) {
		instance.log(fullString);
		if(instance.commands[command]) {
			instance.commands[command](fullArray);
		} else {
			instance.log("Unknown command: \""+command+"\"");
		}
	}
	
	this.help=function(arr) {
		instance.log("Executed HELP command");
	}
	
	for(var i=0,o=this.definedCommands.length;i<o;i++) {
		this.addCommand(this.definedCommands[i]);
	}
}

document.addEventListener('DOMContentLoaded', function (){consolePanel_initConsole(document.getElementById('consolePanel_console'));});

function consolePanel_initConsole(obj) {
	var cObj = document.getElementById('consolePanel_select_console_input');
	var dObj = document.getElementById('consolePanel_wrapper');
	var appender=function(appendString) {
		obj.innerHTML+=appendString.replace("\n","<br />");
		dObj.scrollTop = dObj.scrollHeight;
		cObj.focus();
	}
	Console.setAppender(appender);
}

var Console=new Console();