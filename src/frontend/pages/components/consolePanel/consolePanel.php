<!DOCTYPE html>
<html lang="en">
<head>
<script generic="true" src="js/jquery.min.js"></script>
<script generic="true" src="js/bootstrap.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
<link generic="false" rel="stylesheet" href="consolePanel.css">
<script generic="false" src="consolePanel.js"></script>
</head>
<body>
	<h1>Console</h1>
	<div class="container-fluid consolePanel_fill">
		<span id="consolePanel_wrapper" class="consolePanel_frame" onClick="consolePanel_select_console(this);">
			<span id="consolePanel_console"></span>
			<input id="consolePanel_select_console_input" type=text class="consolePanel_input" onkeypress="consolePanel_keyPress(event);">
		</span>
	</div>
</body>
</html>
