<!DOCTYPE html>
<html lang="en">
<head>
<script generic="true" src="js/jquery.min.js"></script>
<script generic="true" src="js/bootstrap.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
<link generic="true" rel="stylesheet" href="css/theme.css">
<link generic="false" rel="stylesheet" href="registerPanel.css">
<script generic="false" src="registerPanel.js"></script>
<script generic="true" src="js/ajax.js"></script>
</head>
<body>
<?php
if (isset ( $_POST ["registerPanel_form"] )) {
	die ( "A" );
}

function addPrivacy($row, $defaultSelect = 0) {
	$select = $row->addSelect ( "privacy" );
	$select->addOption ( "Only me", "Only_Me", $defaultSelect == 0 );
	$select->addOption ( "Only my friends", "Only_My_Friends", $defaultSelect == 1 );
	$select->addOption ( "Public", "Public", $defaultSelect == 2 );
	return $select;
}

$form = new QForm ( "registerPanel_form" );

/**
 * Panel 1
 */
$panel1 = $form->addFormPanel ( "Your account to be" );
$usernameRow = $panel1->addRow ( "Username" );
$username = $usernameRow->addInput ( "username", "text", "Enter Username", "registerPanel_checkUsername" );
$username->addValidationFunctionParameters ( $usernameRow );
$username->addValidationFunctionParameters ( $username );

$passwordRow = $panel1->addRow ( "Password" );
$password = $passwordRow->addInput ( "password", "password", "Enter Password", "registerPanel_checkPassword" );

$passwordRow2 = $panel1->addRow ( "Password once again" );
$password2 = $passwordRow2->addInput ( "password", "password", "Enter Password", "registerPanel_checkPassword" );

$password2->addValidationFunctionParameters ( $passwordRow );
$password2->addValidationFunctionParameters ( $passwordRow2 );
$password2->addValidationFunctionParameters ( $password );
$password2->addValidationFunctionParameters ( $password2 );

$password->addValidationFunctionParameters ( $passwordRow );
$password->addValidationFunctionParameters ( $passwordRow2 );
$password->addValidationFunctionParameters ( $password );
$password->addValidationFunctionParameters ( $password2 );

$emailRow = $panel1->addRow ( "Email" );
$email = $emailRow->addInput ( "Email", "email", "Enter your email address", "registerPanel_checkEmail" );
$email->addValidationFunctionParameters ( $emailRow );
$email->addValidationFunctionParameters ( $email );

$emailPrivacy = addPrivacy ( $emailRow, 0 );

/**
 * Panel 2
 */
$panel2 = $form->addFormPanel ( "Tell us more about yourself (optional)" );
$firstNameRow = $panel2->addRow ( "Your first name" );
$firstName = $firstNameRow->addInput ( "First name", "text", "First name" );
$firstNamePrivacy = addPrivacy ( $firstNameRow, 1 );

$lastNameRow = $panel2->addRow ( "Your last name" );
$lastName = $lastNameRow->addInput ( "Last name", "text", "Last name" );
$lastNamePrivacy = addPrivacy ( $lastNameRow, 0 );

$birthdayRow = $panel2->addRow ( "Your birthday" );
$birthday = $birthdayRow->addInput ( "Birthday", "date", "" );
$birthdayPrivacy = addPrivacy ( $birthdayRow, 1 );

$hearUsRow = $panel2->addRow ( "How did you find us?" );
$hearUsSel = $hearUsRow->addSelect ( "J" );
$hearUsSel->addOption ( "Web search", "web" );
$hearUsSel->addOption ( "By friend", "friend" );
$hearUsSel->addOption ( "Other", "other" );

/**
 * Panel 3
 */

$panel3 = $form->addFormPanel ( "Finish the registration" );
$conditions = $panel3->addRow ( "You have read and agreed to our Term Of Service (TOS)" );
$conditions->addInput ( "conditions", "checkbox", "Checkbox" );
$buttonRow = $panel3->addButtonRow ();
$submitbtn = $buttonRow->addSubmit ( "Submit registration", "submit", "primary" );
$resetbtn = $buttonRow->addSubmit ( "Reset", "reset", "warning" );

if ($submitbtn->isSubmitted ()) {
	try {
		$username->addAssertionRule ( array (
				QInputObject::MUST_HAVE_VALUE,
				QInputObject::MIN_LENGTH,
				QInputObject::MAX_LENGTH,
				QInputObject::MUST_PASS_REGEX 
		), array (
				null,
				4,
				20,
				"/[a-zA-Z0-9]{4,20}/" 
		), "Invalid username" );
		$usernameStr = $username->getValue ();
		if (User::usernameExists ( $usernameStr )) {
			throw new QAssertionFailException ( $username, QInputObject::MUST_PASS_REGEX, "Username already exists" );
		}
		
		$password->addAssertionRule ( array (
				QInputObject::MUST_HAVE_VALUE,
				QInputObject::MIN_LENGTH 
		), array (
				null,
				4 
		), "Invalid password" );
		$email->addAssertionRule ( array (
				QInputObject::MUST_HAVE_VALUE,
				QInputObject::EMAIL 
		), array (
				null,
				null 
		), "Invalid email" );
		if ($birthday->hasValue ()) {
			$val = $birthday->getValue ( "" );
			if ($val != "") {
				$birthday->addAssertionRule ( array (
						QInputObject::DATE_PAST 
				), array (
						null 
				), "Invalid birthday" );
				$bdayint = QInputObject::dateToStamp ( $birthday->getValue () );
			}
		} else {
			$bdayint = null;
		}
		$fnp = User::getPermissionFromName ( $firstNamePrivacy->getValue () );
		$lnp = User::getPermissionFromName ( $lastNamePrivacy->getValue () );
		$emp = User::getPermissionFromName ( $emailPrivacy->getValue () );
		$bdp = User::getPermissionFromName ( $birthdayPrivacy->getValue () );
		$user = User::createUser ( $usernameStr, $password->getValue (), $email->getValue (), $firstName->getValue ( null ), $lastName->getValue ( null ), $bdayint, $emp, $fnp, $lnp, $bdp );
		Config::getConfig ()->redirect ( "" );
		die ();
	} catch ( QAssertionFailException $e ) {
		echo "<h2>Registration failed, because " . $e->getReason () . "</h2>";
		echo $form->getHTML ();
	}
	die ( "Submitted with errors" );
} else {
	echo $form->getHTML ();
}
?>
</body>
</html>
