function registerPanel_checkPassword(row1, row2, password1, password2) {
	var pw1 = password1.value;
	var pw2 = password2.value;
	if (pw2 == "") {
		return;
	}
	registerPanel_resetRowStyles(row2, password2);
	registerPanel_resetRowStyles(row1, password1);
	if (pw1 != pw2) {
		registerPanel_showErrorOnInput(row1, password1, "Passwords do not match!");
		registerPanel_showErrorOnInput(row2, password2, "Passwords do not match!");
		return;
	}
	var toCheck = password1.value;
	if (toCheck.length < 4) {
		registerPanel_showErrorOnInput(row1, password1, "Password must be at least 4 characters long!");
		return;
	}
	registerPanel_showSucesssOnInput(row2, password2);
	registerPanel_showSucesssOnInput(row1, password1)
}

function registerPanel_checkUsername(row, input) {
	var name = input.value;
	registerPanel_resetRowStyles(row, input);
	registerPanel_usernameAvailable = false;
	if (name.length < 4 || name.length > 20) {
		if (name.length < 4) {
			registerPanel_showErrorOnInput(row, input, "Username must have at least 4 characters!");
		} else {
			registerPanel_showErrorOnInput(row, input, "Username must have at most 20 characters!");
		}
		return;
	}
	var ajax = new Ajax();
	registerPanel_setLoadingStyle(row, input);
	ajax.asyncQuery("usernameAvailable", {
		"username" : name
	}, function(available) {
		registerPanel_resetRowStyles(row, input);
		if (available == "YES") {
			registerPanel_usernameAvailable = true;
			registerPanel_showSucesssOnInput(row, input);
		} else {
			registerPanel_showErrorOnInput(row, input, "This username is already taken!");
		}
	});
}

function registerPanel_checkEmail(row, email) {
	registerPanel_resetRowStyles(row, email);
	var mail = email.value;
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (re.test(mail)) {
		registerPanel_showSucesssOnInput(row, email);
	} else {
		registerPanel_showErrorOnInput(row, email, "Invalid email");
	}
}

function registerPanel_setLoadingStyle(row, input) {
	var glyph = input.parentElement.getElementsByClassName("form-control-feedback")[0];
	glyph.classList.add("glyphicon-refresh-animate");
	glyph.classList.add("glyphicon-refresh");
	glyph.classList.remove("hidden");
}

function registerPanel_resetRowStyles(row, input) {
	row.classList.remove("has-error");
	row.classList.remove("has-success");
	var msg = input.parentElement.getElementsByClassName("text-danger")[0];
	msg.innerHTML = "";
	var glyph = input.parentElement.getElementsByClassName("form-control-feedback")[0];
	glyph.classList.add("hidden");
	glyph.classList.remove("glyphicon-ok");
	glyph.classList.remove("glyphicon-remove");
	glyph.classList.remove("glyphicon-refresh");
	glyph.classList.remove("glyphicon-refresh-animate");
	glyph.classList.remove("spinning");
}

function registerPanel_showSucesssOnInput(row, input) {
	row.classList.add("has-success");
	var msg = input.parentElement.getElementsByClassName("text-danger")[0];
	msg.innerHTML = "";
	var glyph = input.parentElement.getElementsByClassName("form-control-feedback")[0];
	glyph.classList.add("glyphicon-ok");
	glyph.classList.remove("hidden");
}

function registerPanel_showErrorOnInput(row, input, errmsg) {
	row.classList.add("has-error");
	var msg = input.parentElement.getElementsByClassName("text-danger")[0];
	msg.innerHTML = errmsg;
	var glyph = input.parentElement.getElementsByClassName("form-control-feedback")[0];
	glyph.classList.add("glyphicon-remove");
	glyph.classList.remove("hidden");
	glyph.style.hidden = "false";
}

var registerPanel_M=function() {
	this.processingDialog=false;
	var instance=this;

	this.showProcessingDialog = function(title) {
		if (!instance.processingDialog) {
			instance.processingDialog = BootstrapDialog.show({type:BootstrapDialog.TYPE_SUCCESS,title:"Processing",message:"Processing"});
			var qbody = instance.processingDialog.getModalBody()[0];
			var rc = qbody.children[0];
			rc.innerHTML = '<p>'+Safe.escape(title)+'</p><div class="progress progress-striped active"><div class="progress-bar progress-bar-striped progress-bar-info active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div></div>';
			instance.processingDialog.setClosable(false);
		}
	}

	this.sendData = function(action, data) {
		var form = document.createElement("form");
		form.action = "";
		form.method = "POST";
		var getValue = function(key, value) {
			var elem = document.createElement("input");
			elem.type = "hidden";
			elem.name = key;
			elem.value = value;
			return elem;
		}
		form.appendChild(getValue("registerPanel_action", action));
		for ( var a in data) {
			var val = data[a];
			form.appendChild(getValue(a, val));
		}
		form.submit();
	}

	this.hideProcessingDialog = function(timeout=false) {
		if(timeout) {
			window.setTimeout(function() {instance.hideProcessingDialog(false);},timeout);
		} else {
			if (instance.processingDialog) {
				instance.processingDialog.close();
				instance.processingDialog=false;
			}
		}
	}
};

var registerPanel_instance=new registerPanel_M();


function registerPanel_register() {
	var username = document.getElementById('registerPanel_username').value
	var password1 = document.getElementById('registerPanel_password1').value
	var password2 = document.getElementById('registerPanel_password2').value
	var email = document.getElementById('registerPanel_email').value
	var vemail = document.getElementById('registerPanel_email_vis').value
	var fname = document.getElementById('registerPanel_fname').value
	var vfname = document.getElementById('registerPanel_fname_vis').value
	var lname = document.getElementById('registerPanel_lname').value
	var vlname = document.getElementById('registerPanel_lname_vis').value
	var bday = document.getElementById('registerPanel_bday').value
	var vbday = document.getElementById('registerPanel_bday_vis').value
	var terms=document.getElementById('registerPanel_accept').checked?"on":"off";
	var findus=document.getElementById('registerPanel_findus').value;
	registerPanel_instance.showProcessingDialog("Registering");
	var ajax=new Ajax();
	ajax.asyncQuery("register", {
		"username":username,
		"password":password1,
		"email":email,
		"firstname":fname,
		"lastname":lname,
		"terms":terms,
		"bday":bday,
		"firstname_vis":vfname,
		"lastname_vis":vlname,
		"email_vis":vemail,
		"bday_vis":vbday,
		"findus":findus }, function(result) {
			var res=JSON.parse(result);
			registerPanel_instance.hideProcessingDialog();
			if(res) {
				if(res["Status"]=="Ok") {
					BootstrapDialog.show({
						closeable: false,
						type : BootstrapDialog.TYPE_SUCCESS,
						title : 'Registration complete',
						message : 'You are now registered.',
						buttons: [
								{
									label : 'Take me to manual',
									cssClass : 'btn-default',
									action : function(dialog) {
										dialog.close();
										registerPanel_instance.sendData("next",{"next":"tutorial"});
									}
								},
						          {
							label : 'Take me to homepage',
							cssClass : 'btn-success',
							action : function(dialog) {
								dialog.close();
								registerPanel_instance.sendData("next",{"next":"main"});
							}
						},
						
					
			          ]
					});
					return;
				} else if(res["Status"]=="Miss") {
					details=res["Details"];
					var mt=document.createElement("div");
					var dt=document.createElement("div");
					dt.innerHTML="Registration failed, because of the following:<br /><br />";
					mt.appendChild(dt);
					var br=document.createElement("div");
					br.classList.add("qtable");
					br.classList.add( "fullwidth");
					for(var key in details) {
						var val=details[key];
						var tr=document.createElement("div");
						tr.classList.add("qtable-row");
						tr.classList.add( "fullwidth");
						var td=document.createElement("div");
						td.classList.add("qtable-cell");
						var th=document.createElement("div");
						th.classList.add("qtable-cell-header");
						th.classList.add("text-right");
						th.innerHTML=Safe.escape(key)+": ";
						td.innerHTML="&nbsp;&nbsp;"+Safe.escape(val)+"<br />&nbsp;";
						tr.appendChild(th);
						tr.appendChild(td);
						br.appendChild(tr);
					}
					mt.appendChild(br);
					BootstrapDialog.show({
						type : BootstrapDialog.TYPE_DANGER,
						title : 'Registration failed',
						message : mt
					});
					return;
				}
			}
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Registration failed',
				message : 'Registration failed. Please try it again later.'
			});
	});

}