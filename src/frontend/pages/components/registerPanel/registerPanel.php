<!DOCTYPE html>
<html lang="en">
<head>
<script generic="true" src="js/jquery.min.js"></script>
<script generic="true" src="js/bootstrap.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
<link generic="true" rel="stylesheet" href="css/theme.css">
<link generic="false" rel="stylesheet" href="registerPanel.css">
<script generic="false" src="registerPanel.js"></script>
<script generic="true" src="js/ajax.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap-dialog.min.css">
<script generic="true" src="js/bootstrap-dialog.min.js"></script>
<script generic="true" src="js/safe.js"></script>

</head>
<body>

<?php
if (isset ( $_POST ["registerPanel_action"] )) {
	$action = $_POST ["registerPanel_action"];
	if (isset ( $_POST ["next"] )) {
		$next = $_POST ["next"];
		switch ($next) {
			case "main":
				Config::getConfig ()->redirect ( "" );
				die ();
			break;
			case "tutorial":
				Config::getConfig ()->redirect ( "" );
				die ();
		}
	}
}

?>
	<form class="form-horizontal" name="registerPanel_form" action="" method="POST" role="form">
		<div class="container container-fluid seenit_panel">
			<div class="seenit_panel_title">Your account to be</div>
			<br />
			<div id="f90a30d79c0e143ce6d3fa96036b2f34" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_username"> Username: </label>
				<div class="col-sm-3 has-feedback">
					<input type="text" class="form-control" id="registerPanel_username" name="registerPanel_username" placeholder="Enter Username" onBlur="registerPanel_checkUsername(document.getElementById('f90a30d79c0e143ce6d3fa96036b2f34'),document.getElementById('registerPanel_username'))" />
					<span class="glyphicon glyphicon-ok form-control-feedback hidden" aria-hidden="true"></span>
					<span id="registerPanel_usernameStatus" class="text-danger"></span>
				</div>
			</div>
			<div id="6a84cc2fab720dccac128b952e2848e8" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_password1"> Password: </label>
				<div class="col-sm-3 has-feedback">
					<input type="password" class="form-control" id="registerPanel_password1" name="registerPanel_password1" placeholder="Enter Password" onBlur="registerPanel_checkPassword(document.getElementById('6a84cc2fab720dccac128b952e2848e8'),document.getElementById('74f6911492ca5041eeef5145e2c8fe3f'),document.getElementById('registerPanel_password1'),document.getElementById('registerPanel_password2'))" />
					<span class="glyphicon glyphicon-ok form-control-feedback hidden" aria-hidden="true"></span>
					<span id="registerPanel_usernameStatus" class="text-danger"></span>
				</div>
			</div>
			<div id="74f6911492ca5041eeef5145e2c8fe3f" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_password2"> Password once again: </label>
				<div class="col-sm-3 has-feedback">
					<input type="password" class="form-control" id="registerPanel_password2" name="registerPanel_password2" placeholder="Enter Password" onBlur="registerPanel_checkPassword(document.getElementById('6a84cc2fab720dccac128b952e2848e8'),document.getElementById('74f6911492ca5041eeef5145e2c8fe3f'),document.getElementById('registerPanel_password1'),document.getElementById('registerPanel_password2'))" />
					<span class="glyphicon glyphicon-ok form-control-feedback hidden" aria-hidden="true"></span>
					<span id="registerPanel_usernameStatus" class="text-danger"></span>
				</div>
			</div>
			<div id="694a15bde5ddc534200e128e95c07c80" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_email"> Email: </label>
				<div class="col-sm-3 has-feedback">
					<input type="email" class="form-control" id="registerPanel_email" name="registerPanel_email" placeholder="Enter your email address" onBlur="registerPanel_checkEmail(document.getElementById('694a15bde5ddc534200e128e95c07c80'),document.getElementById('registerPanel_email'))" />
					<span class="glyphicon glyphicon-ok form-control-feedback hidden" aria-hidden="true"></span>
					<span id="registerPanel_usernameStatus" class="text-danger"></span>
				</div>
				<div class="col-sm-2 has-feedback" style="min-width: 210px !important">
					<select class="form-control" id="registerPanel_email_vis" name="registerPanel_email_vis">
						<option selected value="2">Only me</option>
						<option value="1">Only my friends</option>
						<option value="0">Public</option>
					</select>
				</div>
			</div>
		</div>
		<div class="container container-fluid seenit_panel">
			<div class="seenit_panel_title">Tell us more about yourself (optional)</div>
			<br />
			<div id="550739bdb5f324f253bf25b5095ae36a" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_fname"> Your first name: </label>
				<div class="col-sm-3 has-feedback">
					<input type="text" class="form-control" id="registerPanel_fname" name="registerPanel_fname" placeholder="First name" />
				</div>
				<div class="col-sm-2 has-feedback" style="min-width: 210px !important">
					<select class="form-control" id="registerPanel_fname_vis" name="registerPanel_fname_vis">
						<option value="2">Only me</option>
						<option selected value="1">Only my friends</option>
						<option value="0">Public</option>
					</select>
				</div>
			</div>
			<div id="441b68603ce8b387028e6aec3a911886" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_lname"> Your last name: </label>
				<div class="col-sm-3 has-feedback">
					<input type="text" class="form-control" id="registerPanel_lname" name="registerPanel_lname" placeholder="Last name" />
				</div>
				<div class="col-sm-2 has-feedback" style="min-width: 210px !important">
					<select class="form-control" id="registerPanel_lname_vis" name="registerPanel_lname_vis">
						<option selected value="2">Only me</option>
						<option value="1">Only my friends</option>
						<option value="0">Public</option>
					</select>
				</div>
			</div>
			<div id="d99c1fc765e7684e32615d94659d64a6" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_bday"> Your birthday: </label>
				<div class="col-sm-3 has-feedback">
					<input type="date" class="form-control" id="registerPanel_bday" name="registerPanel_bday" placeholder="" />
				</div>
				<div class="col-sm-2 has-feedback" style="min-width: 210px !important">
					<select class="form-control" id="registerPanel_bday_vis" name="registerPanel_bday_vis">
						<option value="2">Only me</option>
						<option selected value="1">Only my friends</option>
						<option value="0">Public</option>
					</select>
				</div>
			</div>
			<div id="702b4b173b48d6e4afd3b2a5ea875e49" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_findus"> How did you find us?: </label>
				<div class="col-sm-3 has-feedback">
					<select class="form-control" id="registerPanel_findus" name="registerPanel_findus">
						<option value="web">Web search</option>
						<option value="friend">By friend</option>
						<option value="other">Other</option>
					</select>
				</div>
			</div>
		</div>
		<div class="container container-fluid seenit_panel">
			<div class="seenit_panel_title">Finish the registration</div>
			<br />
			<div id="a595adf890f9aa73105650b25e99cd4e" class="form-group">
				<label class="control-label col-sm-4" for="registerPanel_accept"> You have read and agreed to our Term Of Service (TOS): </label>
				<div class="col-sm-3 has-feedback">
					<input type="checkbox" class="form-control" id="registerPanel_accept" name="registerPanel_accept" placeholder="Checkbox" />
				</div>
			</div>
			<div id="f1f72ec06d0d1d0cfd338187e67eb8c3" class="form-group">
				<div class="col-sm-offset-4 col-sm-7">
					<div class="col-sm-3 has-feedback">
						<div class="btn btn-primary" name="registerPanel_submit" onClick="registerPanel_register();">Register</div>
					</div>
					<div class="col-sm-2 has-feedback">
						<input type="reset" class="btn btn-warning" name="registerPanel_reset" id="registerPanel_reset" value="Reset" />
					</div>
				</div>
			</div>
		</div>
	</form>

</body>
</html>
