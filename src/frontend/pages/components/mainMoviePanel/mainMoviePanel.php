<!DOCTYPE html>
<html lang="en">
<head>
<script generic="true" src="js/jquery.min.js"></script>
<script generic="true" src="js/bootstrap.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
<link generic="true" rel="stylesheet" href="css/theme.css">
<link generic="false" rel="stylesheet" href="mainMoviePanel.css">
<script generic="true" src="js/chart.js"></script>
<script generic="true" src="js/ajax.js"></script>
<script generic="true" src="js/bus.js"></script>
<script generic="true" src="js/safe.js"></script>
<script generic="false" src="mainMoviePanel.js"></script>
<script generic="true" src="js/bootstrap-dialog.min.js"></script>
<link generic="true" rel="stylesheet" href="css/bootstrap-dialog.min.css">

</head>
<body>
<?php
$movie = viewState::getMovie ();
$session = Config::getConfig ()->getSession ();
$logged = $session->isLogged ();
if ($logged) {
	$user = $session->getUser ();
	$mvi = MovieList::getMovieForUser ( $user, $movie );
}
$inList = false;
$seen = false;
$iWouldRecommend = false;
$iWouldNotRecommend = false;
if (isset ( $_POST ["mainMoviePanel_rec_form_yes"] ) || isset ( $_POST ["mainMoviePanel_rec_form_no"] )) {
	if ($logged) {
		$vote = isset ( $_POST ["mainMoviePanel_rec_form_yes"] ) ? MovieList::WOULD_RECOMMEND : MovieList::WOULD_NOT_RECOMMEND;
		if ($mvi) {
			$mvi->setRecommend ( $vote );
			$seen = $mvi->hasSeen ();
			$inList = true;
		} else {
			MovieList::newItem ( $user, $movie, $vote );
		}
	}
}
$myAlbums = array ();
if ($logged) {
	$user = Config::getConfig ()->getSession ()->getUser ();
	$myAlbums = viewState::getAllMoviesForUser ( $user );
	if ($mvi) {
		$recommend = $mvi->getRecommendation ();
		$seen = $mvi->hasSeen ();
		if ($recommend == MovieList::WOULD_RECOMMEND) {
			$iWouldRecommend = true;
		} else if ($recommend == MovieList::WOULD_NOT_RECOMMEND) {
			$iWouldNotRecommend = true;
		}
	}
}

$iVoted = $iWouldNotRecommend || $iWouldRecommend;

$recommendCount = MovieList::getRecommendCount ( $movie );
$notRecommendCount = MovieList::getNotRecommendCount ( $movie );
$allCounts = $recommendCount + $notRecommendCount;

$recommendPercent = ( int ) $allCounts == 0 ? 0 : ((100 * $recommendCount) / $allCounts);
$notRecommendPercent = $allCounts == 0 ? 0 : (100 - $recommendPercent);

$movieId = $movie->getID ();
echo '<input type=hidden id="mainMoviePanel_movieId" value="' . $movieId . '">';
?>
	<div class="flex-container-column">
		<div class="container mainMoviePanel_mainPanel well">
			<div class="mainMoviePanel_mainPanel_row1 fullwidth">
				<div class="mainMoviePanel_leftPanel well fullwidth">
					<div class="container-fluid flex-container-column">
						<div class="flex-container-row">
							<div class="mainMoviePanel_imagePanel">
						<?php
						$safePoster = Safe::escape ( $movie->getPosterURL () );
						echo '<img src="' . $safePoster . '" width="240" height="320" />';
						?>
							</div>
							<div class="flex-item fullwidth margin-left-20">
								<h2 class="fullwidth alignCenter"><?php echo Safe::escape($movie->getNameEN()); ?></h2>
								<div class="qtable qcellspacing-10">
									<div class="qtable-row">
										<div class="qtable-cell-header magin-right-30 text-right nowrap">Release year:</div>
										<div class="qtable-cell margin-left-30"><?php echo $movie->getYear(); ?></div>
									</div>
									<div class="qtable-row">
										<div class="qtable-cell-header magin-right-30 text-right nowrap">Seen by:</div>
										<div class="qtable-cell margin-left-30"><?php $cnt=Movielist::getUsercountSeenMovie($movie); echo $cnt.($cnt==1?" user":" users");?></div>
									</div>
									<div class="qtable-row">
										<div class="qtable-cell-header magin-right-30 text-right nowrap">Seen:</div>
										<div class="qtable-cell margin-left-30">
											<?php
											if ($seen) {
												echo '<span class="glyphicon glyphicon-ok text-success"></span>';
											} else {
												echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
											}
											?>
										</div>
									</div>
									<?php
									if (! $inList || true) { // TODO: Show always?
										echo '
											<div class="qtable-row">
												<div class="qtable-cell-header magin-right-30 text-right nowrap">Save to:</div>
												<div class="qtable-cell text-info">
													<select class="form-control" id="mainMoviePanel_sel">
														<option value="-1">All my movies</option>
														<optGroup label="My Albums">
													';
										foreach ( $myAlbums as $album ) {
											echo '<option value="' . $album->getAlbumID () . '">' . Safe::escape ( $album->getAlbumName () ) . '</option>';
										}
										
										echo '			</optGroup>
													</select>
												</div>
												<div class="qtable-cell">
													<span id="mainMoviePanel_ico1" class="glyphicon glyphicon-plus text-success pointer" title="Import this movie"></span>
													<span id="mainMoviePanel_ico2" class="hidden glyphicon glyphicon-refresh glyphicon-refresh-animate text-info" title="Processing"></span>
												</div>
											</div>
											<div class="qtable-row">
												<div class="qtable-cell"></div>
											<div class="qtable-cell">
													<span id="mainMoviePanel_msg1" class="hidden text-success">Added to <b>All My Movies</b></span>
													<span id="mainMoviePanel_msg2" class="hidden text-danger"><b>Import failed. Please refresh the page and try it again.</b></span>
												</div>
											</div>';
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="mainMoviePanel_rightPanel well">
					<div class="flex-container-row">
						<div class="flex-item">
						<?php
						echo '<input type=hidden id="mainMoviePanel_graph_rec" value="' . $recommendPercent . '">';
						echo '<input type=hidden id="mainMoviePanel_graph_not_rec" value="' . $notRecommendPercent . '">';
						?>
						<canvas id="mainMoviePanel_graph" width="200" height="200">Imagine a pie chart at this place</canvas>
						</div>
						<div class="flex-item margin-left-20 fullwidth nowrap">
							<label class="text-info"><?php echo $recommendPercent; ?>% Would recommend</label><br /> <label class="text-danger"><?php echo $notRecommendPercent;?>% Would not recommend</label>
						</div>
					</div>
					<?php
					if ($logged) {
						$csfdid = $movie->getCsfdID ();
						$imdbid = $movie->getImdbID ();
						echo '<br />
						<img style="display:inline" src="http://movieranking.cz/csfd/7/' . $csfdid . '.png" width="200" height="56" />
						<img style="display:inline" src="http://movieranking.cz/imdb/7/' . $imdbid . '.png" width="200" height="56" />
					<div class="flex-container-column mainMoviePanel_box">
						<div class="flex-item text-center">
							<h4>Recommendation</h4>
						</div>';
						if ($iVoted) {
							echo '<div class="flex-item text-center text-' . ($iWouldRecommend ? "info" : "danger") . '">You would ' . ($iWouldNotRecommend ? "not " : "") . 'recommend this movie</div>';
						}
						echo '
						<div class="flex-item text-center">
							<form name=mainMoviePanel_rec_form " method="POST" action="#">
								<input type=submit class="btn btn-info" name="mainMoviePanel_rec_form_yes" style="width: 70px" value="Yes"> <input type=submit class="btn btn-danger" name="mainMoviePanel_rec_form_no" style="width: 70px" value="No">
							</form>
						</div>
					</div>';
					} else {
						echo '<div class="flex-container flex-grow" style="flex-grow: 1"></div><div class="flex-item text-center"><h3>You must be logged in to share your opinion</h3></div>';
					}
					?>
				</div>
			</div>
			<hr style="width: 100%; border-color: black;" />
			<div class="flex-item well">
				<div class="mainMoviePanel_mainPanel_row2">

					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
						<li role="presentation"><a id="mainMoviePanel_discussionTab" href="#discussion" aria-controls="discussion" role="tab" data-toggle="tab">Discussion</a></li>
						<li role="presentation"><a href="#ratings" aria-controls="ratings" role="tab" data-toggle="tab">User ratings</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active mainMoviePanel_mainPanel_row2Wrap mainMoviePanel_mainPanel_leftborder" id="overview">
							<div class="mainMoviewPanel_overview">
							<?php
							echo Safe::escape ( $movie->getDescriptionEN () );
							?>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane mainMoviePanel_mainPanel_row2Wrap" id="discussion">
							<div class="fullwidth text-center hidden" id="mainMoviePanel_discussionRootRefresh">
								<span class="glyphicon bigGlyph glyphicon-refresh glyphicon-refresh-animate text-info"></span>
							</div>
							<?php
							if ($logged) {
								echo '<span id="mainMoviePanel_discussion_btns">
									<div class="btn btn-success" style="margin-bottom: 10px">
										<span class="glyphicon glyphicon-plus"> </span>
										&nbsp; New post
									</div>
								</span>';
							}
							?>
							<ul class="media-list" id="mainMoviePanel_discussionRoot">
							</ul>
						</div>
						<div role="tabpanel" class="tab-pane mainMoviePanel_mainPanel_row2Wrap" id="ratings">User ratings</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
