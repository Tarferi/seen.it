var mainMoviePanel_M = function() {
	var instance = this;
	this.ico1 = document.getElementById('mainMoviePanel_ico1');
	this.ico2 = document.getElementById('mainMoviePanel_ico2');
	this.msg1 = document.getElementById('mainMoviePanel_msg1');
	this.msg2 = document.getElementById('mainMoviePanel_msg2');
	this.sel = document.getElementById('mainMoviePanel_sel');
	this.movieId = document.getElementById('mainMoviePanel_movieId').value;
	
	this.btnGroup=document.getElementById('mainMoviePanel_discussion_btns');

	if(this.btnGroup) {
		this.newPostBtn=this.btnGroup.children[0];
		this.newPostBtn.addEventListener("click",function() {instance.newPostClicked()});
	}
	
	this.mainMoviePanel_importClicked = function() {
		var id = instance.sel.options[instance.sel.selectedIndex].value;
		var action = "copyMovies";
		instance.msg1.innerHTML = "Added to album <b>" + Safe.escape(instance.sel.options[instance.sel.selectedIndex].innerHTML) + "</b>"
		if (id == -1) {
			instance.msg1.innerHTML = "Added to <b>All My Movies</b>"
			action = "importMovies";
		}
		var movieId = instance.movieId;
		instance.ico1.classList.add("hidden");
		instance.ico2.classList.remove("hidden");
		if (!instance.msg1.classList.contains("hidden")) {
			instance.msg1.classList.add("hidden");
		}
		if (!instance.msg2.classList.contains("hidden")) {
			instance.msg2.classList.add("hidden");
		}
		var ajax = new Ajax();
		ajax.asyncQuery(action, {
			"data" : movieId,
			"albumId" : id
		}, function(result) {
			var res = JSON.parse(result);
			if (res) {
				if (res["Status"] == "Ok") {
					var created = res["Created"];
					var added = res["Added"];
					EventBus.dispatchEvent("MovieImported", {
						"ToAllMovies" : (id == -1),
						"AlbumID" : id,
						"MovieID" : movieId,
						"Created" : created,
						"Added" : added,
						"Origin" : "Click",
						"Seen" : false
					});
					instance.ico1.classList.remove("hidden");
					instance.ico2.classList.add("hidden");
					instance.msg1.classList.remove("hidden");
					return;
				}
			}
			instance.ico1.classList.remove("hidden");
			instance.ico2.classList.add("hidden");
			instance.msg2.classList.remove("hidden");
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Operation failed',
				message : 'Your last operation was not saved. Please refresh the page.',
			});
		});
	}

	this.ico1.addEventListener("click", this.mainMoviePanel_importClicked);

	this.discusstionTab = document.getElementById('mainMoviePanel_discussionTab');
	this.discussionRoot = document.getElementById('mainMoviePanel_discussionRoot');
	this.discussionLoadingIcon = document.getElementById('mainMoviePanel_discussionRootRefresh');

	this.processingDialog=false;
	
	this.showProcessingDialog = function(title) {
		if (!instance.processingDialog) {
			instance.processingDialog = BootstrapDialog.show({type:BootstrapDialog.TYPE_SUCCESS,title:"Processing",message:"Processing"});
			var qbody = instance.processingDialog.getModalBody()[0];
			var rc = qbody.children[0];
			rc.innerHTML = '<p>'+Safe.escape(title)+'</p><div class="progress progress-striped active"><div class="progress-bar progress-bar-striped progress-bar-info active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div></div>';
			instance.processingDialog.setClosable(false);
		}
	}


	this.hideProcessingDialog = function(timeout=false) {
		if(timeout) {
			window.setTimeout(function() {instance.hideProcessingDialog(false);},timeout);
		} else {
			if (instance.processingDialog) {
				instance.processingDialog.close();
				instance.processingDialog=false;
			}
		}
	}
	
	this.postRemove=function(post) {
		BootstrapDialog.confirm('Are you sure you want to delete that post?\nThis operation cannot be undone!',function(result) {
			if(result) {
				instance.showProcessingDialog("Removing post");
				var ajax=new Ajax();
				var postId=post.getPostId();
				ajax.asyncQuery("deletePost",{"postId":postId},function(result) {
					var res=JSON.parse(result);
					if(res) {
						if(res["Status"]=="Ok") {
							instance.hideProcessingDialog(1000);
							window.setTimeout(instance.discussionLoad,1000);
							return;
						}
					}
					instance.hideProcessingDialog();
					BootstrapDialog.show({
						type : BootstrapDialog.TYPE_DANGER,
						title : 'Operation failed',
						message : 'Failed to delete post. Please report this.',
					});
				});
			}
		});
	}
	
	this.newPostClicked=function() {
		var post=new mainMoviePanel_post_M();
		post.setPostId(false);
		post.setTitle("");
		post.setContents("");
		instance.postEdit(post);
	}
	
	this.savePost=function(post) {
		instance.showProcessingDialog("Saving post changes");
		var ajax=new Ajax();
		var postId=post.getPostId();
		var title=post.getTitle();
		var contents=Safe.EscapedNewLines(post.getContents());
		var action=postId===false?"newPost":"savePost";
		ajax.asyncQuery(action,{"postId":postId, "movieId":instance.movieId, "title":title,"contents":contents},function(result) {
			var res=JSON.parse(result);
			if(res) {
				if(res["Status"]=="Ok") {
					instance.hideProcessingDialog(1000);
					window.setTimeout(instance.discussionLoad,1000);
					return;
				}
			}
			instance.hideProcessingDialog();
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Operation failed',
				message : 'Failed to get comments for this movie. Please report this.',
			});
		});
	}
	
	this.postEdit = function(post) {
		var root = document.createElement("div");
		root.classList.add("qtable");
		
		var creatingNew=post.getPostId()===false;

		var row1 = document.createElement("div");
		row1.classList.add("qtable-row");

		var getTitle = function(title) {
			var el = document.createElement("label");
			el.innerHTML = Safe.escape(title);
			return el;
		}

		var titleElem = document.createElement("div");
		titleElem.classList.add("qtable-cell");
		titleElem.appendChild(getTitle("Post title:"));
		row1.appendChild(titleElem);

		var title = document.createElement("input");
		title.setAttribute("required",true);
		title.classList.add("form-control");
		title.placeholder = "Post title";
		titleElem.appendChild(title);

		var row2 = document.createElement("div");
		row2.classList.add("qtable-row");

		var contentElem = document.createElement("div");
		contentElem.classList.add("qtable-cell");
		contentElem.appendChild(getTitle("Message:"));
		row2.appendChild(contentElem);

		var content = document.createElement("textarea");
		content.classList.add("form-control");
		content.setAttribute("required",true);
		content.placeholder="Post message";
		content.style.width = "100%";
		content.style.resize = "vertical";
		content.style.minHeight = "150px";
		contentElem.appendChild(content);

		root.style.width = "100%";
		root.appendChild(row1);
		root.appendChild(row2);

		title.value = post.getTitle();
		content.value = Safe.ValueNewLines(post.getContents());

		var titlr=creatingNew?"Create post":"Edit post";
		var titlrbtn=creatingNew?"Create":"Save";
		var allowCloseWithPopup=false;
		BootstrapDialog.show({
			title : titlr,
			message : root,
			onhide: function(dialogRef){
				var qtitle = post.getTitle();
				var qcontent = Safe.ValueNewLines(post.getContents());	
				var atitle=title.value;
				var acontent=content.value;
				if((qtitle!=atitle)|| (qcontent!=acontent)) {
					if(!allowCloseWithPopup) {
						BootstrapDialog.confirm('Closing the dialog will discard your changes\nContinue?',function(result) {
							if(result) {
								allowCloseWithPopup=true;
								dialogRef.close();
							}
						});
						return false;
					}
				}
			},
			buttons : [{
				label : 'Cancel',
				cssClass : 'btn-warning',
				action : function(dialog) {
					allowCloseWithPopup=true;
					dialog.close();
				}
			}, {
				label : titlrbtn,
				cssClass : 'btn-success',
				action : function(dialog) {
					var tl=title.value;
					var cl=content.value;
					var ok=true;
					if(tl.length==0) {
						if(!titleElem.classList.contains("has-error")) {
							titleElem.classList.remove("has-success");
							titleElem.classList.add("has-error");
						}
						ok=false;
					} else {
						if(!titleElem.classList.contains("has-success")) {
							titleElem.classList.remove("has-error");
							titleElem.classList.add("has-success");
						}
					}
					if(cl.length==0) {
						if(!contentElem.classList.contains("has-error")) {
							contentElem.classList.add("has-error");
						}
						ok=false;
					} else {
						if(!contentElem.classList.contains("has-success")) {
							contentElem.classList.remove("has-error");
							contentElem.classList.add("has-success");
						}
					}
					if(ok) {
						post.setTitle(title.value);
						post.setContents(content.value);
						allowCloseWithPopup=true;
						dialog.close();
						instance.savePost(post);
					}
				}
			} ]
		});
	}

	this.discussionLoad = function() {
		instance.discussionRoot.innerHTML = "";
		if(instance.btnGroup){ 
			instance.btnGroup.classList.add("hidden");
		}
		instance.discussionLoadingIcon.classList.remove("hidden");
		var ajax = new Ajax();
		var movieId = instance.movieId;
		ajax.asyncQuery("getCommentsForMovie", {
			"movieId" : movieId
		}, function(result) {
			var res = JSON.parse(result);
			if(instance.btnGroup){
				instance.btnGroup.classList.remove("hidden");
			}
			if (res) {
				if (res["Status"] == "Ok") {
					data = res["Data"];
					myId = res["MyId"];
					instance.discussionLoadingIcon.classList.add("hidden");
					for (var i = 0; i < data.length; i++) {
						var elem = instance.constructCommentFromRow(data[i], myId);
						instance.discussionRoot.appendChild(elem);
					}
					return;
				}
			}
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_DANGER,
				title : 'Operation failed',
				message : 'Failed to get comments for this movie. Please report this.',
			});
		});
	}

	this.constructCommentFromRow = function(row, myId) {
		var data = new mainMoviePanel_post_M();
		data.setImageURL(row["AvatarURL"]);
		data.setUsername(row["CreatorUsername"]);
		data.setTitle(row["Title"]);
		data.setContents(row["Contents"]);
		var postId = row["CommentID"];
		data.setPostId(postId);
		var posterId = row["CreatorID"];
		if (myId == posterId) {
			data.setEditable(true);
			data.setRemovable(true);
			data.setReportable(false);
		} else {
			data.setEditable(false);
			data.setRemovable(false);
			data.setReportable(true);
		}
		return data.getObj();
	}

}

var mainMoviePanel_post_M = function() {
	var instance = this;
	this.obj = document.createElement("li");
	this.titleObj = document.createElement("span");
	this.usernameObj = document.createElement("span");
	this.contentsObj = document.createElement("span");
	this.obj.classList.add("well");
	
	this.titleValue;
	this.contentValue;
	
	this.editBtn = document.createElement("span");
	this.editBtn.classList.add("glyphicon");
	this.editBtn.classList.add("text-muted");
	this.editBtn.classList.add("pointer");
	this.editBtn.classList.add("glyphicon-edit");
	this.editBtn.title = "Edit this post";
	this.editBtn.style.marginRight="5px";

	this.reportBtn = document.createElement("span");
	this.reportBtn.classList.add("glyphicon");
	this.reportBtn.classList.add("text-muted");
	this.reportBtn.classList.add("pointer");
	this.reportBtn.classList.add("glyphicon-warning-sign");
	this.reportBtn.title = "Report this post";
	this.reportBtn.style.marginRight="5px";

	this.removeBtn = document.createElement("span");
	this.removeBtn.classList.add("glyphicon");
	this.removeBtn.classList.add("text-muted");
	this.removeBtn.classList.add("pointer");
	this.removeBtn.classList.add("glyphicon-remove");
	this.removeBtn.title = "Remove this post";

	this.imgObj = document.createElement("img");
	this.imgObj.width = "40";
	this.imgObj.height = "40";

	this.postId=false;
	
	this.setPostId=function(postId) {
		instance.postId=postId;
	}
	
	this.getPostId=function() {
		return instance.postId;
	}
	
	this.getTitle = function() {
		return instance.titleValue;
	}

	this.getContents = function() {
		return instance.contentValue;
	}

	this.setEditable = function(editable) {
		if (!editable) {
			instance.editBtn.classList.add("hidden");
		} else {
			instance.editBtn.addEventListener("click", function() {
				mainMoviePanel_instance.postEdit(instance);
			});
		}
	}
	
	this.setRemovable = function(removable) {
		if (!removable) {
			instance.removeBtn.classList.add("hidden");
		} else {
			instance.removeBtn.addEventListener("click", function() {
				mainMoviePanel_instance.postRemove(instance);
			});
		}
	}
	
	this.setReportable = function(reportable) {
		if (!reportable) {
			instance.reportBtn.classList.add("hidden");
		}
	}

	this.getObj = function() {
		return this.obj;
	}

	this.setImageURL = function(url) {
		instance.imgObj.src = url;
	}

	this.setUsername = function(username) {
		instance.usernameObj.innerHTML = Safe.escape(username);
	}

	this.setTitle = function(title) {
		instance.titleValue=title;
		instance.titleObj.innerHTML = Safe.escape(title);
	}
	
	this.setContents = function(contents) {
		instance.contentValue=contents;
		instance.contentsObj.innerHTML = Safe.HTMLNewLines(Safe.escape(contents));
	}

	var leftObj = document.createElement("div");
	leftObj.classList.add("media-left");
	leftObj.classList.add("text-center");
	leftObj.classList.add("border-right");
	leftObj.appendChild(this.imgObj);
	leftObj.appendChild(this.usernameObj);
	this.obj.appendChild(leftObj);

	var rightObj = document.createElement("div");
	rightObj.classList.add("media-body");
	rightObj.classList.add("padding-left-10");
	rightObj.style.overflow="visible";
	var h4 = document.createElement("h4");
	h4.appendChild(this.titleObj);
	rightObj.appendChild(h4);

	var btnGroup = document.createElement("span");
	btnGroup.style.float = "right";
	btnGroup.style.position="relative";
	btnGroup.style.top="-20px";
	btnGroup.style.right="-10px";
	
	btnGroup.appendChild(this.editBtn);
	btnGroup.appendChild(this.reportBtn);
	btnGroup.appendChild(this.removeBtn);
	h4.appendChild(btnGroup);

	var h5 = document.createElement("h5");
	h5.appendChild(this.contentsObj);
	rightObj.appendChild(h5);

	this.obj.appendChild(rightObj);
}

var mainMoviePanel_instance;

document.addEventListener('DOMContentLoaded', function() {
	mainMoviePanel_instance = new mainMoviePanel_M();
	new Chart("mainMoviePanel_graph", [ document.getElementById("mainMoviePanel_graph_rec").value * 1, document.getElementById("mainMoviePanel_graph_not_rec").value * 1 ], [ "#31708F", "#A94442" ]);

	$('a[id="mainMoviePanel_discussionTab"]').on('shown.bs.tab', function(e) {
		mainMoviePanel_instance.discussionLoad();
	});

});
