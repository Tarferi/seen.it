var loggedInPanel_M = function() {
	var instance = this;

	this.albumCountObj = document.getElementById('loggedInPanel_albumCount');
	this.unseenCountObj = document.getElementById('loggedInPanel_unseenCount');

	this.logoutBtn = document.getElementById('loggedInPanel_logout');
	this.messagesBtn = document.getElementById('loggedInPanel_message');
	this.profileBtn = document.getElementById('loggedInPanel_profile');

	this.attachExecution = function(btn, name, value) {
		btn.addEventListener("click", function() {
			instance.executeForm(name, value);
		});
	}
	if(this.logoutBtn) {
		this.attachExecution(this.logoutBtn, "loggedInPanel_action", "logout");
		this.attachExecution(this.messagesBtn, "loggedInPanel_action", "messages");
		this.attachExecution(this.profileBtn, "loggedInPanel_action", "profile");
	}
	

	this.executeForm = function(name, value) {
		var frm = document.createElement("form");
		frm.action = "";
		frm.method = "POST";
		var hidden = document.createElement("input");
		hidden.type = "hidden";
		hidden.name = name;
		hidden.value = value;
		frm.appendChild(hidden);
		frm.submit();
	}

	this.getAlbumCount = function() {
		return instance.albumCountObj.innerHTML * 1;
	}

	this.setAlbumCount = function(value) {
		instance.albumCountObj.innerHTML = value + "";
	}
	this.getUnseenCount = function() {
		return instance.unseenCountObj.innerHTML * 1;
	}

	this.setUnseenCount = function(value) {
		instance.unseenCountObj.innerHTML = value + "";
	}

	this.albumAdded = function(albumData) {
		instance.setAlbumCount(instance.getAlbumCount() + 1);
	}

	this.albumDeleted = function(albumData) {
		instance.setAlbumCount(instance.getAlbumCount() - 1);
	}

	this.seenChanged = function(seenData) {
		var seen = seenData["Seen"];
		var origin = seenData["Origin"];
		if (seen) {
			if (origin == "Click") {
				instance.setUnseenCount(instance.getUnseenCount() - 1);
			}
		} else {
			if (origin == "Click") {
				instance.setUnseenCount(instance.getUnseenCount() + 1);
			}
		}
	}

	this.movieDeleted = function(seenData) {
		var seen = seenData["Seen"];
		if (!seen) {
			instance.setUnseenCount(instance.getUnseenCount() - 1);
		}
	}

}

var loggedInPanel_instance;

document.addEventListener('DOMContentLoaded', function() {
	loggedInPanel_instance = new loggedInPanel_M();

	EventBus.addListener("AlbumAdded", loggedInPanel_instance.albumAdded);
	EventBus.addListener("AlbumDeleted", loggedInPanel_instance.albumDeleted);

	EventBus.addListener("SeenChanged", loggedInPanel_instance.seenChanged);
	EventBus.addListener("MovieDeleted", loggedInPanel_instance.movieDeleted);
	EventBus.addListener("MovieImported", loggedInPanel_instance.seenChanged);

});