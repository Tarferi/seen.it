<!DOCTYPE html>
<html lang="en">
<head>
	<script generic="true" src="js/jquery.min.js"></script>
	<script generic="true" src="js/bootstrap.min.js"></script>
	<link generic="true" rel="stylesheet" href="css/bootstrap.min.css">
	<link generic="true" rel="stylesheet" href="css/bootstrap-theme.min.css">
</head>
<body>
	<form class="navbar-form navbar-left" role="search">
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Search term">
		</div>
		<button type="submit" class="btn btn-default">Search</button>
	</form>
</body>
</html>
