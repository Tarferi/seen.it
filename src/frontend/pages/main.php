<?php

$headerFunctions = function () {
	$logged = Config::getConfig ()->getSession ()->isLogged ();
	preProcessor::get ()->includeHead ( "navigationTop" );
	if ($logged) {
		preProcessor::get ()->includeHead ( "mainLoggedPanel" );
	}
	preProcessor::get ()->includeHead ( "pageFooter" );
};

$bodyHeaderFunction = function () {
	preProcessor::get ()->includeBody ( "navigationTop" );
};

$bodyFunction = function () {
	$logged = Config::getConfig ()->getSession ()->isLogged ();
	if ($logged) {
		preProcessor::get ()->includebody ( "mainLoggedPanel" );
	} else {
		echo "Not logged yet";
	}
};

$bodyFooterFunction = function () {
	preProcessor::get ()->includeBody ( "pageFooter" );
};

$pageTitle = "Seen.it";
include "defaultPage.php";

?>