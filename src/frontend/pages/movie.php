<?php
$headerFunctions = function () {
	preProcessor::get ()->includeHead ( "navigationTop" );
	preProcessor::get ()->includeHead ( "mainMoviePanel" );
	preProcessor::get ()->includeHead ( "pageFooter" );
};

$bodyHeaderFunction = function () {
	preProcessor::get ()->includeBody ( "navigationTop" );
};

$bodyFunction = function () {
	preProcessor::get ()->includebody ( "mainMoviePanel" );
};

$bodyFooterFunction = function () {
	preProcessor::get ()->includeBody ( "pageFooter" );
};

$pageTitle = "Seen.it";
include "defaultPage.php";

?>