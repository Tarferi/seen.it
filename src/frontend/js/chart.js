var Chart = function(id, data, colors) {
	var newData = [];
	var newColors = [];
	for (var i = 0; i < data.length; i++) {
		var d = data[i];
		if (d != 0) {
			newData.push(data[i]);
			newColors.push(colors[i]);
		}
	}
	data=newData;
	colors=newColors;
	var elementId = id;
	var canvas = document.getElementById(elementId);
	var ctx = canvas.getContext("2d");
	var lastend = 0;
	var myTotal = 0;
	var hasData = false;
	for (var i = 0; i < data.length; i++) {
		if (data[i] > 0) {
			hasData = true;
			break;
		}
	}
	if (!hasData) {
		data = [ 1 ];
		colors = [ "lightgrey" ];
	}
	for (var e = 0; e < data.length; e++) {
		myTotal += data[e];
	}
	canvasHH = canvas.height / 2;
	canvasWH = canvas.width / 2;
	var drawSublines = data.length > 1;
	for (var i = 0; i < data.length; i++) {
		ctx.fillStyle = colors[i];
		ctx.beginPath();
		ctx.moveTo(canvas.width / 2, canvas.height / 2);
		var rotation = lastend + (Math.PI * 2 * (data[i] / myTotal));
		ctx.arc(canvasWH, canvasHH, canvasHH, lastend, rotation, false);
		ctx.lineTo(canvasWH, canvasHH);
		ctx.fill();
		if (drawSublines) {
			ctx.beginPath();
			console.log(lastend);
			console.log(myTotal);
			ctx.moveTo(canvasWH, canvasHH);
			ctx.lineTo(canvasWH + (Math.cos(lastend) * canvasHH), canvasHH + (Math.sin(lastend) * canvasHH));
			ctx.stroke();
			ctx.moveTo(canvasWH, canvasHH);
			ctx.lineTo(canvasWH + (Math.cos(rotation) * canvasHH), canvasHH + (Math.sin(rotation) * canvasHH));
			ctx.stroke();
		}
		lastend += Math.PI * 2 * (data[i] / myTotal);
	}
	ctx.beginPath();
	ctx.arc(canvas.width / 2, canvas.height / 2, canvas.height / 2, 0, 2 * Math.PI);
	ctx.stroke();
}