var Ajax = function() {
	var instance = this;

	this.request_url = document.location.protocol + "//" + document.location.host + "/Ajax";

	this.query = function(action, data) {
		var datastr = [];
		for ( var key in data) {
			datastr[datastr.length] = key + "=" + data[key];
		}
		var data = datastr.join("&");
		var url = instance.request_url + "?action=" + action + "&cache=" + (Math.random() * 1000000) + "&" + data;
		var req = new XMLHttpRequest();
		req.open("GET", url, false);
		req.send();
		var status = req.status;
		return req.responseText;
	}

	this.asyncQuery = function(action, data, callback) {
		var qdata = data;
		var qcallback = callback;
		var qaction = action;
		async(function() {
			var datastr = [];
			for ( var key in qdata) {
				datastr[datastr.length] = key + "=" + qdata[key];
			}
			var data = datastr.join("&");
			var url = instance.request_url + "?action=" + qaction + "&cache=" + (Math.random() * 1000000) + "&" + data;
			//console.log(url);
			var req = new XMLHttpRequest();
			req.onreadystatechange = function() {
				if (req.readyState == 4 && req.status == 200) {
					var status = req.status;
					var result = req.responseText;
					qcallback(result);
				}
			};
			req.open("GET", url, true);
			req.send();
		});
	}
}

function async(qfunc) {
	setTimeout(qfunc, 10);
}