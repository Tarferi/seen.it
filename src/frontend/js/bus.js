var Bus = function() {
	var instance = this;

	this.dispatchers = {};

	this.addListener = function(event, callback) {
		if (!instance.dispatchers[event]) {
			instance.dispatchers[event] = [];
		}
		instance.dispatchers[event].push(callback);
	}

	this.dispatchEvent = function(event, data) {
		var dsp = instance.dispatchers[event];
		if (dsp) {
			dsp.forEach(function(asp) {
				asp(data);
			})
		}
	}

	this.removeDispatchListener = function(event, listener) {
		var dsp = instance.dispatchers[event];
		if (dsp) {
			var index = dsp.indexOf(listener);
			if (indexx > -1) {
				dsp.splice(index, 1);
			}

		}
	}

}

var EventBus = new Bus();