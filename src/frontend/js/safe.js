var Safe = function() {

	this.escape = function(text) {
		if (text) {
			return text.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
		} else {
			return text;
		}
	}

	this.descape = function(text) {
		if (text) {
			return text.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"').replace(/&#039;/g, "'");
		} else {
			return text;
		}
	}

	this.HTMLNewLines = function(text) {
		return text.replace(/\n/g, "<br />");
	}

	this.ValueNewLines = function(text) {
		return text.replace(/<br \/>/g, "\n");
	}
	
	this.EscapedNewLines=function(text) {
		return text.replace(/\n/g, "%0A");
	}
}

var Safe = new Safe();