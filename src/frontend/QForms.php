<?php

class QForm {
	private $panels = array ();
	private $name;

	public function QForm($name) {
		$this->name = md5 ( $name );
	}

	public function addFormPanel($name) {
		$form = new QFormPanel ( $this->name, $name );
		$this->panels [] = $form;
		return $form;
	}

	public function getHTML() {
		$html = "<form class=\"form-horizontal\" name=\"" . $this->name . "\" action=\"\" method=\"POST\" role=\"form\">";
		foreach ( $this->panels as $panel ) {
			$html .= $panel->getHTML ();
		}
		$html .= "</form>";
		return $html;
	}

	public function isSubmitted() {
		return isset ( $_POST [$this->name] );
	}

}

class QFormPanel {
	private $title;
	private $id;
	private $rows = array ();

	public function getID() {
		return $this->id;
	}

	public function __construct($formID, $name) {
		$this->title = $name;
		$this->id = md5 ( $formID . "_" . $name );
	}

	/**
	 *
	 * @param unknown $title
	 * @return QRow
	 */
	public function addRow($title) {
		$row = new QRow ( $this->id, $title );
		$this->rows [] = $row;
		return $row;
	}

	/**
	 *
	 * @return QButtonRow
	 */
	public function addButtonRow() {
		$row = new QButtonRow ( $this->id, "empty" );
		$this->rows [] = $row;
		return $row;
	}

	public function getHTML() {
		$html = "<div class=\"container container-fluid seenit_panel\"><div class=\"seenit_panel_title\">" . $this->title . "</div><br />";
		foreach ( $this->rows as $row ) {
			$html .= $row->getHTML ();
		}
		$html .= "</div>";
		return $html;
	}

}

class QAssertionFailException extends Exception {
	private $rule;
	private $source;
	private $reason;

	public function __construct($source, $rule, $reason) {
		$this->rule = $rule;
		$this->source = $source;
		$this->reason = $reason;
	}

	public function getRule() {
		return $this->rule;
	}

	public function getSource() {
		return $this->source;
	}

	public function getReason() {
		return $this->reason;
	}

}

abstract class QInputObject {

	const MIN_LENGTH = 0;

	const MAX_LENGTH = 1;

	const EMAIL = 2;

	const DATE = 3;

	const DATE_PAST = 4;

	const DATE_FUTURE = 5;

	const MUST_HAVE_VALUE = 6;

	const MUST_PASS_REGEX = 7;

	public abstract function getID();

	public function hasValue() {
		return isset ( $_POST [$this->getID ()] );
	}

	public function getValue($defaultValue = "") {
		return $this->hasValue () ? $_POST [$this->getID ()] : $defaultValue;
	}

	public static function isValidEmail($email) {
		return filter_var ( $email, FILTER_VALIDATE_EMAIL ) !== false;
	}

	public static function isValidDate($dateStr) {
		return static::passRegex ( "/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/", $dateStr );
	}

	public static function dateToStamp($dateStr) {
		return strtotime ( $dateStr );
	}

	public static function dateIsFuture($dateStr) {
		return static::dateToStamp ( $dateStr ) > time ();
	}

	public static function dateIsPast($dateStr) {
		return static::dateToStamp ( $dateStr ) < time ();
	}

	private static function passRegex($regexPattern, $str) {
		$output_array = array ();
		preg_match ( $regexPattern, $str, $output_array );
		if (count ( $output_array ) == 1) {
			$val = $output_array [0];
			return $val == $str;
		}
		return false;
	}

	/**
	 *
	 * @param unknown $rule
	 * @param string $ruleValue
	 * @throws QAssertionFailException
	 */
	public function addAssertionRule($rule, $ruleValue = null, $reason) {
		if (is_array ( $rule )) {
			foreach ( $rule as $i => $r ) {
				$this->addAssertionRule ( $r, $ruleValue [$i], $reason );
			}
		} else {
			switch ($rule) {
				case QInputObject::MIN_LENGTH:
					if (strlen ( $this->getValue () ) < $ruleValue) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
				case QInputObject::MAX_LENGTH:
					if (strlen ( $this->getValue () ) > $ruleValue) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
				case QInputObject::EMAIL:
					if (! static::isValidEmail ( $this->getValue () )) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
				case QInputObject::DATE:
					if (! static::isValidDate ( $this->getValue () )) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
				case QInputObject::DATE_FUTURE:
					if (! static::isValidDate ( $this->getValue () )) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
					if (! static::dateIsFuture ( $this->getValue () )) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
				case QInputObject::DATE_PAST:
					if (! static::isValidDate ( $this->getValue () )) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
					if (! static::dateIsPast ( $this->getValue () )) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
				case QInputObject::MUST_HAVE_VALUE:
					if (! $this->hasValue ()) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
				case QInputObject::MUST_PASS_REGEX:
					if (! static::passRegex ( $ruleValue, $this->getValue () )) {
						throw new QAssertionFailException ( $this, $rule, $reason );
					}
				break;
			}
		}
	}

}

class QButtonRow extends QRow {

	public function getHTML() {
		$il = count ( $this->inputs );
		if ($il > 0) {
			$i0 = $this->inputs [0];
			$html = "<div id=\"" . $this->getID () . "\" class=\"form-group\"><div class=\"col-sm-offset-4 col-sm-7\">";
			foreach ( $this->inputs as $i => $input ) {
				$html .= "<div class=\"col-sm-" . (3 - $i) . " has-feedback\">";
				$html .= $input->getHTML ();
				$html .= "</div>";
			}
			$html .= "</div></div>";
		} else {
			$html = "";
		}
		return $html;
	}

	public function addSubmit($title, $submitOrReset = "submit", $cls = "primary") {
		$submit = new QSubmit ( $this->getID (), $title, $submitOrReset, $title, $cls );
		$this->inputs [] = $submit;
		return $submit;
	}

}

class QRow {
	private $label;
	private $id;
	private $inputs = array ();

	public function getID() {
		return $this->id;
	}

	public function __construct($panelID, $label) {
		$this->label = $label;
		$this->id = md5 ( $panelID . "_" . md5 ( $label ) );
	}

	/**
	 *
	 * @param unknown $name
	 * @param unknown $type
	 * @param unknown $placeHolder
	 * @param string $validationFunction
	 * @return QInput
	 */
	public function addInput($name, $type, $placeHolder, $validationFunction = false) {
		$input = new QInput ( $this->id, $name, $type, $placeHolder, $validationFunction );
		$this->inputs [] = $input;
		return $input;
	}

	/**
	 *
	 * @param unknown $name
	 * @return QSelect
	 */
	public function addSelect($name) {
		$sel = new QSelect ( $this->id, $name );
		$this->inputs [] = $sel;
		return $sel;
	}

	public function getHTML() {
		$il = count ( $this->inputs );
		if ($il > 0) {
			$i0 = $this->inputs [0];
			$html = "<div id=\"" . $this->id . "\" class=\"form-group\"><label class=\"control-label  col-sm-4\" for=\"" . $i0->getID () . "\" >" . $this->label . ":</label>";
			foreach ( $this->inputs as $i => $input ) {
				$html .= "<div class=\"col-sm-" . (3 - $i) . " has-feedback\">";
				$html .= $input->getHTML ();
				$html .= "</div>";
			}
			$html .= "</div>";
		} else {
			$html = "";
		}
		return $html;
	}

}

class QSubmit extends QInputObject {
	private $value;
	private $id;
	private $cls;

	public function __construct($id, $name, $type, $value, $cls) {
		$this->value = $value;
		$this->type = $type;
		$this->id = md5 ( $id . "_" . $name );
		$this->cls = $cls;
	}

	public function isSubmitted() {
		return isset ( $_POST [$this->id] );
	}

	public function getID() {
		return $this->id;
	}

	public function getHTML() {
		return "<input type=\"" . $this->type . "\" class=\"btn btn-" . $this->cls . "\" name=\"" . $this->id . "\" id=\"" . $this->id . "\" value=\"" . $this->value . "\" />";
	}

}

class QSelect extends QInputObject {
	private $id;
	private $options = array ();

	public function getID() {
		return $this->id;
	}

	public function hasValue() {
		return isset ( $_POST [$this->id] );
	}

	public function getValue() {
		return $_POST [$this->id];
	}

	public function __construct($id, $name) {
		$this->id = md5 ( $id . "_" . $name );
	}

	public function addOption($name, $value, $selected = false) {
		$opt = new QOption ( $name, $value, $selected );
		$this->options [] = $opt;
		return $opt;
	}

	public function getHTML() {
		$il = count ( $this->options );
		if ($il > 0) {
			$html = "<select class=\"form-control\" id=\"" . $this->id . "\" name=\"" . $this->id . "\" >";
			foreach ( $this->options as $i => $option ) {
				$html .= $option->getHTML ();
			}
			$html .= "</select>";
		} else {
			$html = "";
		}
		return $html;
	}

}

class QOption {
	private $name;
	private $value;
	private $selected;

	public function __construct($name, $value, $selected) {
		$this->name = $name;
		$this->value = $value;
		$this->selected = $selected;
	}

	public function getHTML() {
		$sel = $this->selected ? " selected" : "";
		return "<option" . $sel . " value=\"" . $this->value . "\">" . $this->name . "</option>";
	}

}

class QInput extends QInputObject {
	private $id;
	private $type;
	private $validationFunction;
	private $placeHolder;
	private $params = array ();

	public function getID() {
		return $this->id;
	}

	public function addValidationFunctionParameters($param) {
		$this->params [] = $param->getID ();
	}

	public function __construct($rowID, $name, $type, $placeHolder, $validationFunction) {
		$this->id = md5 ( $rowID . "_" . $name );
		$this->type = $type;
		$this->validationFunction = $validationFunction;
		$this->placeHolder = $placeHolder;
	}

	private function resolveParameters() {
		$p = array ();
		foreach ( $this->params as $param ) {
			$p [] = "document.getElementById('" . $param . "')";
		}
		return implode ( ",", $p );
	}

	public function getHTML() {
		$callback = $this->validationFunction !== false ? " onBlur=\"" . $this->validationFunction . "(" . $this->resolveParameters () . ")\"" : "";
		if ($this->validationFunction !== false) {
			$toAdd = "<span class=\"glyphicon glyphicon-ok form-control-feedback hidden\" aria-hidden=\"true\"></span><span id=\"registerPanel_usernameStatus\" class=\"text-danger\"></span>";
		} else {
			$toAdd = "";
		}
		
		return "<input type=\"" . $this->type . "\" class=\"form-control\" id=\"" . $this->id . "\" name=\"" . $this->id . "\" placeholder=\"" . $this->placeHolder . "\"" . $callback . " />" . $toAdd;
	}

}

?>