<?php

class preProcessor {
	private $styles_generics = array ();
	private $script_generics = array ();
	private static $instance;
	private static $processedComponents = array ();

	public function appendHeadItems() {
		foreach ( $this->script_generics as $src => $data ) {
			echo "<script src=\"" . $src . "\"></script>";
		}
		foreach ( $this->styles_generics as $href => $data ) {
			echo "<link href=\"" . $href . "\" rel=\"stylesheet\" type=\"text/css\" />";
		}
	}

	/**
	 *
	 * @return preProcessor
	 */
	public static function get() {
		if (static::$instance == null) {
			static::$instance = new preProcessor ();
		}
		return static::$instance;
	}

	private function __construct() {
	}

	private function convert($componentName) {
	}

	private function conversionCheck($componentName) {
		if (! isset ( static::$processedComponents [$componentName] )) {
			$componentFolder = "pages/components/" . $componentName;
			if (! file_exists ( $componentFolder )) {
				var_dump ( $componentFolder );
				die ( "Invalid component: " . $componentName );
			}
			$catalog = Catalog::getCatalog ( $componentFolder, $componentName );
			static::$processedComponents [$componentName] = $catalog;
			if ($catalog->update () || ! Config::DEBUG_USE_CATALG) {
				$this->commitUpdate ( $componentName, $catalog );
				$catalog->save ();
			}
			foreach ( $catalog->getGenericStyles () as $style => $a ) {
				$this->styles_generics [$style] = array (
						"src" => $style,
						"component" => $componentName 
				);
			}
			foreach ( $catalog->getGenericScripts () as $script => $a ) {
				$this->script_generics [$script] = array (
						"src" => $script,
						"component" => $componentName 
				);
			}
		}
	}
	private static $singleTags = array (
			"meta",
			"input",
			"link",
			"hr",
			"br",
			"img"
	);

	private function commitUpdate($componentName, $catalog) {
		$singleTags = implode ( "|", static::$singleTags );
		$componentFolder = "pages/components/" . $componentName;
		$processFolder = "pages/processed/" . $componentName;
		$componentFile = $componentFolder . "/" . $componentName . ".php";
		$processedHeadFile = $processFolder . "/" . $componentName . "_head.php";
		$processedBodyFile = $processFolder . "/" . $componentName . "_body.php";
		
		if (file_exists ( $processFolder )) {
			@rmdir ( $processFolder );
		}
		@mkdir ( $processFolder );
		$tokens = token_get_all ( file_get_contents ( $componentFile ) );
		$phpScriptCount = 0;
		$currentScriptArray = array ();
		$currentHTMLArray = array ();
		$inPhpTag = false;
		foreach ( $tokens as $token ) {
			if (is_array ( $token )) {
				$type = $token [0];
				$text = $token [1];
				$line = $token [2];
				switch ($type) {
					case T_OPEN_TAG:
						$inPhpTag = true;
						$currentScriptArray = array (
								$text 
						);
						$currentHTMLArray [] = "<script type=\"PHP\" src=\"" . $phpScriptCount . "\"></script>";
					break;
					case T_CLOSE_TAG:
						$inPhpTag = false;
						$currentScriptArray [] = $text;
						$scriptFile = $processFolder . "/tmp_scr_" . $phpScriptCount . ".php";
						file_put_contents ( $scriptFile, implode ( "", $currentScriptArray ) );
						$phpScriptCount ++;
					break;
					default :
						if ($inPhpTag) {
							$currentScriptArray [] = $text;
						} else {
							$currentHTMLArray [] = $text;
						}
					break;
				}
			} else {
				if ($inPhpTag) {
					$currentScriptArray [] = $token;
				} else {
					$currentHTMLArray [] = $token;
				}
			}
		}
		$content = trim ( implode ( "", $currentHTMLArray ) );
		if (strlen ( $content ) == 0) {
			$content = "<html><head></head><body></body></html>";
		}
		$original = new domDocument ();
		@$original->loadHTML ( $content ); // TODO: HTML or XML?
		$head = $original->getElementsByTagName ( "head" )->item ( 0 );
		$body = $original->getElementsByTagName ( "body" )->item ( 0 );
		$title = $original->getElementsByTagName ( "title" )->item ( 0 );
		if ($title != null) {
			$title->parentNode->removeChild ( $title );
		}
		$metas = $original->getElementsByTagName ( "meta" );
		$index = 0;
		while ( ($meta = $metas->item ( $index )) != null ) {
			$meta->parentNode->removeChild ( $meta );
			// $index ++; //TODO: Error?
		}
		
		$links = $original->getElementsByTagName ( "link" );
		$index = 0;
		while ( ($link = $links->item ( $index )) != null ) {
			$href = $link->getAttribute ( "href" );
			if ($link->hasAttribute ( "generic" )) {
				$generic = strtolower ( $link->getAttribute ( "generic" ) ) == "true";
			} else if ($link->hasAttribute ( "specific" )) {
				$generic = strtolower ( $link->getAttribute ( "specific" ) ) == "false";
			} else {
				$generic = false;
			}
			if ($generic) {
				$href = "/" . Config::FRONTEND_URL . "/" . $href;
				$catalog->addGenericStyle ( $href );
				$link->parentNode->removeChild ( $link );
			} else {
				$href = "/" . Config::FRONTEND_URL . "/" . $componentFolder . "/" . $href;
				$link->setAttribute ( "href", $href );
				$index ++;
			}
		}
		$scripts = $original->getElementsByTagName ( "script" );
		$index = 0;
		while ( ($script = $scripts->item ( $index )) != null ) {
			$src = $script->getAttribute ( "src" );
			$ignore = $script->hasAttribute ( "type" ) ? $script->getAttribute ( "type" ) == "PHP" : false;
			if (! $ignore) {
				if ($script->hasAttribute ( "generic" )) {
					$generic = strtolower ( $script->getAttribute ( "generic" ) ) == "true";
				} else if ($script->hasAttribute ( "specific" )) {
					$generic = strtolower ( $script->getAttribute ( "specific" ) ) == "false";
				} else {
					$generic = false;
				}
				if ($generic) {
					$src = "/" . Config::FRONTEND_URL . "/" . $src;
					$catalog->addGenericScript ( $src );
					$script->parentNode->removeChild ( $script );
				} else {
					$src = "/" . Config::FRONTEND_URL . "/" . $componentFolder . "/" . $src;
					$this->script_specifics [$src] = array (
							"src" => $src,
							"component" => $componentName 
					);
					$script->setAttribute ( "src", $src );
					$index ++;
				}
			} else {
				$index ++;
			}
		}
		
		$head = $head->ownerDocument->saveHTML ( $head );
		$body = $body->ownerDocument->saveHTML ( $body );
		for($i = 0; $i < $phpScriptCount; $i ++) {
			$scriptFile = $processFolder . "/tmp_scr_" . $i . ".php";
			$scr = file_get_contents ( $scriptFile );
			$search = "<script type=\"PHP\" src=\"" . $i . "\"></script>";
			$head = str_replace ( $search, $scr, $head );
			$body = str_replace ( $search, $scr, $body );
			unlink ( $scriptFile );
		}
		
		// TODO: If indentation fails, this is why:
		$head = preg_replace ( "/(<\W*(?:" . $singleTags . ")[^\/]*(?:\W*>|(?:[^>])*))*/im", "$1", $head );
		$head = preg_replace ( "/<\W*(?:" . $singleTags . ")(?:[^>])*/im", "$0 /", $head );
		
		$body = preg_replace ( "/(<\W*(?:" . $singleTags . ")[^\/]*(?:\W*>|(?:[^>])*))*/im", "$1", $body );
		$body = preg_replace ( "/<\W*(?:" . $singleTags . ")(?:[^>])*/im", "$0 /", $body );
		
		$lh1 = strlen ( "<head>" );
		$lh2 = strlen ( "</head>" );
		$head = trim ( $head );
		$body = trim ( $body );
		$head = substr ( $head, $lh1, strlen ( $head ) - $lh1 - $lh2 );
		$body = substr ( $body, $lh1, strlen ( $body ) - $lh1 - $lh2 );
		$head = trim ( $head );
		$body = trim ( $body );
		file_put_contents ( $processedHeadFile, $head );
		file_put_contents ( $processedBodyFile, $body );
	}

	public function includeHead($componentName) {
		if (! isset ( static::$processedComponents [$componentName] )) { //TODO: Head errors happen here. Inclusion is exclusive!
			$this->conversionCheck ( $componentName );
			
			$processFolder = "pages/processed/" . $componentName;
			$processedHeadFile = $processFolder . "/" . $componentName . "_head.php";
			
			include $processedHeadFile;
		}
	}

	public function includeBody($componentName) {
		$this->conversionCheck ( $componentName );
		
		$processFolder = "pages/processed/" . $componentName;
		$processedBodyFile = $processFolder . "/" . $componentName . "_body.php";
		
		include $processedBodyFile;
	}

}

class Catalog {
	private $data = array ();
	private $catalogFile;
	private $catalogFolder;
	private $componentName;

	public function getGenericStyles() {
		return $this->data ["generics"] ["styles"];
	}

	public function getGenericScripts() {
		return $this->data ["generics"] ["scripts"];
	}

	public function addGenericScript($script) {
		$this->data ["generics"] ["scripts"] [$script] = true;
	}

	public function addGenericStyle($style) {
		$this->data ["generics"] ["styles"] [$style] = true;
	}

	private function __construct($catalogFolder, $catalogFile, $componentName) {
		$this->catalogFile = $catalogFile;
		$this->catalogFolder = $catalogFolder;
		$this->componentName = $componentName;
		$this->data = json_decode ( file_get_contents ( $catalogFile ), true );
	}

	private function getFileMetrics($file) {
		return md5 ( file_get_contents ( $file ) );
	}

	/**
	 *
	 * @return Catalog
	 */
	public function update() {
		$dir = scandir ( $this->catalogFolder );
		$update = false;
		// foreach ( $dir as $fileName ) {
		$fileName = $this->componentName . ".php";
		if ($fileName == ".." || $fileName == ".") {
			continue;
		}
		$file = $this->catalogFolder . "/" . $fileName;
		$nw = $this->getFileMetrics ( $file );
		if (isset ( $this->data ["files"] [$fileName] )) {
			$current = $this->data ["files"] [$fileName];
			if ($current != $nw) {
				$update = true;
				$this->data ["files"] [$fileName] = $nw;
			}
		} else {
			$update = true;
			$this->data ["files"] [$fileName] = $nw;
		}
		// }
		return $update;
	}

	/**
	 *
	 * @return Catalog
	 */
	public function save() {
		file_put_contents ( $this->catalogFile, json_encode ( $this->data ) );
		return $this;
	}

	/**
	 *
	 * @return Catalog
	 */
	public static function getCatalog($componentFolder, $componentName) {
		$catalogFile = $componentFolder . "/catalog.json";
		if (! file_exists ( $catalogFile )) {
			file_put_contents ( $catalogFile, json_encode ( array (
					"files" => array (),
					"generics" => array (
							"scripts" => array (),
							"styles" => array () 
					) 
			) ) );
		}
		$catalog = new Catalog ( $componentFolder, $catalogFile, $componentName );
		return $catalog;
	}

}
