<?php

class viewState {
	private static $isRegistering;
	private static $movie;
	private static $cache;
	private static $viewUserID;

	public static function setViewUserID($userId) {
		static::$viewUserID = $userId;
	}

	public static function getViewUserID() {
		return static::$viewUserID;
	}

	private static function isCached($key) {
		return isset ( static::$cache [$key] );
	}

	private static function addToCache($key, $value) {
		static::$cache [$key] = $value;
	}

	private static function getCache($key) {
		return static::$cache [$key];
	}

	public static function setMovie($movie) {
		static::$movie = $movie;
	}

	/**
	 *
	 * @return Movie
	 */
	public static function getMovie() {
		return static::$movie;
	}

	public static function isRegistering() {
		return static::$isRegistering;
	}

	public static function setRegistering($reg) {
		static::$isRegistering = $reg;
	}

	public static function forceUpdateAllMoviesForUser($user) {
		$ret = QuickAlbumDetails::getAllForUser ( $user );
		static::addToCache ( "QuickAlbumDetails::getAllForUser", $ret );
	}

	public static function getAllMoviesForUser($user) {
		if (static::isCached ( "QuickAlbumDetails::getAllForUser" )) {
			return static::getCache ( "QuickAlbumDetails::getAllForUser" );
		} else {
			$ret = QuickAlbumDetails::getAllForUser ( $user );
			static::addToCache ( "QuickAlbumDetails::getAllForUser", $ret );
			return $ret;
		}
	}

}